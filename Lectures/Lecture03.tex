% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 3 (Scholze, 25.10.2023) -- {\it More on Light Condensed Abelian Groups}]{Lecture 3 (3/24)}
\subsection*{Recall:} Light profinite Sets: \begin{alignat*}{2}\textrm{Pro}_\N(\textrm{Fin}) & \cong\left\{\begin{array}{l}
\textrm{metrizable, totally disconnected,}\\
\textrm{compact, Hausdorff spaces}
 \end{array}\right\}\\ & \cong\left\{\textrm{Countable Boolean algebras}\right\}^{\textrm{op}}\end{alignat*}\\
 They have a Grothendieck topology with covers defined as: 
 \begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
 	\item finite disjoint unions
	\item all surjective maps
 \end{enumerate}
 \[\Rightarrow \textrm{sequential limits of covers are still covers!}\]
 \[\begin{tikzcd}
                                                                                                                   & \textrm{Top} \arrow[rd, "X\mapsto\underline{X}"] &                                                                              \\
\textrm{Pro}_\mathbb{N}(\textrm{Fin}) \arrow[ru, hook] \arrow[rr, "{\chi: S\mapsto(T\mapsto\textrm{Cont}(T,S))}"', hook] &                                                  & \textrm{CondSet}^{\textrm{light}}=\textrm{Shv(Pro}_\mathbb{N}(\textrm{Fin}))
\end{tikzcd}\]
The image of $\chi$ generates $\textrm{CondSet}^\textrm{light}$ under colimits. The functor
\begin{align*}
	\mathrm{Top}&\rightarrow\lcondset
\\X &\mapsto\underline{X}\end{align*} 
has a left adjoint $X\mapsto X(\ast)_\textrm{top}$, where the topology is given by the quotient topology of \[\underset{\alpha\in X(\textrm{Cantor Set})}\bigsqcup(\textrm{Cantor Set})\longtwoheadrightarrow X(\ast)_\textrm{top}.\]
Also we have in topological spaces the quotient map
\[\underset{\gamma\in \underline{\textrm{Cantor Set}}(\N\cup\{\infty\})}\bigsqcup(\N\cup\{\infty\})\longtwoheadrightarrow\textrm{Cantor Set}\] 
Hence we can understand \[\textrm{Cantor Set} \cong \underset{\arraycolsep=0.5pt\def\arraystretch{0.1}\substack{
\text{countable, closed}\\ 
 \text{subsets }Z}}\colim(Z) \in \textrm{Top}.\] 
This gives us something like "metrizable compactly generated" = "sequential" and we get 
\[\{\textrm{sequential topological spaces}\}\longhookrightarrow\textrm{CondSet}^\textrm{light}.\]
\begin{remark**}\label{metriz}
Hence we can also get the topology of $X(\ast)_\textrm{top}$ via the quotient topology
\[\underset{X(\textrm{Cantor Set})}\bigsqcup\,\underset{\underline{\textrm{Cantor Set}}(\N\cup\{\infty\})}\bigsqcup(\N\cup\{\infty\})\longtwoheadrightarrow X(\ast)_\textrm{top}.\] 
If $X$ is a qcqs light profinite set embedded via the Yoneda embedding, this coincides with the topology on $X$ as a metrizable totally disconnected compact Hausdorff space\footnote{We see that qcqs light profinite sets are metrizable totally disconnected compact Hausdorff spaces in \ref{prop}.}.
\end{remark**}
\begin{proof*}\footnote{Thanks to Semen for this idea!}
Let us denote $X$ with its original topology as $X_\textrm{lprof}$. Since on set level $X_\textrm{lprof}= X(\ast)_{\textrm{top}}$ it suffices to show, that Hom$(X_\textrm{lprof}, Y)=\textrm{Hom}(X(\ast)_\textrm{top}, Y)$ for any topological space $Y$. Now, since light profinite sets are metrizable spaces they are also sequential. Suppose we have a map $X_{\textrm{lprof}}\rightarrow Y$ and consider all convergent sequences of the image of an element $\gamma\in\underline{X}(\textrm{Cantor Set}) \subseteq X_{\textrm{lprof}}$. Then we get the diagram where solid arrows are continuous maps and the dotted arrows are maps of sets:
\[
\begin{tikzcd}
X(\ast)_\textrm{top} \arrow[r, dotted] \arrow[rd, <->, dotted, "\textrm{id}_\textrm{Set}"] & Y                            \\
\bigsqcup(\mathbb{N}\cup\{\infty\}) \arrow[r] \arrow[u]        & X_{\textrm{lprof}} \arrow[u]
\end{tikzcd}
\] Now as $\bigsqcup(\mathbb{N}\cup\{\infty\})\rightarrow X_{\textrm{lprof}}\rightarrow Y$ is continuous and commutes with the factorization through $X(\ast)_{\textrm{top}}$, the upper dotted arrow is continuous from the defintion of the quotient topology. For the other way round we get, as $\bigsqcup(\mathbb{N}\cup\{\infty\})\rightarrow Y$ is continuous via the factorization through $X(\ast)_{\textrm{top}}$, that for every convergent sequence in the image of $\gamma$ we get a converging sequence in $\gamma(\textrm{Cantor Set})\rightarrow Y$. Since $X_\textrm{lprof}$ is sequential, this is continuous. As we get local continuity that way, we are done.
\end{proof*}

\subsection{Why allow the Cantor Set?}
In any topos, there is an intrinsic notion of being "compact" and of being "Hausdorff".
\begin{definition}\leavevmode
	\begin{enumerate}
	\item An object $X$ is \textbf{quasicompact (qc)} if any cover admits a finite subcover, i.e. for $\underset{i\in I}\bigsqcup X_i\longtwoheadrightarrow X$, we get a finite $J\subseteq I$ s.t. $\underset{i\in J}\bigsqcup{X_i}\longtwoheadrightarrow X$. \\In our case $\iff\exists$ surjection (Cantor Set) $\longtwoheadrightarrow X$ or $X=\emptyset$
	\item An object is $\textbf{quasiseparated (qs)}$ if for all quasicompact $Y,Z\rightarrow X$, the fibre product $Y\times_X Z$ is quasicompact. 
	\end{enumerate}
\end{definition}
\textbf{Here:} For all $f,g:(\textrm{Cantor Set})\longtwoheadrightarrow X$ we have $\{0,1\}^\N\times_X \{0,1\}^\N$ is quasicompact. If we only allowed $\N\cup\{\infty\}$ in test category then the quasicompact objects would all be countable.
\begin{proposition}\label{prop}
\leavevmode
\begin{enumerate}
	\item $\{\text{qcqs (light) condensed sets}\}\cong\{\text{(metrizable) compact Hausdorff spaces}\}.$\\ For this equivalence to be true we need 
	\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
		\item finitary Grothendieck topology (otherwise our basic objects wouldn't be quasicompact)
		\item allow Cantor Set
	\end{enumerate}
	\item The following holds:\footnote{The ind-category is the category of formal direct limits, similar to how a pro-category is the category of formal inverse limits.}  \begin{align*}\left\{\begin{array}{l}
	\text{qs (light)}\\
	\text{condensed sets}
	\end{array}\right\}&\cong \mathrm{Ind}_{\mathrm{inj}}\left(\left\{\begin{array}{l}
\text{(metrizable,) compact}\\
 \text{Hausdorff spaces}
 \end{array}\right\}\right)\\
  & \supseteq\left\{\begin{array}{l}
\text{(metrizable,) compactly generated}\\
 \text{weak Hausdorff spaces}\end{array}\right\}=:\mathrm{mCGWH}\end{align*}

\end{enumerate}
\end{proposition}
In mCGWH one can write Cantor Set $=\colim$(countable closed subsets) which we can't in (qs) light condensed sets.\\
As $[0,1]$ is compact Hausdorff, we have implicitly in 1.:\begin{align*} \{0,1\}^\N&\twoheadrightarrow[0,1]      \\
(a_0,a_1,...)&\mapsto0.a_0a_1a_2... \quad\text{in binary expansion} \end{align*}
\subsection{Light Condensed Abelian Groups}
\begin{align*} \lcondab & = \text{Abelian group objects in }\lcondset \\
						 & = \mathrm{Shv(Pro}_\N\mathrm{(Fin), Ab)}\end{align*}
General theory of sheaves:
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item Grothendieck abelian category
	\item has $\otimes$\footnote{$M\otimes N$ is the sheafification of $S\mapsto M(S)\otimes N(S)$.}: unit object is $\underline{\Z}:S\mapsto\cont(S,\Z)$
	\item The forgetful functor $\lcondab\rightarrow\lcondset$ has a left adjoint "free condensed abelian group" $X\mapsto\Z[X]$ where $\Z[X]$ is the sheafification of $S\mapsto\Z[X(S)]$.
\end{enumerate}
\textbf{Idea:} $\Z[X]$ has "some topology" on $\Z[X(\ast)]$.
\begin{example}
We will have a look at $\Z[\underline{\R}]$. Firstly we will see what happens at the classical object, i.e. \footnote{Note that we don't need to worry about the sheafification, as evaluating at a point is just taking a stalk.}\begin{align*}\Z[\underline{\R}](\ast)&=\Z[\R]=\left\{\sum_{x\in\R}n_x[x], n_x\in\Z, \text{almost all }0\right\} \\
&=\underset{I=[-c,c]\subset\R}\colim\Z[I].\end{align*} Here we have $\Z[I]=\underset{n\in\N}\bigcup\Z[I]_{\leq n}\footnote{These are compact Hausdorff.}$ where $\Z[I]_{\leq n}:=\left\{\sum_{x\in I}n_x[x]\mid\sum|n_x|\leq n\right\}$. To get results in the more global setting we can use the same construction via this limit.
\end{example}
By general abstract nonsense $\Z[G]$ is naturally a condensed ring.
\begin{theorem}\label{2.7}
In $\lcondab$ the following hold:
\begin{enumerate}
	\item Countable products are exact.
	\item Sequential limits of surjective maps are still surjective.
	\item $\Z[\N\cup\{\infty\}]$ is internally projective.\footnote{We say that $P$ projective iff $\ext^i(P,-)=0$ for $i>0$. We say that $P$ internally projective iff $\underline{\ext}^i(P,-)=0$ for $i>0$. Here internal Ext is given by $\underline{\ext}^i(M,N)=$ sheafification of $S\mapsto\ext^i(M\otimes\Z[S],N).$}
\end{enumerate}
\end{theorem}
\begin{proof}[Proof of 1. and 2.]
\leavevmode
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item reduces to 2.: We need $\forall f_n:M_n\longtwoheadrightarrow N_n$, that also $\prod f_n:\prod M_n\longtwoheadrightarrow \prod N_n$ and then we are done, since products of injective maps stay injective. We have $\forall m$, \[\underset{n\leq m}\prod M_n\times\underset{n+1\geq m}\prod N_n\longtwoheadrightarrow\underset{n}\prod N_n.\] Now if we take the limit over this, we get \[\underset{m}\varprojlim\,\left(\underset{n\leq m}\prod M_n\times\underset{n+1\geq m}\prod N_n\right)=\underset{m}\prod M_n.\]
	\item A map of sheaves $F'\rightarrow F$ is surjective, iff for every section $s\in F(U)$ , there is a covering $\{U_i\}$, s.t. there exists a preimage of $s\vert_{U_i}\in F(U_i)$ in $F'(U_i)$ . Since by Yoneda embedding $\mathrm{Hom}(S,X)\cong X(S)$ for a map $X'\rightarrow X$ in $\lcondab$ and for all $S\in\mathrm{Pro}_\N\text(\mathrm{Fin})$ a map $X'\rightarrow X$ is surjective iff for every diagram of solid arrows, there are dotted arrows that make the following diagram commute:
\[
\begin{tikzcd}
X' \arrow[r]                                      & X           \\
S' \arrow[r, two heads, dotted] \arrow[u, dotted] & S \arrow[u]
\end{tikzcd}
\]
In our case we have a sequential limit of surjective maps, i.e. a limit of the diagram \[
\begin{tikzcd}
... \arrow[r, two heads] & M_2 \arrow[r, two heads] & M_1 \arrow[r, two heads] & M_0
\end{tikzcd}.
\] Hence we get the diagram\[\begin{tikzcd}
M_\infty=\underset{m}\varprojlim\,(\text{ ...} \arrow[r, two heads] & M_2 \arrow[r, two heads] & M_1 \arrow[r, two heads] & M_0) \arrow[r, "f"] & M_0.
\end{tikzcd}\]
We want to show that $f$ is surjective, hence we want to get a surjective map $S_\infty \longtwoheadrightarrow S\text{ in Pro}_\N\text{(Fin)}$ and a map $S_\infty\rightarrow M_\infty$ s.t.
\[\begin{tikzcd}
M_\infty&[-35]=\underset{m}\varprojlim\,(\text{ ...} \arrow[r, two heads]      & M_2 \arrow[r, two heads] & M_1 \arrow[r, two heads] & M_0) \arrow[r, "f"] & M_0           \\
S_\infty \arrow[rrrrr, two heads] \arrow[u, "\exists?", dotted] &                          &                          &                                   &   & S_0 \arrow[u]
\end{tikzcd}\] commutes. Since inside the maps are surjections, we get a diagram of the following form: 
\[\begin{tikzcd}
M_\infty&[-35] =\underset{m}\varprojlim\,(     &[-35pt]\text{ ...} \arrow[r, two heads]           & M_2 \arrow[r, two heads]           & M_1 \arrow[r, two heads]           & M_0) \arrow[r, "f"]                 & M_0            \\
                                       &                    & \text{ ...} \arrow[r, two heads] \arrow[u] & S_2 \arrow[u] \arrow[r, two heads] & S_1 \arrow[u] \arrow[r, two heads] & S_0 \arrow[u] \arrow[rd, two heads] &                \\
S_\infty \arrow[rrrrrr, two heads] \arrow[uu, "\exists?", dotted] &   &      &                                   &                                    &                                    &                                      S_0 \arrow[uu]
\end{tikzcd}
.\] Now, as countable limits of surjections are still surjective in $\text{Pro}_\N\text{(Fin)}$, we can take $S_\infty$ to be the limit of the sequence  \begin{tikzcd}
S_\infty=\underset{m}\varprojlim\,(\text{ ...} \arrow[r, two heads] & S_2 \arrow[r, two heads] & S_1 \arrow[r, two heads] & S_0)
\end{tikzcd}.
Hence we get our induced map from the limit and the following commuting diagram:\[
\begin{tikzcd}
M_\infty \arrow[rr, equals]& &[-25]\underset{m}\varprojlim(&[-35pt]\text{...} \arrow[r, two heads]           & M_2 \arrow[r, two heads]           & M_1 \arrow[r, two heads]           & M_0) \arrow[r, "f"]                  & M_0            \\
                                  & &  \underset{m}\varprojlim (                      & \text{ ...} \arrow[r, two heads] \arrow[u] & S_2 \arrow[u] \arrow[r, two heads] & S_1 \arrow[u] \arrow[r, two heads] & S_0) \arrow[u] \arrow[rd, two heads] &                \\
S_\infty \arrow[rrrrrrr, two heads] \arrow[uu] \arrow[rru, equals] &      &     &&                                                         &                                    &                                                                         & S_0 \arrow[uu]
\end{tikzcd}
\]
So we are done here.
\end{enumerate}
\end{proof}
\noindent Critical for 2 is that countable limits of covers are covers. This forces one to use totally disconnected spaces as building blocks. The intuition is the picture below, where each line is an interval. 
\begin{figure}[htbp]
\centering
\tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        

\begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-1,xscale=1]
%uncomment if require: \path (0,300); %set diagram left start at 0, and has height of 300

%Straight Lines [id:da4866041193146896] 
\draw    (113.34,156) -- (169.4,156) -- (187.11,156) ;
%Straight Lines [id:da582861441270271] 
\draw    (121.1,191) -- (254.39,191) ;
%Straight Lines [id:da20653755001504837] 
\draw    (150,160) -- (150,178) ;
\draw [shift={(150,180)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da9275846454974006] 
\draw    (220,150) -- (220,178) ;
\draw [shift={(220,180)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da6022983010595248] 
\draw    (169.3,143) -- (238.59,143) -- (260.47,143) ;
%Straight Lines [id:da6925541298408323] 
\draw    (224.65,90) -- (260.2,90) -- (271.43,90) ;
%Straight Lines [id:da5114532845360319] 
\draw    (191.69,71) -- (235.78,71) -- (249.7,71) ;
%Straight Lines [id:da4474378597494293] 
\draw    (164.46,91) -- (193.23,91) -- (202.32,91) ;
%Straight Lines [id:da6407153101292957] 
\draw    (250,99) -- (250,128) ;
\draw [shift={(250,130)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da9188175975988044] 
\draw    (210,79) -- (210,128) ;
\draw [shift={(210,130)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;
%Straight Lines [id:da1956831843455611] 
\draw    (190,99) -- (190,128) ;
\draw [shift={(190,130)}, rotate = 270] [color={rgb, 255:red, 0; green, 0; blue, 0 }  ][line width=0.75]    (10.93,-3.29) .. controls (6.95,-1.4) and (3.31,-0.3) .. (0,0) .. controls (3.31,0.3) and (6.95,1.4) .. (10.93,3.29)   ;

% Text Node
\draw (118,182.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};%
% Text Node
\draw (110,148) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};%
% Text Node
\draw (249.94,182.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};%
% Text Node
\draw (182.7,148) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};%
% Text Node
\draw (166,134.8) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};%
% Text Node
\draw (255.5,134.8) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};%
% Text Node
\draw (221.2,81.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};%
% Text Node
\draw (266.7,81.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};%
% Text Node
\draw (189,62.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};%
% Text Node
\draw (245,62.5) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};%
% Text Node
\draw (161,82) node [anchor=north west][inner sep=0.75pt]   [align=left] {(};
% Text Node
\draw (197.44,82) node [anchor=north west][inner sep=0.75pt]   [align=left] {)};


\end{tikzpicture}


\end{figure}\\
\noindent In the limit we get something totally disconnected.

\begin{proof}[Proof of 3.]

For this proof we will take two steps: First we show that $\Z[\ninfty]$ is projective. This is only true in $\lcondab$ and not true in $\lcondset$: \[\begin{tikzcd}
                                                                  & (2\mathbb{N}\cup\{\infty\})\sqcup((2\mathbb{N}+1)\cup\{\infty\}) \arrow[d, two heads] \\
\mathbb{N}\cup\{\infty\} \arrow[ru, "\nexists", dotted] \arrow[r] & \mathbb{N}\cup\{\infty\}                                                             
\end{tikzcd} \]
Afterwards, we will show internally projectivity.
\begin{description}
\item[1. Projectivity] 
Let $M=\Z[\ninfty]/\Z[\infty]$ which classifies null sequences. Note that if we can show that $M$ is projective we are done, as then $\Z[\ninfty]= \Z[\infty]\oplus M$, where $\Z[\infty]$ is already projective. Suppose we have \[
\begin{tikzcd}
                                           & N' \arrow[d, two heads] \\
M \arrow[ru, "\exists?", dotted] \arrow[r] & N                 
\end{tikzcd}   \]  
Now, as $\Z[-]$ is left adjoint to the forgetful functor from $\lcondab$ to $\lcondset$ and , we have $\mathrm{Hom}_{\lcondab}(\Z[\ninfty], M)\cong\mathrm{Hom}_{\lcondset}(\ninfty, M)$. This works as $M \in\lcondab$. Thus it suffices to lift the following diagram\footnote{This diagram exists because $\ninfty$ is a light profinite set and as discussed before, since $N'\longtwoheadrightarrow N$ is surjective, there must be a $S\in\lcondset$ such that this diagram commutes.} in $\lcondset$:
\[
\begin{tikzcd}
S \arrow[rr, "\exists g"] \arrow[d, two heads] &             & N' \arrow[d, two heads] \\
\ninfty \arrow[r]                              & M \arrow[r,"h"] & N                       \\[-22pt]
\infty \arrow[rr, maps to]                     &             & 0                      
\end{tikzcd}
\]
As $\ninfty$ is covered by $S$ at each point, we can choose just any lift (i.e. a point as preimage for each point but $\infty$)  as another cover s.t. w.l.o.g. $S\times_\ninfty \N \xrightarrow{\sim}\N$. We have that $S_\infty:= S\times_\ninfty \{\infty\}\overset{i}\subseteq S$ is closed. Since $S_\infty$ is light, it is injective in Prof(Fin) $\Rightarrow \exists\text{ retraction}$  $r:S\rightarrow S_\infty$. Now consider: \[
\begin{tikzcd}
S \arrow[rr, "g-g\circ i \circ r"]                                        &  & N'                          \\
S_\infty \arrow[rr] \arrow[u, hook] &  & \{0\} \arrow[u, hook]
\end{tikzcd}
\]
But as \[
\begin{tikzcd}
S_\infty \arrow[r, hook] \arrow[d] & S \arrow[d, two heads] \\
\{\infty\} \arrow[r]               & \ninfty               
\end{tikzcd}
\] is a pushout in light condensed sets, we get the diagram:
\[
\begin{tikzcd}
S \arrow[rr, "g-g\circ i \circ r"] \arrow[rd] &                                         & N'                \\
                                              & \ninfty \arrow[ru, "\exists f", dotted] &                   \\
                                              & \{\infty\} \arrow[rd] \arrow[u, hook]   &                   \\
S_\infty \arrow[rr] \arrow[uuu] \arrow[ru]    &                                         & \{0\} \arrow[uuu]
\end{tikzcd}
\] It's now left to show that this $f$ actually lifts our original diagram:
\[
\begin{tikzcd}
M &[-32pt] = &[-32] {\Z[\ninfty]/\Z[\infty]} \arrow[r,"f"] \arrow[rd, "\overset{?}=h"'] & N' \arrow[d, two heads] \\
  &   &                                                                 & N                      
\end{tikzcd}
.\]
As $\Z[S]\longtwoheadrightarrow M$ surjects\footnote{Here we have used the forgetful functor, $\Z[-]$- adjunction again.}, it suffices to show that the composite is the same as $h$. But as the map $\Z[S]\rightarrow N'$ is induced by $g-g\circ i\circ r$ and $g\circ i\circ r$ projects to $0$ on $N$, we are done by the original diagram.
\item[2. Internally projective$^*$]{\footnote{Thanks to Ferdinand for this proof!}}
We want that $\underline{\mathrm{Hom}}(\Z[\ninfty],-)$ is exact, i.e. if we have an epimorphism $N'\longtwoheadrightarrow N$, then $\underline{\mathrm{Hom}}(\Z[\ninfty],N')\longtwoheadrightarrow\underline{\mathrm{Hom}}(\Z[\ninfty],N)$. 
\begin{align*}
&\underline{\mathrm{Hom}}\left(\Z[\ninfty], N\right)(S)\\
&\cong\mathrm{Hom}_\lcondset\left(S, \underline{\mathrm{Hom}}\left(\Z[\ninfty], N\right)\right) \\
&\cong\mathrm{Hom}_\lcondab\left(\Z[S], \underline{\mathrm{Hom}}\left(\Z[\ninfty], N\right)\right)\\
&\cong\mathrm{Hom}_\lcondab\left(\Z[S]\otimes\Z[\ninfty], N\right)\\
&\cong\mathrm{Hom}_\lcondab\left(\Z[S\times\left(\ninfty\right)], N\right)\\
&\cong\mathrm{Hom}_\lcondset\left(S\times\left(\ninfty\right), N\right)
\end{align*}
Hence there is a $T\in \mathrm{Pro}_\N\mathrm{(Fin)}$ and a commutative diagram:
\[
\begin{tikzcd}
T \arrow[r] \arrow[d, "f"', two heads] & N' \arrow[d, two heads] \\
S\times(\ninfty) \arrow[r]       & N                      
\end{tikzcd}
\] 

Now our aim is, to get some $S'$, s.t. $S'\longtwoheadrightarrow S$ is a cover and s.t. the following diagram commutes: 
\[
\begin{tikzcd}
S'\times(\ninfty) \arrow[r] \arrow[d, "f"', two heads] & N' \arrow[d, two heads] \\
S\times(\ninfty) \arrow[r]       & N                      
\end{tikzcd}\]
Following from that, there is a surjection \[\underline{\mathrm{Hom}}(\Z[\ninfty],N')(S')\longtwoheadrightarrow \underline{\mathrm{Hom}}(\Z[\ninfty],N')(S),\] which suffices, as it is surjective on a cover. Note that it is important that the morphism $S'\times\ninfty\longtwoheadrightarrow S\times\ninfty$ is induced by a surjection $S'\longtwoheadrightarrow S$.\\ 
For that, define 
$T_i:=f^{-1}\left(S\times\{i\}\right)\subseteq S\times(\ninfty)$ for $i\in\ninfty$.  Consider the fibre product: \[\begin{tikzcd}
	& [-37pt]T_1\times T_2\times T_{\infty}  \arrow[ld, two heads] \arrow[rd, two heads] &  [-37pt]                         \\
T_1 \arrow[rd, two heads] &                                                                             & T_2 \arrow[ld, two heads] \\
	& S                                                                           &                          
\end{tikzcd}\]
The limit over indices, i.e. $A=T_1\times_S T_2\times_S...\times_S T_\infty$ is still a cover, as sequential limits of surjections stay surjective. Choosing a surjection $\{0,1\}^\N\longtwoheadrightarrow A$, it inductively covers each $T_i$ by $\{0,1\}^\N$ via $\{0,1\}^\N\longtwoheadrightarrow A\longtwoheadrightarrow T_i$. As we never touch $T_\infty$ using this process, one gets the cover $(\{0,1\}^\N\times\N)\cup \left(T_\infty\times\{\infty\}\right)$.
Now as it is covered by $(\{0,1\}^\N\times\N)\cup \left(T_\infty\times\{\infty\}\right)$ (at least set theoretical, one would still need to check continuity) and the cover $(\{0,1\}^\N\times\N)\cup \left(T_\infty\times\{\infty\}\right)\longtwoheadrightarrow S\times\ninfty$ is constructed, s.t. it is induced by a map $\{0,1\}^\N\longtwoheadrightarrow S$ we are done. \qedhere
\end{description}
\end{proof}

\begin{remark}\leavevmode
\begin{enumerate}
\item In CondAb there are all products exact and it has projective generators $\Z[S]$\footnote{With $S = \beta I$, $I$ discrete, we mean the Stone-Čech compactification, i.e. $S$ is extremally disconnected.} , but none of them are internally projective as $\Z[\beta I\times\beta J]$ is never projective ($I, J$ infinite). One thing that is better in CondAb is that we do have projective generators, other than in $\lcondab$. Explicit reason that $\Z[\ninfty]$ is \underline{not} internally projective in CondAb:
\[
\begin{tikzcd}
{\Z[\beta\N]} \arrow[r, two heads, bend left] & {\Z[\ninfty]} \arrow[l, "\nexists", dotted, bend left]
\end{tikzcd}
\]
This is also related to certain questions of Banach spaces: The only known injective Banach spaces\footnote{There is some kind of duality, that projective objects in CondAb becomes injective in Banach spaces.} are $\textrm{Cont}(S,\R)$ with $S$ extremally disconnected. So basically Stone-Čech compactifications are retracts of it. It's known that there are Banach spaces which are retracts of continuous functions on a Stone-Čech compactification. $C_0(\N)$, the Banach space of null-sequences which corresponds to our $\Z[\ninfty]/\Z[\infty]$, is not injective but it is "separably injective", which corresponds to $\Z[\ninfty]$ being projective in $\lcondab$ but not in CondAb.
\item (AB6) holds for countable products in $\lcondab$.
\end{enumerate}
\end{remark}
\subsection{Cohomology}
If $X$ is any light condensed set, $M$ an abelian group:
\begin{definition}
$H^i(X,M):=\ext^i_\lcondab(\Z[X],M)$
\end{definition}
Here we mean by $M$ the light condensed abelian group $\underline{M}=\mathrm{Const}(-,M)$ where $M$ has discrete topology. In the following lectures we will use the notation $M$ and $\underline{M}$ interchangeably, as it is clear in context what is meant. 
\begin{theorem}
If $X$ is a CW complex, $H^i(\underline{X},M)\cong H^i_{\mathrm{sing}}(X,M)$.
\end{theorem}




