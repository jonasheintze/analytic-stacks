% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 1 (Clausen, 18.10.2023) -- {\it Motivation and Introduction}]{Lecture 1 (1/24)}

\subsection{Introduction}
Classically there are several different theories of analytic geometry:
\begin{description}
\item[1. Complex analytic spaces:]
In the smooth case complex manifolds.
\item[2. Locally analytic manifolds $(K, \norm{\cdot})$ complete valued field:]  The idea is to "glue open subsets of $K^d$ along locally analytic maps". When $K$ is non-archimedean (e.g. $K = \Q_p$) it is not geometrically rich because the topology is totally disconnected.
\item[3. Rigid analytic geometry (over a non-archimedean field):] Here the idea is to focus on local rings of functions instead of local topology. The local rings of functions are (quotients of) functions convergent on a closed polydisc (Tate Algebra). This was generalised by Huber to the theory of \textbf{adic spaces}. These still have local rings of functions $A$, but also include extra data of a certain subring $A^+ \subseteq A$.
\item[4. Berkovich Theory:] Local models are given by Banach rings $(R, \norm{\cdot}) \leadsto M(R, \norm{\cdot})$, where $M(R, \norm{\cdot})$ is a Berkovich space of multiplicative semi-norms, which are compact Hausdorff spaces. 
\end{description}

\subsection{Why produce a new theory?}
(1)-(4) and their relationships are well understood but they can't all be treated at the same time. Of all the above, the only rich theory allowing archimedean and non-archimedean geometry is Berkovich's, but it is not well worked out. Glueing is only worked out in some "finite type" setting. 

\noindent Even individually these theories are less flexible than e.g. the theory of schemes. One of the main problems is issues of descent: One main construction in the theory of schemes is QCoh($X$) which can be thought of as modules over a ring glued together to form a sheaf over $X$. We don't have such things in classical analytic geometry. Even though we could theoretically still have modules locally over a ring, this concept wouldn't be "global" as that wouldn't glue. This would only work with finitely generated modules which would lead to the theory of coherent sheaves.

\noindent There is a potentially practical reason, coming from the Langlands program: Fargues-Scholze "geometrized" local Langlands. This was done by replacing $\Q_p$ by a more exotic object, namely the "Fargue-Fontaine curve". This was produced in the language of adic spaces over $\Q_p$, which is not of finite type.

\noindent Speculatively, we could also hope for a geometrization of global Langlands by replacing $\Z$ by some exotic analytic space over $\Z$. Whatever such thing might be (if something like this exists), it would need archimedean and non-archimedean aspects and would not be of finite type as well. 

\noindent The goal of the course is to introduce a new theory of analytic geometry and show its relations to the old theory.

\subsection{Condensed Mathematics}
The local models classically are in fact topological rings and we need to remember the topology. But topological rings and modules are not suitable to a general theory\footnote{One might want to read the motivation in \cite{condensed}. One of the major flaws of topological abelian groups/rings/modules/... is that it's not an Abelian Category. Take e.g.
$0 \rightarrow  \mathbb{R}_{\textrm{discrete}} \rightarrow  \mathbb{R} \rightarrow  0$ which is exact but $\R_{\textrm{discrete}}\rightarrow\R$ is not an isomorphism. The workaround is to introduce some "condensed" notation whose categories will be Abelian in nice cases.}.  That's why we go to the basics and define a replacement for the category of topological spaces. The basic idea: instead of encoding a topological space $X$ traditionally, we single out a collection of "nice test spaces" $S$ and we just record the data of "continuous maps $S\rightarrow X$". Formally, the test spaces will be \textbf{profinite sets}\footnote{These can be defined as the inverse limit of finite sets with discrete topology. By Stone duality this is equivalent to compact totally disconnected topological spaces.} but this is a very large category.

\begin{definition} A \textbf{light profinite set} is a countable inverse limit of finite sets. A \textbf{light condensed set}\footnote{For those who read \cite{condensed}: With that category, we don't need to look at extremally disconnected condensed sets anymore. We will see in later lectures what this technically means.} is a sheaf of sets on the category of light profinite sets on the Grothendieck topology, where covers are finite families of surjective maps.
\end{definition}
\noindent More explicitly, a light condensed set is a functor  \[\textrm{LightProf}^\textrm{op}\rightarrow\textrm{Set}\] with these properties:
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item[1.] $X(\emptyset)=\emptyset$
	\item[2.] $X(S\sqcup T)\,\xrightarrow{\sim}\, X(S)\times X(T)$
	\item[3.] $\exists T \twoheadrightarrow S \Rightarrow X(S) = \textrm{eq}(X(T)\rightrightarrows X(T\times_S T))$
\end{enumerate}

\begin{example}
Every Topological space $X$ is by Yoneda a Condensed set via $X(S)=\cont(S,X)$.
\end{example}
\noindent Remarks on why we make this precise definition:

\begin{description}[style=unboxed,leftmargin=0.1cm]
\item[1.] For any light condensed set $X$, if we plug in $S =\ast$ we will get back the "underlying set". Note that if $X$ is a topological space we will just get back the set $X$ (we will still have to put in some work to get back the topology in special cases). If we plug in $S=\N\cup\{\infty\}$ we get something like " the space of convergent series  in $X$ with its limit".
\item[2.] Allowing \underline{all} surjections to count as covers gives a nice simplification of the structure of light condensed sets. It gives us some nice homological Algebra properties, if we pass it to light condensed abelian groups, since we have a lot of flexibility working locally with these covers. 
\item[3.] Requiring the Grothendieck topology to be finitary gives good categorical compact- ness properties for light profinite sets. We get these properties even for all metrizable compact Hausdorff spaces, e.g. \[\{0,1\}^\N \twoheadrightarrow[0,1]\]The compactness from Hausdorff spaces, which we know from general topology, now kind of translates into some categorical notation of compactness.
\end{description}

\subsection{Analytic Rings}
In the following "condensed sets/rings/..." mean "light condensed sets/rings/..." and rings are always commutative with unit. The definition of a condensed set gives rise to the definition of condensed rings and condensed modules over rings (sheaves of rings and sheaves of modules).
Why can't we use condensed rings (alone) as local models for our analytic geometry? 

\noindent\underline{Basic reason:} The category of condensed rings has pushouts given by relative tensor product just as for classical rings. Let $A,B,k$ be condensed rings: \[(A\otimes_k B)(\ast)=(A(\ast)\otimes_{k(\ast)} B(\ast))\] $\Rightarrow A\otimes_k B \textrm{ just gives a condensed structure on the abstract }-\otimes -$, so in particular it's not giving a completed tensor product, as the completion procedure does change the underlying set.

\noindent To fix this, we put additional structure on a condensed ring: We record some class of condensed modules, to be considered as "complete". That would give the notion of Analytic ring.

\noindent But what kind of rings do we want? Experience in algebraic geometry shows that the generally correct notion of a fibre product is actually the derived fibre product, which on affine level corresponds to derived relative tensor products of rings. So our rings should be derived, but which kind? There are two basic options: 
\begin{enumerate} [topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item \textbf{$\textrm{E}_\infty$-algebras}
	\item \textbf{Animated commutative rings (presented by simplicial commutative rings)}
\end{enumerate}
For the course we will not allow negative homotopy and use the second option, as it is more closely tied to algebraic geometry. Formally, a \textbf{condensed animated ring} is a hypersheaf of animated rings on the site of profinite sets. From now on, another convention for today's lecture is, that by "ring" we mean "animated ring". If we want to stress that the ring lives in degree 0, it will be denoted as classical/static ring. Most of the time, one can just pretend, that everything is an ordinary ring but that's not the general case. The basic invariant of such $R$ is its derived category $D(R)$. If $R$ is static, this is the usual unbounded derived category of the abelian category of condensed R-modules. In particular it has a t-structure as a notion of connected objects and anti-connected objects. In general you still have a t-structure but it's not the derived category of $R$-modules.
\begin{definition}
An analytic ring is a pair $R=(R^\rhd,D(R))$ where $R^\rhd$ is a condensed ring and $D(R)\subseteq D(R^\rhd)$ such that
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1.5ex]%[style=unboxed,leftmargin=0.1cm]
\item[1.] $D(R)$ is closed under all limits and colimits.
\item[2.] If $N\in D(R), M \in D(R^\rhd)$, then $\underline{R\textrm{Hom}}_{R^\rhd}(M,N)\in D(R)$.
\item[3.] If $\widehat{(-)}_R$ denotes the left adjoint to the inclusion $D(R)\subseteq D(R^\rhd)$, then $(-)_{\hat{R}}$ sends $D(R^\rhd)_{\geq 0}\rightarrow D(R)_{\geq 0}$.
\item[4.] $R^\rhd\in D(R)$.
\end{enumerate}
A map of analytic rings $R\rightarrow S$ is a map of condensed rings $R^\rhd\rightarrow S^\rhd$ s.t. $D(S^\rhd)\rightarrow D(R^\rhd)$ restricts to $D(S)\rightarrow D(R)$.
\end{definition}

\begin{remark}
There is always a $t$-structure on $D(R)$:\[D(R)_{\geq 0}=D(R)\cap D(R^\rhd)_{\geq 0}\]\[D(R)_{\leq 0}= D(R)\cap D(R^\rhd)_{\leq 0}\]\[\Rightarrow\textrm{We get an abelian category }D(R)^\heart=D(R)\cap D(R^\rhd)^\heart\]
\end{remark}
\noindent For a profinite set $S$, consider the free $R$-module $R[S]:= \widehat{(R^\rhd[S])}_R$. These generate $D(R)_{\geq 0}$ under colimits.
\paragraph{Intuition:} 

$R^\rhd[S]=$ "space of  $R^\rhd$-linear combinations of dirac measures on $S$"

$R[S]=$ "some completion, i.e. a bigger space of measures"

\paragraph{Rule:} $M\in D(R^\rhd)^\heart$ lies in $D(R)^\heart \iff$ 
\begin{tikzcd}
           & \forall f: {R^\rhd[S]} \arrow[rd] \arrow[rr] &                                         & M \\
&\exists !\textrm{ extension along}    & {R[S]} \arrow[ru, "\exists !"', dotted] &  
\end{tikzcd}

\noindent In other words, if $f:S\rightarrow M$ and $\mu\in R[S]$ then we get a well-defined $\int_{S}fd\mu \in M$.

\paragraph{Colimits in analytic rings:} Filtered colimits (more generally sifted colimits):

\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
\item $(\underset{i}{\varinjlim} R_i)^\rhd=\underset{i}{\varinjlim} (R_i^\rhd)$
\item $(\underset{i}{\varinjlim} R_i)[S]=\underset{i}{\varinjlim} (R_i[S])$
\end{enumerate}

\paragraph{Pushouts in analytic rings:} For maps of analytic rings $A\leftarrow k\rightarrow B$, we have that $D(A\otimes_k B)\subseteq D(A^\rhd\otimes_{k^\rhd}B^\rhd)$ is the full subcategory, s.t. the underlying $A^\rhd$-module lies in $D(A)$ and the underlying $B^\rhd$-module lies in $D(B)$.

\begin{warning}  $(A^\rhd\otimes_{k^\rhd}B^\rhd, D(A\otimes_k B))$ is \textit{not} an analytic ring. It satisfies (1)-(3), but not (4). To fix this, apply left adjoint to $D(A\otimes_k B)\subset D(A^\rhd\otimes_{k^\rhd}B^\rhd)$.
\end{warning}

\noindent\textbf{Solid analytic rings} ($\leadsto$ Adic spaces)\\
For an analytic ring $R$ it is natural to consider $M_R(\N)=R[\N\cup\infty]/R[\infty]$ which classifies nullsequences in $R$-modules. It turns out that it is not hard to show, that addition on $\N$ induces a ring structure on $M_R(\N)$. As a ring it sits between\footnote{Here one might want to think of $M_R(\N)$ as $T^\N$.} \[R[T]\rightarrow M_R(\N)\rightarrow R\llbracket T\rrbracket.\]
\begin{definition} R is \textbf{solid} if $M_R(\N)/(T-1)= 0$
\end{definition}
An interpretation of this, is if you have a measure $\mu\in M$ s.t. $(T-1)\mu=1\in M_R(\N)$ then $\mu$ corresponds to something like "$\sum_n T^n$". One would have to work it out but this means something like "every null-sequence is summable" which is a classic non-archimedean condition. 

\begin{theorem}
There exists a solid analytic ring $Z^\square=(\Z,D(\Z^\square))$ s.t. an analytic ring is solid $\iff\exists$ (necessarily unique) map $\Z^\square\rightarrow R$. Moreover for $S=\underset{n}{\varprojlim}\, (S_n)$ and $S$ infinite:
\begin{enumerate}
	\item[1.] $\Z^\square(S)=\underset{n}{\varprojlim}\,\Z[S_n]$ \indent $(\cong\prod_I \Z$ with $I$ countable) 
	\item[2.]
		\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
			\item $\prod_I \Z$ are the compact projective generators of $D(\Z^\square)^\heart$
			\item $\prod_I \Z$ is flat w.r.t. the $(-\otimes_{\Z^\square}-)$ 
			\item $\prod_I\Z\otimes_{\Z^\square}\prod_J\Z=\prod_{I\times J}\Z$ (One can find this in Corollary \ref{5.10}.)
		\end{enumerate}
	\item[3.] The collection of finitely presented objects in $D(\Z)^\heart$ is abelian, closed under extensions and every finitely presented $M$ has a free resolution by $\prod_I\Z$'s of length\footnote{One can interpret this as "$\Z^\square$ behaves like a regular ring of dim 2".} $\leq 2$.
\end{enumerate}
\end{theorem}




