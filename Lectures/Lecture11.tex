% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 11 (Clausen, 26.11.2023) -- {\it Solid Miscellany}]{Lecture 11 (11/24)}
\subsection*{Recall}
For a pair $(A^\rhd,A^+)$ with a solid ring $A^\rhd$ and a subring of $A^\circ\subseteq A^\rhd(*)$ we can associate this to a certain analytic ring \[\module_{(A^\rhd,A^+)_\square}\subseteq\module_{A^\rhd}(\solid_\Z)\] by enforcing the elements of $A^+$ to become solidified variables. We also defined 
\[D(A^\rhd,A^+)_\square\subseteq D\bigl(\module_{A^\rhd}(\solid_\Z)\bigr)\] 
as the full subcategory of $D\bigl(\module_{A^\rhd}(\solid_\Z)\bigr)$, where all homologies of elements $M\in D(A^\rhd,A^+)_\square$ live in $\module_{(A^\rhd,A^+)}$. For both inclusions we have left adjoints. Furthermore we have a compact projective generator \[
  \underbrace{\prod_\N \Z}_{\text{flat in } \Z_\square} \otimes_{\Z_\square}A^\rhd  \in \module_{A^\rhd}(\solid_\Z)
\] and a compact generator
\begin{equation}\label{eq:compgen}
\Bigl(\prod\Z\otimes_{\Z_\square}A^\rhd\Bigr)^{A^+L\square} \in D(A^\rhd,A^+)_\square 
\end{equation}
\begin{lemma}\label{known}
    Recall that \begin{description}      \item[-]$\bigl(M\otimes_\Z\Z[T]\bigr)^{\Z[T]\square}$ commutes with limits in $M$ and sends $\Z\mapsto \Z[T]$
        \item[-] 
$M^{T_1,T_2\square}=\bigl(M^{T_1\square}\bigr)^{T_2\square}=\bigl(M^{T_2\square}\bigr)^{T_1\square}$
    \end{description}
\end{lemma}
\begin{theorem}\label{deg}
    The compact generator (\ref{eq:compgen}) lives in degree $0$. Hence it is a compact projective generator for $\module_{(A^\rhd,A^+)_\square}$ and $D\bigl(\module_{(A^\rhd,A^+)_\square}\bigr)= D\bigl(A^\rhd,A^+\bigr)_\square$.\footnote{This is not contrary to Warning \ref{war:der} as this is only a subclass of analytic rings.}
\end{theorem}
\begin{lemma} \[
        M^{L A^+\square}= \colim_{\substack{R\subseteq A^+\\ R \text{ finite type over }\Z }}  M^{L R\square}=: N
\]which is a filtered colimit.\end{lemma}
\begin{proof}
There are two things to check: \begin{enumerate}
    \item $N$ is derived $A^+$-solid:\\[1.25ex]
    \noindent We have that derived $A^+$-solid is the same as derived $R$-solid $\forall R\subseteq A^+$ of finite type. So certainly, for fixed $R$, the $R$-th term of the filtered colimit is $R$-solid, but also every further term is derived $R$-solid, as it has the stronger property of being $R'$-solid with $R\subseteq R'$. So there is a cofinal system of $R$-solid modules in the filtered colimit and a filtered colimit of $R$-solids is $R$-solid.
    \item $\hom_{(A,A^+)_\square}(M, L)=\hom_{(A,A^+)_\square}(N, L)$ for all $L$ derived $A^+$-solid:\\[1.25ex]
    By the same fact as before a $A^+$-solid map is the same as a map of $R$-solids for every $R\subseteq A^+$ of finite type. Then the statement follows by a similar argument.\qedhere
\end{enumerate}\end{proof}
\begin{proof}[Proof of Theorem \ref{deg}.]
It now suffices to show\[
    \left(\prod\Z\otimes_{\Z_\square} A\right)^{L R\square}  
\] lives in degree $0$. As \[\left(\prod\Z\otimes_{\Z_\square} A\right)^{L R\square} = \left(\prod\Z\otimes_{\Z_\square}R\right)^{L R\square}\otimes^L_{R_\square}A\]
it suffices to see: 

\begin{center}$(\prod\Z\otimes_{\Z_\square}R)^{L R\square}$ lives in degree $0$ and it is flat with respect to $\otimes_{R_\square}$. \end{center}
The flatness part is going to be proved in Lemma \ref{lem:flat}.\\
\noindent\underline{Claim:} For $R$ of finite type\footnote{If it is not of finite type, then it would be equal to 
$\underset{{\substack{R'\subseteq R\\\text{finite type}}}}{\bigcup}\prod R'$.} over $\Z$:\[
    \left(\prod_\N\Z\otimes_{\Z_\square}\R \right)^{L R\square}= \prod_\N R
\]
\begin{adjustwidth}{1cm}{}
    \textit{Proof of Claim.} First consider $R=\Z[x_1,\dots, x_n]$. Now by Lemma \ref{known}:
    \begin{align*}
\prod_\N\Z\otimes_{\Z_\square}\Z[x_1,\dots,x_n]^{Lx_1,\dots,x_n\square} & = \Bigl(\prod_\N\Z\otimes_{\Z_\square}\Z[x_1,\dots,x_n]^{Lx_1\square}\Bigr)^{Lx_2,\dots,x_n\square}\\ 
& =\Bigl(\prod\Z[x_1]\otimes_{\Z[x_1]_\square}\Z[x_2,\dots,x_n]\Bigr)^{Lx_2,\dots,x_n\square} \\ &=\dots\\
    &=\prod\Z[x_1,\dots,x_n]
    \end{align*}
    General case: $\Z[x_1,\dots,x_n]\twoheadrightarrow R$\begin{align*}
    \Bigl(\prod\Z\otimes_{\Z_\square}R\Bigr)^{LR\square} &=\Bigl(\prod\Z\otimes_{\Z_\square}\Z[T_1,\dots,T_n]\Bigr)^{L\Z[T_1,\dots,T_n]\square}\otimes^L_{\Z[T_1,\dots,T_n]}R\\ &= \Bigl(\prod\Z[T_1,\dots,T_n]\Bigr)\otimes^L_{\Z[T_1,\dots, T_n]}R
    \end{align*}
    As $\Z[T_1,\dots,T_n]$ is noetherian, $R$ can be resolved by finite free $\Z[T_1,\dots,T_n]$-modules. Hence \[
        \prod\Z[x_1,\dots,x_n]\otimes^L_{\Z[x_1,\dots,x_n]}R\xrightarrow{\sim}\prod R
    \]
\end{adjustwidth}
\end{proof}
\begin{definition}
    For an arbitrary commutative ring $R$ we define $\solid^{f.p.}_R$ as the category of cokernels of maps\footnote{Compare this definition to Definition \ref{def:finpres}!} $\prod R\rightarrow \prod R$.
\end{definition}
\begin{corollary}
    Let $R$ be an arbitrary commutative ring. Then \[
        \solid_R\overset{\mathrm{def}}=\solid_{(R,R)}= \ind(\solid_R^{\text{f.p.}})
    \] and \[
    \solid_R^{\text{f.p.}}=\colim_{\substack{R'\subseteq R\\\text{finite type}}}\solid_{R'}^{\text{f.p.}}    
    \]
\end{corollary}

\subsection{Structure of Solid\texorpdfstring{${_R}$}{Solid R}}
\begin{lemma}\label{lem:eq}
    If $M\in\solid_R$ is quasiseparated, then TFAE:
    \begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
        \item \label{lem:eq:1}$M$ is finitely presented
        \item \label{lem:eq:2}$M$ is finitely generated
        \item \label{lem:eq:3}$M=\varprojlim_n M_n$, $M_n$ finitely generated $R$-module, $M_{n+1}\twoheadrightarrow M_n$
    \end{enumerate}
\end{lemma}
\begin{proof}
    "\ref{lem:eq:1}. $\Rightarrow$ \ref{lem:eq:2}.": clear.\\
    "\ref{lem:eq:2}. $\Rightarrow$ \ref{lem:eq:3}.": By definition we have an exact sequence \[
        0\rightarrow K\rightarrow \prod R \twoheadrightarrow M\rightarrow 0    . 
    \]
    Since $M$ is quasiseparated, $K\rightarrow \prod R$ is a quasicompact map. As $\prod R$ is metrizable, by Lemma \ref{lem:seq} this is the same as a closed subset in $\prod R$ with product topology. Now by defining $K_n$ as the image of \[K\rightarrow\prod R\rightarrow \prod_{i\leq n} R,\] one gets that $K\xrightarrow{\sim}\varprojlim K_n$ as in \ref{lem:eq:3}. . Furthermore we get that $M=\varprojlim \prod_{i\leq n} R/K_n$.\\
    "\ref{lem:eq:3}. $\Rightarrow$ \ref{lem:eq:1}.": It suffices to prove "\ref{lem:eq:3}. $\Rightarrow$ \ref{lem:eq:2}.", as if $M$ is finitely generated, then we can get again get an exact sequence \[
        0\rightarrow K\rightarrow \prod R \twoheadrightarrow M\rightarrow 0
    \] and by the proof before, we see that if $M$ is finitely generated, then the kernel is also of the form in \ref{lem:eq:3}. which then is also finitely generated which gives \footnote{The map $\prod R\twoheadrightarrow K$ , where $K$ is the kernel of $\prod R\twoheadrightarrow M$, induces the exact sequence $\prod R\rightarrow \prod R\rightarrow M\rightarrow 0$} us \ref{lem:eq:1}. Now for sufficiently big $d_n$ one gets maps:\[
        \begin{tikzcd}
            R^{\oplus d_3} \arrow[r, two heads]  & \ker(f_2) \arrow[r] & M_3 \arrow[d, "f_2", two heads] \\
            R^{\oplus d_2} \arrow[r, two heads]  & \ker(f_1) \arrow[r] & M_2 \arrow[d, "f_1", two heads] \\
            R^{\oplus d_1} \arrow[rr, two heads] &                     & M_1                            
            \end{tikzcd}
    \]This induces:\[\begin{tikzcd}
        \cdots \arrow[d, two heads]                                          & \cdots \arrow[d, two heads]         \\
        R^{\oplus d_1+d_2+d_3} \arrow[r, two heads] \arrow[d, two heads] & M_3 \arrow[d, "f_2", two heads] \\
        R^{\oplus d_1+d_2} \arrow[r, two heads] \arrow[d, two heads]     & M_2 \arrow[d, "f_1", two heads] \\
        R^{\oplus d_1} \arrow[r, two heads]                              & M_1                            
        \end{tikzcd}\]
        taking the limit of this diagram we get a map $\prod R\twoheadrightarrow M$.
\end{proof}
\begin{theorem}\label{theo:coh}
    Let $R$ be a ring of finite type\footnote{Finite type is used in the proof, in order that $\prod R$ is the projective generator.} over $\Z$. Then we have "Coherence": $\solid^{f.p.}_R$ is closed under $\ker, \coker$ and extensions.
 
\end{theorem}
\begin{reminder*}
    A classical commutative ring is called \textbf{coherent} if the finitely presented $R-$modules form an Abelian subcategory. By some short exact sequences one can show, that one only needs to check, that every finitely generated ideal is actually finitely presented, in order to prove that a ring is coherent.
\end{reminder*}
\begin{proof}[Proof of Theorem.]
By the same arguments as in the classical case it suffices to check that every finitely generated subobject of $\prod R$ is finitely presented. Note, that every subobject of $\prod R$ is quasiseparated, as $\prod R$ is quasiseparated. So now we only need to show, that every finitely generated quasiseparated module is finitely presented. This follows from Lemma \ref{lem:eq}.
\end{proof}
\begin{lemma}\label{lem:flat}
    If $M$ is finitely presented in $\solid_R$, we get \[
        M\otimes^L_{R^\square}\prod R\xrightarrow{\sim}\prod M
    \] In particular $M\otimes^L_{R^\square}\prod R$ lives in degree $0$.
\end{lemma}
\begin{remark}
    This implies that $\prod R$ is flat with respect to $\otimes_{R^\square}$, as  any object is a filtered colimit of finitely presented objects, hence for any $M$, $M\otimes^L_{R^\square}\prod R$ lives in degree $0$.
\end{remark}
\begin{proof}
    As $M$ is finitely presented, we have the surjection $\prod_R\rightarrow M\rightarrow 0$, where the kernel is finitely generated. As the kernel is also quasiseparated, it will also be finitely presented. This way we get an infinite resolution\[
        \cdots\rightarrow \prod R\rightarrow \prod R\rightarrow M\rightarrow 0
    \]
    This reduces us to the case $M=\prod R$, i.e. we need to show that $\prod R \otimes_{R^\square}^L \prod R=\prod R$. As $\prod R = \bigl(\prod \Z\otimes_{\Z^\square} R\bigr)^{R\square}$ the fact descends to the analogous fact for $\prod \Z$, which was proven in Corollary \ref{5.10}.
\end{proof}