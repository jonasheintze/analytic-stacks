% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 5 (Scholze, 08.11.2023) -- {\it Solid Abelian Groups}]{Lecture 5 (5/24)}
\subsection{Solid Abelian Groups}


The goal is to isolate a class of "complete" objects in $\lcondab$. For this lecture, whenever we write Hom or $\ext^i$ we mean $\mathrm{Hom}_\lcondab$ or $\ext^i_\lcondab$
\[
    \Z\llbracket T \rrbracket\otimes_{\mathrm{Ab}}\Z\llbracket T \rrbracket\cong\left(\underline{\Z}\llbracket T \rrbracket\otimes_\lcondab \underline{\Z}\llbracket T \rrbracket\right)(*)\neq \Z\llbracket T,U\rrbracket
\]
\textbf{Idea:} Form completed tensor products instead. \\
\textbf{Want:} "completeness" defines an abelian category: \[
    \begin{tikzcd}
    0 \arrow[r] & \underline{\Z} \arrow[r] & \underline{\Z}_p \arrow[r] & \underline{\Z}_p/\underline{\Z} \arrow[r] & 0
    \end{tikzcd}
    \]
Turns out it is difficult to find a notion for which $\underline{\R}$ is complete, but there is a theory that works well in the non-archimedean context. \\
\textbf{Idea:} $M\in\lcondab$ should be "non-archimedean complete" if any null sequence in $M$ is summable.\\
\textbf{Formalization:} Consider $P:= \Z[\ninfty]/\Z[\infty]\in \lcondab$ the "free condensed abelian group on nullsequences". Recall $P$ is internally projective. 
\begin{align*}
    \underline{\mathrm{Hom}}(P,-):\; \lcondab&\longrightarrow \,\lcondab\\
     M &\longmapsto \left(S\mapsto\mathrm{Hom}(P\otimes\Z[S], M)\right)
\end{align*}
is exact and preserves all limits and colimits. 
Consider 
\begin{align*}
    f= id-\text{shift}:\; P&\longrightarrow P\\
                        [n]&\longmapsto[n]-[n+1]
\end{align*}
\begin{definition}\label{5.1}
    $M\in\lcondab$ is \textbf{solid} if 
    \begin{align*}
        f^*: \underline{\mathrm{Hom}}(P,M)&\overset{\sim}\longrightarrow\underline{\mathrm{Hom}}(P,M)\\
        (m_0,m_1,...)&\longmapsto(m_0-m_1,m_1-m_2,...)\\
        "\left(\sum_{i=0}^\infty m_i, \sum_{i=1}^\infty m_i,...\right)"& \;\reflectbox{$\longmapsto$}\;(m_0,m_1,...)
    \end{align*}
\end{definition}
We see from that definition, that "null sequences" are summable, which is a "non-archimedean property".
\begin{proposition}
    $\solid\subset\lcondab$ is an abelian subcategory, which is stable under kernels, cokernels, extensions, all (co)limits, $\underline{\mathrm{Hom}}$, $\underline{\ext}^i$ and contains $\underline{\Z}$. It is also not equal to $\lcondab$, e.g. $\underline{\R}\notin\solid$.
\end{proposition}
\begin{remark}
    This Proposition says something like "Solid has an analytic ring structure on $\underline{\Z}$".
\end{remark}
\begin{proof}
    All, but \underline{Hom}, $\underline{\ext}^i$ and $\underline{\Z}$ for free because $\underline{\mathrm{Hom}}(P,-)$ is exact, i.e. because $P$ is internally projective and compact. 

    \noindent\textbf{Stability for internal \underline{Hom}:}\\ $M$ solid, $N\in\lcondab$, we want $\underline{\mathrm{Hom}}(N,M)$ solid, i.e. $f^*$ is an isomorphism: \[
    \begin{tikzcd}
        f^*         &[-18pt] {} \arrow[loop, distance=2em, in=215, out=145] &[-35pt] {\underline{\mathrm{Hom}}\left(P,\underline{\mathrm{Hom}}(N,M)\right)} & [-33pt]{\cong \underline{\mathrm{Hom}}(P\otimes N,M)} &[-33pt] {\cong \underline{\mathrm{Hom}}\left(N,\underline{\mathrm{Hom}}(P,M)\right)}. \\[-25pt]
        \mathrm{iso?} &                                                &                                                                    &                                              &               
        \end{tikzcd}
        \] As $f^*$ is now only acting on $\mathrm{Hom}(P,M)$ we are done, as $M$ is solid.

    \noindent\textbf{Stability for internal $\boldsymbol{\underline{\ext}^i}$:}
        As $\underline{\mathrm{Hom}}(P,-)$ is exact:
          \[\underline{\mathrm{Hom}}\left(P,\underline{\ext}^i(N,M)\right)=\underline{\ext}^i(P\otimes N, M)\cong \underline{\ext}^i(N,\underline{\mathrm{Hom}}(P,M)).\] Now we are done with the same argument as above.
        
        \noindent\textbf{$\underline{\Z}$ is solid:}
            \[\begin{tikzcd}
                {\underline{\mathrm{Hom}}(P,\underline{\Z})} & [-33]=\bigoplus_\N\Z &[-33pt] {} \arrow[loop, distance=2em, in=35, out=325] &[-17pt] id-\text{shift is iso}
            \end{tikzcd}\]
        
\end{proof}
\begin{corollary}\label{5.4}
    There is a left adjoint to $\solid\longhookrightarrow \lcondab$, 
    \begin{align*}
        \lcondab&\longrightarrow\solid\\
        M&\longmapsto M^\square
    \end{align*}
    called solidification. $\forall\, N\in\mathrm{ Solid}: \mathrm{ Hom}_{\lcondab}(M,N)= \mathrm{ Hom}_{\solid}\left(M^\square,N\right).$\\
    Moreover Solid acquires a unique colimit preserving symmetric monoidal product $\otimes^\square$ making $M\longmapsto M^\square$ a symmetric monoidal functor. 
\end{corollary}
\begin{proof}[Sketch of Proof]\leavevmode
    \paragraph{Existence} of $M\longmapsto M^\square$ is by an adjoint functor theorem. 

    \paragraph{Symmetric monoidal $\otimes^\square$:} Define $M\otimes^\square N:=\left(M\otimes N\right)^\square$. We want:
    
    \[ \forall\, N,M\in \lcondab:\, \left(M\otimes N\right)^\square \overset{\sim}\longrightarrow\left(M^\square\otimes N^\square\right)^\square\]
    \underline{Check:} $\forall S\in$ Solid, $\mathrm{Hom}(-,S)$ agree. We proceed:
    \[
        \begin{tikzcd}
            {\mathrm{Hom}(M\otimes N,S)} \arrow[d, "\cong", phantom] \arrow[r, "\cong", phantom] & {\mathrm{Hom}\left(M^\square\otimes N^\square, S\right)} \arrow[d, "\cong", phantom]                  \\
            {\mathrm{Hom}\left(N,\underline{\mathrm{Hom}}(M,S)\right)} \arrow[d, "\cong", phantom]            & {\mathrm{Hom}\left(M^\square,\underline{\mathrm{Hom}}(N^\square, S)\right)} \arrow[d, "\cong", phantom] \\
            {\mathrm{Hom}\left(N^\square, \underline{\mathrm{Hom}}(M,S)\right)} \arrow[r, "\cong", phantom]   &\underline{\mathrm{Hom}}\left(M,\underline{\mathrm{Hom}}(N^\square,S)\right)           
            \end{tikzcd}    
    \] where $\underline{\mathrm{Hom}}(M,S)$ and $\underline{\mathrm{Hom}}(N^\square, S)$ are both solid. Hence all these equations hold.
\end{proof}
\begin{lemma}
    $\underline{\R}^\square=0.$
\end{lemma}
\begin{proof}
    $\underline{\R}^\square$ is a ring, so it is enough to show $1=0\in\underline{\R}^\square$. Consider the null sequence \[1, \frac{1}{2},\frac{1}{2}, \frac{1}{4},\frac{1}{4},\frac{1}{4},\frac{1}{4},\frac{1}{8},...\] which is a map $g: P\rightarrow \underline{\R}$. Consider the diagram:
    \[
    \begin{tikzcd}
        & P \arrow[r, "g"]                                     & \underline{\R} \arrow[d] \\
\underline{\Z} \arrow[r, "{[0]}"] & P \arrow[u, "f"] \arrow[r, "\exists!"', dotted] & \underline{\R}^\square  
\end{tikzcd}\] where $1 \in\Z$ has the image $1=:x\in\underline{\R}^\square, \,$ which corresponds on an intuitive level to $"1+ \frac{1}{2}+\frac{1}{2}+ \frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{8}+..."$, as we map $[0]$ to some $m_0\in\underline{\R}^\square$ and now by the backwards map in \ref{5.1}, it should have this form going over $f\circ g$.\\
\underline{Claim:} $x=1+x$ (Hence it follows that $1=0$.)
    \begin{align*}
        x &= 1+ \frac{1}{2}+\frac{1}{2}+ \frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{8}+\cdots\\
        &=1+
        \biggl(\underbrace{\frac{1}{2}+\frac{1}{2}}_{=1}+ \underbrace{\frac{1}{4}+\frac{1}{4}}_{=\frac{1}{2}}+\underbrace{\frac{1}{4}+\frac{1}{4}}_{=\frac{1}{2}}+\cdots\biggr)\\
        &= 1 + \left( 1+ \frac{1}{2}+\frac{1}{2}+ \frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\frac{1}{4}+\cdots\right)= 1+x
    \end{align*}
\end{proof}
\begin{corollary}\label{Rmodule}
    If $M\in\lcondab$ admits an $\underline{\R}$-module structure then $M^\square=0$ (even $\ext^i(M,\solid)=0$).
\end{corollary}
\begin{proof}
     \[\cdots\longrightarrow M\otimes\underline{\R}\otimes\underline{\R}\longrightarrow M\otimes\underline{\R}\longrightarrow M\longrightarrow 0 \]
    is exact. As solidifying is right exact, we get: \[
        0=M^\square\otimes^\square\underline{\R}^\square = (M\otimes \underline{\R})^\square\longtwoheadrightarrow M^\square
    \] since $\underline{\R}^\square=0$. \\
    Observation: $\underline{\ext}^i(M,\solid)$ will be $\underline{\R}^\square$-modules as it clearly has $\underline{\R}-$module structure and internal $\underline{\ext}^i$ into something solid is solid itself.
\end{proof}
\paragraph{Goal:} Compute $P^\square$
\begin{lemma}
    Let $\prod_\N^\mathrm{bdd}\Z:= \bigcup_{n\in\N}\prod_{\N}\left(\Z\cap[-n,n]\right)$. There is the map \begin{align*}
        P &\longrightarrow {\prod_{\N}}^{\mathrm{bdd}}\Z\\
        [n]&\longmapsto (0,...,0,1,0,...)
    \end{align*} where the $1-$entry is the $n-$th coordinate. Then\[
        P^\square\overset{\sim}\longrightarrow \left({\prod_\N}^{\mathrm{bdd}}\Z\right)^\square.
    \]
    Even better: $\forall\, M$ solid, $\ext^i\left(P,M\right)\overset{\sim}\longleftarrow\ext^i\left(\prod_\N^{\mathrm{bdd}}\Z, M\right)$.
\end{lemma}
\begin{proof}
    Consider the diagram \[
        \begin{tikzcd}
            P\otimes\prod_\N^{\mathrm{bdd}}\;\Z \arrow[r, "g", two heads]          & \prod_\N^{\mathrm{bdd}}\;\Z \\
            P\otimes\prod_\N^{\mathrm{bdd}}\;\Z \arrow[r, dotted] \arrow[ u, "f\otimes id"] & P \arrow[u, hook]        
            \end{tikzcd}
    \]Here, $g$ corresponds to a null sequence of maps \[\begin{tikzcd}
        \prod_\N^{\mathrm{bdd}}\Z &[-33pt] {} \arrow[loop, distance=2em, in=35, out=325]
        \end{tikzcd},\] which are "projections to coordinates $\geq n"$. (This is again essentially \ref{5.1}.)\\ The composition $P\otimes\prod_{\N}^\mathrm{bdd}\;\Z\longrightarrow \prod_\N^{\mathrm{bdd}}\;\Z$ corresponds to the null sequence of maps \[\begin{tikzcd}
            \prod_\N^{\mathrm{bdd}}\Z &[-33pt] {} \arrow[loop, distance=2em, in=35, out=325]
            \end{tikzcd},\] which are "projections to the $n-$th coordinate". All of these factor over $P$, hence we get the dotted arrow.\\
            Now solidify:
            \[
            \begin{tikzcd}
                \left(P\otimes\prod_\N^{\mathrm{bdd}}\;\Z\right)^\square\arrow[r, two heads]          & \left(\prod_\N^{\mathrm{bdd}}\;\Z\right)^\square \\
                \left(P\otimes\prod_\N^{\mathrm{bdd}}\;\Z\right)^\square \arrow[r] \arrow[ u, "(f\otimes id)^\square"] & P^\square \arrow[u, hook, "h^\square"]        
                \end{tikzcd}
                \]
                Now $\left(f\otimes id\right)^\square$ is an isomorphism, since $f$ solidifies to an isomorphism by definition of solid and solidification is symmetric monoidal, hence as both $f$ and $id$ solidfy to isomorphisms, the whole map is an isomorphism. This means that $h^\square$ is an split surjection! Also, resulting idempotent of $P^{\square}$ is identity. Now we only need to show by a diagram chase, that going one "round" around the diagram is the identity on $P^\square$, which we will not do here. We have essentially the same argument for $\ext^i(-,\solid)$.
\end{proof}
\begin{lemma}
    $\left(\prod_\N^\mathrm{bdd}\;\Z\right)^\square\overset{\sim}\longrightarrow\Bigl(\prod_\N\;\Z\Bigr)^\square=\prod_\N\Z$ and \[\ext^i\left({\prod_\N}^\mathrm{bdd}\;\Z, \text{ solid}\right)\overset{\sim}\longleftarrow\ext^i\left(\prod_\N\Z,\text{ solid}\right)\]
\end{lemma}
\begin{proof}
    Consider the sequence $0\longrightarrow\prod_\N^\mathrm{bdd}\Z\longrightarrow\prod_{\N}\Z\longrightarrow\prod_\N\Z/\prod_\N^\mathrm{bdd}\Z\longrightarrow 0$. \\We have to see: $\forall i\geq 0$, $M$ solid: \[
        \ext^i\left({\prod_\N\Z/{\prod_\N}^\mathrm{bdd}\Z,M}\right)=0
    \]
    \underline{Claim:} $\prod_\N \Z/\prod_\N^\mathrm{bdd}\Z$ is a $\underline{\R}$-module!\\ A different way to write this is $\prod_\N \Z/\prod_\N^\mathrm{bdd}\Z=\prod_\N \underline{\R}/\prod_\N^\mathrm{bdd}\underline{\R}$, as the ratio between $\prod_\N\Z$ and $\prod_\N\underline{\R}$ is $\prod_\N\underline{\R}/\Z$ and the ratio between $\prod_\N^\mathrm{bdd}\Z$ and $\prod_\N^\mathrm{bdd}\underline{\R}$ is the same, as $\prod_\N^\mathrm{bdd}\underline{\R}$ surjects onto $\prod_\N\underline{\R}/\Z$ with kernel $\prod_\N^\mathrm{bdd}\Z$. Now we are done by \ref{Rmodule}.
\end{proof}
\begin{corollary}
    $P^\square\overset{\sim}\longrightarrow\prod_{\N}\Z$ and for all solid $M$, 
    $\ext^i(P,M)\overset{\sim}\longleftarrow\ext^i
    \left(\prod_\N\Z,M\right)$ for $i>0$ \[
    \ext^i\left(\prod_\N\Z, M\right)=\begin{cases}
        \bigoplus_\N M & i=0\\
        0 & i>0 
    \end{cases}    
    \]  $\Rightarrow \prod_\N\Z$ is compact and projective.
\end{corollary}
\begin{proposition}\label{prop:5.11}
    Let $S$ be any infinite light profinite set. Then $\exists$ map \[P\longrightarrow\Z[S]\] inducing an isomorphism \[P^\square\overset{\sim}\longrightarrow\Z[S]^\square\] and an also an isomorphism \[\ext^i(P, \solid)\overset{\sim}\longleftarrow \ext^i(\Z[S], \solid)\]
        $\Longrightarrow \Z[S]^\square\cong\prod_\N\Z$\\ Canonically: $\Z[S]^\square\overset{\sim}{\longrightarrow}\varprojlim\Z[S_n]$, where $S=\varprojlim S_n$.
\end{proposition}
\begin{proof}[Proof Sketch]
    $S\longtwoheadrightarrow...\longtwoheadrightarrow...\longtwoheadrightarrow S_2 \longtwoheadrightarrow S_1 \longtwoheadrightarrow S_0$\\
    Inductively choose: \begin{description}
        \item[-] section $i_0: S_0\longhookrightarrow S$.
        \item[-] on all elements of $S_1$ that are not in image of $i_0(S_0)$ choose a section $i_1^*$ on $S_1\setminus S_0\longrightarrow S$. Now we get a map $i_1 :=i_0\sqcup i_1^*:\, S_1\longrightarrow S$.
    \end{description}
    Enumerate $\N=S_0\sqcup(S_1\setminus S_2)\sqcup(S_2\setminus S_1)\sqcup...$. We get a map \[
        g: P\longrightarrow \Z[S]
        \]
    which is on $S_0$ given by $i_0$ and on $S_n\setminus S_{n-1}$ given by the difference of maps induced by $i_n$ and $S_n\setminus S_{n-1}\longrightarrow S_{n-1}\xrightarrow{i_{n-1}}S.$\[\begin{tikzcd}
        {P\otimes\Z[S]} \arrow[r, "h"]                         & {\Z[S]} \arrow[l, "k"',dotted, bend right=60] \\
        {P\otimes\Z[S]} \arrow[ru] \arrow[u] \arrow[r, dotted] & P \arrow[u]                                
        \end{tikzcd}\]
        Where $h$ is the null sequence of maps \[\begin{tikzcd}
            \Z[S] &[-37pt] {} \arrow[loop, distance=2em, in=35, out=325]
        \end{tikzcd}\]  $id$, $id-i_0\pi_0$, $id-i_1\pi_1$, ... where $\pi_n$ is the map $S\rightarrow S_n$. 
        We get the section $k$ via \[\Z\otimes\Z[S]\longrightarrow P\otimes\Z[S]\longrightarrow \Z[S]\] which is given by $\Z\xrightarrow{[0]}P$. Via construction of $h$ this is the $id$.
        Then the diagonal map is given by the null sequence $i_0\pi_0,\, i_1\pi_1-i_0\pi_0,\, ... ,\, i_n\pi_n-i_{n-1}\pi_{n-1},\,\dots$. This is again by \ref{5.1}. Now one can check that the diagonal map factors over $P$ as we have constructed our $i_n$ "nicely", giving us the dotted map.
        Then solidify and use the same arguments as before.
\end{proof}
\begin{remark}
    One could also use the strategy from \cite{condensed}. A short summary:
    \begin{align*}
        &\Z[S]^\square = \underset{i}\varprojlim\mathrm{Hom}\left(C(S_i,\Z),\Z\right)= \\
        &\mathrm{Hom}(C(S,\Z),\Z)=\mathrm{Hom}\left(\bigoplus\Z,\Z\right)=\prod\Z
    \end{align*}
\end{remark}
\begin{corollary}\label{5.10}\[
    \begin{tikzcd}
        \prod_\N\Z\otimes^\square\prod_\N\Z \arrow[r, "\cong", phantom] & \prod_{\N\times\N}\Z \\
        (P\otimes P)^\square \arrow[u, "\cong"] \arrow[r, "\cong", phantom]      & P^\square \arrow[u, "\cong"] 
        \end{tikzcd}
        \] And $\Z\llbracket T \rrbracket\otimes^\square\Z\llbracket T \rrbracket\cong\Z\llbracket T,U\rrbracket.$
\end{corollary}
\begin{proof*}\footnote{Thanks to Julius for this proof!}
    For infinite profinite $S$, $T$, we have
    \[\prod_\N\Z\otimes^\square\prod_\N\Z = \Z[S]^\square\otimes^\square\Z[T]^\square = \bigr(\varprojlim\Z[S_i]\otimes\varprojlim\Z[T_j]\bigl)^\square=\Z[S\times T]^\square= \prod_{\N\times\N}\Z.\]
    The intuition for that would be, that for $(P\otimes P)^\square\cong P^\square$ we have two convergent series to $0$ on $\N\times\N$ and this is the same as one convergent series, where one takes alternating entries of the two convergent series.
\end{proof*}
\begin{theorem}\label{charsolid}
    $\solid\subset\lcondab$ is abelian, stable under (co)limits and has a single compact projective generator $\prod_\N \Z$ (and $\prod_\N\Z\otimes^\square\prod_\N\Z\cong\prod_{\N\times\N}\Z$).\\ Also $M\in\lcondab$ is solid iff $\forall S=\varprojlim S_n\in \mathrm{Pro}_\N(\mathrm{Fin})$, all $g:S\rightarrow M, \exists!$ extension of $g$ to $S\rightarrow\Z[S]^\square=\varprojlim\Z[S_n]\dashrightarrow M$.  
\end{theorem}
\begin{proof}
Only $"\Leftarrow"$ of last assertion is left to show.\\
Consider the diagram, where the first row is a presentation\footnote{Here we use the adjunction of the forgetful functor and the free condensed abelian group functor to see, that it suffices to understand maps in this setting.} of $g$ in $\lcondab$:\[
    \begin{tikzcd}
        {\bigoplus_j\Z[S_j]} \arrow[r] \arrow[d]                                & {\bigoplus_i\Z[S_i]} \arrow[d] \arrow[r]                       & M \arrow[r] & 0 \\
        {\bigoplus_j\Z[S_j]^\square} \arrow[r] \arrow[rru, "0"', bend right=60] & {\bigoplus_i[\Z[S_i]]^\square} \arrow[ru, "\exists!"', dotted] &             &  
        \end{tikzcd}
\]Hence $M$ is a cokernel of a map of solids, hence solid itself.
\end{proof}
\paragraph{Philosophical comment:} \[\Z[S]^\square=\varprojlim \Z[S_n]=\varprojlim \underline{\mathrm{Hom}}(\cont(S_n,\Z),\Z)= \underline{\mathrm{Hom}}\left(\cont(S,\Z),\Z\right),\] where one can understand the right side as "$\Z-$valued measures on $S$". If we consider the diagram \[
    \begin{tikzcd}
        &[-30pt] \mu\in                  &[-25pt] {\Z[S]^\square} \arrow[rd, "\exists!", dotted] & [-25pt]  \\
     g: & S \arrow[ru] \arrow[rr] &                                                & M
     \end{tikzcd}
\] one can interpret the unique map as "$\int gd\mu"\in M$.
\begin{remark}
    Note that in \ref{charsolid} we used the usual Hom, whereas in the definition of solid we had to use the internal \underline{Hom}.
\end{remark}
  