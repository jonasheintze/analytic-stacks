% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 8 (Scholze, 17.11.2023) -- {\it Huber pairs \& Analytic Rings}]{Lecture 8 (8/24)}

\subsection{Motivation} 
By now, we have seen several examples of a pair: 
\begin{align*}
    &\text{light condensed ring }A \\
    +& \text{ notion of "complete" condensed }A\text{-modules} 
\end{align*}    
We want to connect these pairs to the notion of so-called \textit{Huber pairs}. The bridge between these will be given by \textit{Analytic Rings}.
\begin{definition}(Huber)
    \leavevmode
    \begin{enumerate}
        \item A \textbf{Huber ring} is a topological ring $A$, that admits an open subring $A_0\subseteq A$ such that there is a finitely generated $A_0$-ideal $I\subset A_0$, s.t. $A_0$ has the $I$-adic topology.
        \item A \textbf{ring of integral elements} in a Huber ring $A$, is an open and integrally closed subring $A^+\subseteq A^\circ = \left\{\text{powerbounded elements}\right\}$.
        \item A \textbf{Huber pair} is a pair of $(A,A^+)$ of a Huber ring $A$ and a ring of integral elements $A^+$ of $A$
    \end{enumerate}
\end{definition}
\begin{example}
    \leavevmode
    \begin{enumerate}
        \item Any discrete ring is a Huber ring. ($A_0=A, I = (0)$)
        \item Any topological ring with $I$-adic topology for some finitely generated ideal $J$ is Huber. ($A_0=A, I=J$)
        \item $\Q_p$ is Huber. ($A_0=\Z_p, I = (p)$)
    \end{enumerate}
\end{example}
\begin{remark}
    The completion $\hat{A}$ of any Huber ring $A$ is again a Huber ring. In particular we have for \[
        A\supseteq A_0 \supseteq I   
    \] 
    the explicit completion\[
      \hat{A}\supseteq \hat{A}_0 = I-\text{adic completion of } A_0  
    \]
\end{remark}
\noindent We will always consider complete Huber rings/ Huber pairs.
\begin{definition}
For $A$ Huber ring. Then \[\left\{f\in A \mid f^n\xrightarrow{n\rightarrow \infty} 0\right\}=: A^{\circ\circ}\subseteq A^\circ := \Bigl\{f\in A\mid \{f^n\}_n \text{ is bounded}\Bigr\}\subseteq A\]
\end{definition}
\noindent In fact, for all $I \subseteq A_0 \subseteq A$ as above, we get \[
    \begin{tikzcd}
        A^{\circ\circ} \arrow[r, "\subseteq", phantom]                                                               & A^\circ                                                                        \\
        I \arrow[r, "\subseteq", phantom] \arrow[u, "{\reflectbox{\rotatebox[origin=c]{90}{$\subseteq$}}}", phantom] & A_0 \arrow[u, "{\reflectbox{\rotatebox[origin=c]{90}{$\subseteq$}}}", phantom]
        \end{tikzcd}
\]Also one can calculate that
\[
    A^{\circ\circ}  = \varinjlim_{\substack{\text{(filtered)}\\I\subseteq A_0\subseteq A}} I \subseteq A^\circ 
  = \varinjlim_{\substack{\text{(filtered)}\\A_0\subseteq A}} A_0 \subseteq A
\] 
\begin{example}
    $\Z[T]$ has several possible rings of integral elements, which will "lead up" to different theories. For example:
    \begin{enumerate}
        \item $\Z\subset \Z[T]\leadsto \module_{\Z[T]}(\solid_\Z)$
        \item $\Z[T]\subseteq \Z[T]\leadsto \solid_{\Z[T]}$
    \end{enumerate}
\end{example}
\noindent\textbf{Notation:} Huber uses single letter $A$ to denote a Huber pair $(A^\rhd,A^+)$. We will follow this notation.
\subsection{Analytic Rings}
Let $A^\rhd$ be a light condensed ring.
\begin{definition}[Analytic Ring]
    An \textbf{analytic ring structure} on $A^\rhd$ is a full subcategory \[\module_A\subset \cond(A^\rhd)= \left\{\substack{\text{(light) condensed } A^\rhd\text{-modules}\\+ \text{ map: } A^\rhd \otimes M \longrightarrow M}\right\}\]
    that is an abelian subcategory, stable under all (co)limits, all extensions, all $\underline{\ext}^i_{A^\rhd}(N,-)$ for $N\in\cond(A^\rhd)$ and contains $A^\rhd$. \\
    A map of analytic rings \[A=(A^\rhd,\module_A)\longrightarrow B = (B^\rhd, \module_B)\] is a map $A^\rhd\longrightarrow B^\rhd$, s.t. \[
        \begin{tikzcd}
            \module_B \arrow[r, "\subseteq", phantom] \arrow[d, "!"', dashed] & \cond(B^\rhd) \arrow[d, "\mathrm{restr.}"] \\
            \module_A \arrow[r, "\subseteq", phantom]                            & \cond(A^\rhd)                             
            \end{tikzcd}
    \]
\end{definition}
\begin{remark**}
    It is immediate to see, that this kind of generalizes the concept of solid modules over a light condensed ring.
\end{remark**}
\begin{theorem}[Reflection Principle]\label{reflection}
    If $\mathcal{C}$ is presentable, $\mathcal{D}\subset \mathcal{C}$ full and closed under all limits, and $\exists$ regular cardinal $\kappa$ such that $\mathcal{D}$ is closed under $\kappa$-filtered colimits, then $\mathcal{D}$ is presentable and there exists a left adjoint to the inclusion.\end{theorem}
\noindent The $\infty$-Category version of this theorem was proved by Ragimov-Schlank. 

\begin{proposition}
    There is a left adjoint to the inclusion:
    \begin{align*}
        \cond(A^\rhd)&\longrightarrow \module_A\\
        "M&\longmapsto M\otimes_{A^\rhd}A"
    \end{align*}
    The kernel of this functor is a $\otimes$-ideal, and $\module_A$ acquires a unique symmetric monoidal structure making $(-\otimes_{A^\rhd}A)$ symmetric monoidal.
\end{proposition}
\begin{proof}
    \leavevmode
    \begin{description}
    \item[Existence of left adjoint:] formal nonsense + Theorem \ref{reflection} 
    \item[$\otimes$-ideal:] Assume: $M\in \cond(A^\rhd)$ s.t. $M\otimes_{A^\rhd}A=0$. Let $N\in\cond(A^\rhd)$ arbitrary.\\
    \underline{To show:} \begin{align*}
        &\bigl(N\otimes_{A^\rhd}M\bigr)\otimes_{A^\rhd}A=0  \\
        &\iff \forall L\in \module_A:\,\mathrm{Hom}_{A^\rhd}\bigl(N\otimes_{A^\rhd}M, L\bigr)=0
 \end{align*}
 but as 
 \begin{align*}
    &\mathrm{Hom}_{A^\rhd}\bigl(N\otimes_{A^\rhd}M, L\bigr)=\mathrm{Hom}_{A^\rhd}\bigl(M,\underbrace{\homline_{A^\rhd}(N,L)}_{\in \module_A}\bigr)\\
    &=\mathrm{Hom}_{\module_A}\bigl(\underbrace{M\otimes_{A^\rhd}A}_{=0},\underline{\mathrm{Hom}}_{A^\rhd}(N,L)\bigr)=0
 \end{align*}
 \item[unique symmetric monoidal structure:]
 $\otimes_A$ has to be given by \[
   M\otimes_A N:= \bigl(M\otimes_{A^\rhd}N\bigr)\otimes_{A^\rhd}A
 \]
 to check: for all $M,N\in \cond(A^\rhd)$
 \begin{align*}
    \bigl(M\otimes_{A^\rhd}N\bigr)\otimes_{A^\rhd} A\xrightarrow[?]{\sim} & \bigl(M\otimes_{A^\rhd}A\bigr)\otimes_A \bigl(N\otimes_{A^\rhd}A\bigr)\\
    & :=\left(\bigl(M\otimes_{A^\rhd}A\bigr)\otimes_{A^\rhd}\bigl(N\otimes_{A^\rhd}A\bigr)\right)\otimes_{A^\rhd}A
\end{align*}
\noindent For that, use the same argument as in the Solid case, see Corollary \ref{5.4}.
\end{description}
\end{proof}
\begin{remark}
    Hence for a map of analytic rings $A\longrightarrow B$, we get via the left-adjoint of $\module_B\subseteq \cond(B^\rhd)$ and the left adjoint of the restriction $\cond(B^\rhd)\rightarrow\cond(A^\rhd)$ a functor which is symmetric monoidal and also left-adjoint to $\module_B\rightarrow \module_A$:\[
        (-\otimes_A B): \module_A\xrightarrow{\otimes-\text{functor}} \module_B
    \]
\end{remark}
\subsection{Analytic Ring structure on Derived Categories}
\begin{definition}
    Let $A$ be an analytic ring structure on $A^\rhd$. Then $D(A)\subseteq D(\cond(A^\rhd))$ is defined as the full subcategory of all $M\in D(\cond(A^\rhd))$ s.t. $\forall i\in \Z$, $H_i(M)\in\module_A$.
\end{definition}
\begin{warning}\label{war:der}There is a functor $D(\module_A)\rightarrow D(A)$ but not always an equivalence.
\end{warning}

\begin{proposition}
    $D(A)\subseteq D(\cond(A^\rhd))$ triangulated subcategory stable under all $\bigoplus$, all $\prod$. The inclusion has a left adjoint\[
        (-\otimes^L_{A^\rhd}A): D(\cond(A^\rhd))\longrightarrow D(A). 
    \] The kernel of this functor is a $\otimes$-ideal and there is a unique monoidal tensor product $(-\otimes_A^L-)$ on $D(A)$ making $(-\otimes^L_{A^\rhd}A)$ symmetric monoidal.
\end{proposition}

\begin{proof}\leavevmode
    \begin{description}
        \item[triangulated:] Let \[
            M'\longrightarrow M \longrightarrow M''\longrightarrow M'[1]    
        \]
        be a triangle in $D(\cond(A^\rhd))$ and $M',M''\in D(A)$.\\
        \underline{To show:} $M\in D(A)$\[
            \begin{tikzcd}
                H_{i+1}(M'') \arrow[r] & H_i(M') \arrow[rd] \arrow[rr] & [-15pt]                        & [-15pt] H_i(M) \arrow[rd] \arrow[rr] &   [-15pt]                      & [-15pt]H_i(M'') \arrow[r] & H_{i-1}(M') \\
                                       &                               & Q \arrow[ru] \arrow[rd] &                              & K \arrow[ru] \arrow[rd] &                    &             \\
                                       & 0 \arrow[ru]                  &                         & 0 \arrow[ru]                 &                         & 0                  &            
                \end{tikzcd}
        \]
        with $K,Q\in\module_A$ and stability under extensions.\\
        Stability under $\bigoplus$ and stability under countable $\prod_\N$ reduce to $H_i$'s. \\For an arbitrary collection $(M_a)_{a\in L}$ of objects in $\module_A$, we need that \[R^i\prod_{a\in L}M_a\in \module_A\,\forall i.\] Set $M=\bigoplus_{a\in L}M_a$. 
        Then $R^i\prod_{a\in L}M_a$ is a retract of \[R^i\prod_{a\in L}M=\extline^i_{A^\rhd}\bigl(\bigoplus_{a\in L}A^\rhd, M\bigr),\] thus it is in $\module_A\,\forall i$.
        \item[Existence of left adjoint:] Follows from the last point and Theorem \ref{reflection}.
        \item[$\otimes$-ideal:] 
        \begin{align*}
            &M\in \mathrm{ker}\left(D(\cond(A^\rhd))\longrightarrow D(A)\right)\\
            &N\in D\left(\cond(A^\rhd)\right)
        \end{align*}
        \noindent \underline{To show:} $\forall L\in D(A)$:\[
        \rhom_{D\left(\cond(A^\rhd)\right)}\bigl(M\otimes_{A^\rhd}^L N, L\bigr)=0    
        \]Light condensed sets are "replete". This means  countable limits of surjections are surjections.
        \[ \Rightarrow K\in D\left(\cond(A^\rhd)\right), K\xrightarrow{\sim} R\underset{n}\varprojlim \, \tau_{\leq n} K
            \] This is called "Postnikov limit".

            \noindent By Tensor-Hom adjunction, it suffices to show \[\rhomlin_{D(\cond(A^\rhd))}(N,L)\in D(A)\]
            By the above fact, we can assume: $L\in D^+$. On the other hand we can write $N=\colim_{n}\,\tau_{\geq -n}N$. As $N$ lies in the first component, pulling out the colimit of the  $\rhomlin$ gives a limit and we can assume that $N\in D^-$. But then a spectral sequence reduces one to $L\in\module_A,N\in\cond(A^\rhd)$.\\
            $\leadsto$ condition $\underline{\ext}^i_{A^\rhd}\in \module_A$. Now existence of $(-\otimes_A^L-)$ is formal.
        \end{description}
\end{proof}
\begin{remark}
    For a map of analytic rings $A\rightarrow B$ we get via the left adjoint of $D(B)\subseteq D(\cond(B^\rhd))$ the map: \[
        (-\otimes_A^L B): D(A)\longrightarrow D(B)
    \]
\end{remark}
\begin{proposition}
    $D(A)$ has a natural $t$-structure making $\bigl(D(A)\hookrightarrow D(\cond(A^\rhd))\bigr)$ $t$-exact and $(-\otimes_{A^\rhd}^L A)$ preserve $D_{\geq 0}$.
\end{proposition}
\begin{proof}
        $D(A)$ is stable under all $\tau_{\leq n}, \tau_{\geq n}$ by definition. Left adjoints to $t$-exact functors preserve $D_{\geq 0}$.
\end{proof}
\noindent The $\heart$ of the $t$-structure is:
\begin{align*}
     D(A)^\heart &= \module_A\\
    (-\otimes^L_{A^\rhd} A)^\heart &= (-\otimes_{A^\rhd}A)\\
     (-\otimes_A^L-)^\heart&= (-\otimes_A -)
\end{align*}
\subsection*{Back to comparison with Huber Rings}
\[
\begin{tikzcd}
    & \textrm{Solid Rings} \arrow[rd, hook] &                    \\[10pt]
\textrm{Huber rings} \arrow[rr, hook] \arrow[ru, hook] &                                     & \mathrm{Cond Rings}  \\[-17pt]
A^\rhd \arrow[rr, maps to]                           &                                     & \underline{A}^\rhd
\end{tikzcd}
\]
\begin{definition}
    Let $A^\rhd$ be a solid ring, we can define as last time \[
        A^{\circ\circ}\subseteq A^\circ \subseteq A^\rhd(*)
    \]
    With \[A^{\circ\circ}:=\bigl\{f\in A^\rhd(*)\mid \subalign{\Z[T]&\longrightarrow A^\rhd\\T &\longmapsto f} \text{ factors over }\Z\llbracket T \rrbracket\bigr\}\]
    and\[
        A^\circ := \bigl\{f\in A^\rhd(*)\mid A^\rhd\in\solid_{\Z[T]} \text{ via } \subalign{\Z[T]&\longrightarrow A^\rhd\\ T&\longmapsto f}\bigr\}
    \] 
    Remember that $A^{\circ\circ}\subseteq A^\circ$ is a radical ideal and $A^\circ$ is an integrally closed subring of $A^\rhd(*)$.
\end{definition}
\begin{definition}
   
        An analytic ring $A= \bigl(A^\rhd, \module_A\bigr)$ is \textbf{solid} if it admits a (necessarily unique) map of analytic rings\begin{align*}
        &\Z_\square\longrightarrow A\\
        \iff & \text{All } M\in\module_A \text{ are solid}\\
        \iff & 1-\mathrm{shift}: P\otimes_\Z A\xrightarrow{\sim}P\otimes_\Z A
    \end{align*}
    
\end{definition}
\begin{definition}
    Assume $A$ is a solid analytic ring structure on solid ring $A^\rhd$. Then \begin{align*}
        A^+&=\bigl\{f\in A^\rhd(*)\mid \subalign{\Z[T]& \longrightarrow A^\rhd\\ T &\longmapsto f} \text{induces map of analytic rings } \Z[T]_\square\longrightarrow A\bigr\}\\
        &=\bigl\{f\in A^\rhd(*)\mid 1- f\cdot\text{shift:}\,P\otimes_\Z^L A \xrightarrow{\text{isom.}} P\otimes_\Z^L A\bigr\}
    \end{align*}
    
\end{definition}
\begin{remark**}\begin{enumerate}
\item For the last equality consider the map $\Z_\square\xrightarrow{T\mapsto f} A^\rhd$. Naturally we can now consider elements of $\module_A$ as $\Z[T]$-modules via this map. 
As discussed in last lecture, these are in $\solid_{\Z[T]}$ iff $R\homline_\Z\bigl(P,M\bigr)\xrightarrow[\sim]{\mathrm{shift}\cdot f - 1}R\homline_\Z\bigl(P,M\bigr)$ for all $M\in D(A)$. This is by adjunction the same as $R\homline_A\bigl(P\otimes_{\Z}A,M\bigr)\xrightarrow[\sim]{\mathrm{shift}\cdot f - 1}R\homline_A\bigl(P\otimes_\Z A,M\bigr)$. By some computation, this is an iso iff $P\otimes_\Z^L A \xrightarrow[\sim]{\mathrm{shift}\cdot f -1} P\otimes_\Z^L A$.
\item To emphasize the difference between $A^+$ and $A^\circ$: elements in $A^+$ induce a solid $\Z[T]$-module structure for \underline{all} modules in $\module_A$ whereas elements in $A^\circ$ only induce a solid $\Z[T]$-module structure for $A^\rhd$.
\end{enumerate}
\end{remark**}
\begin{proposition} For a solid analytic ring structure $A$ on $A^\rhd$, we get: 
    \[A^{\circ\circ}\subseteq A^+\subseteq A^\circ,\] where $A^+\subseteq A^\circ$ is an integrally closed subring.
\end{proposition}
\begin{proof}
    For $f\in A^{\circ\circ}$, we get an induced map $\Z\llbracket T \rrbracket \rightarrow A^\rhd$. As
    \[\module_{\Z\llbracket T \rrbracket}(\module_A)\subseteq\module_{\Z\llbracket T \rrbracket}(\solid_\Z)\subseteq \solid_{\Z[T]},\] the induced map by $f$ gives elements in $\module_A$ an solid $\Z[T]$ structure, hence $f\in A^+$ by definition.\\
    For $f\in A^+$, we get a map $\Z[T]_\square\rightarrow A \Rightarrow A^\rhd\in\module_A\rightarrow\solid_{\Z[T]}$\\
Integrally closed subring: same argument as last lecture 
\end{proof}
\begin{theorem}\label{8.19}
    For a Huber ring $A^\rhd$:
    \begin{align*}
        \left\{\substack{\text{rings of integral }\\\text{elements}}\right\}&\longleftarrow \left\{\substack{\text{solid analytic ring}\\\text{structures on }\underline{A}^\rhd}\right\}\\
        A^+ &\, \reflectbox{$\longmapsto$}\, A%\\
    \end{align*}
        This has a left adjoint:
        \begin{align*}
        \text{Huber pair: } (A^\rhd, A^+) & \longmapsto A = \begin{array}{l}\left(A^\rhd, A^+\right)_\square\\
        \module_A =\left\{M\in \cond(A^\rhd)\;\big|\; \subalign{ &\forall f\in A^+:
          \\ 
&\homline(P,M)\xrightarrow[\sim]{1- f\cdot\mathrm{shift }}\homline(P,M)}\right\}
    \end{array}
    \end{align*}
\end{theorem}
\begin{example}
    \begin{align*}
        (\Z,\Z)_\square & = \Z_\square=(\Z,\solid_\Z)\\
        (\Z[T],\Z)_\square & = \bigl(\Z[T], \module_{\Z[T]}(\solid_\Z)\bigr)\\
        (\Z[T],\Z[T])_\square &= \bigl(\Z[T], \solid_{\Z[T]}\bigr)
    \end{align*}
\end{example}
\begin{remark**}
    One does not need to worry about this definition regarding $\Z$-solid modules: In definition \ref{5.1} we defined solid $\Z$ modules to be the modules, for that the morphism \[
        \homline(P,M)\xrightarrow{1-\sigma}\homline(P,M)
        \] is an isomorphism. In fact this isomorphism suffices for the morphism \[
            \homline(P,M)\xrightarrow{1-n\sigma}\homline(P,M)
        \] to be an isomorphism: 

\noindent On an intuitive level, for a zero sequence $m_0,m_1,\dots$, one wants to construct $\sum_{i=j}^\infty m_i n^i$, as this would behave as the inverse of $1-n\sigma$ for each entry. As we already know that for any zero sequence we can construct $\sum_{i=0}^\infty m_i$ via the inverse of $1-\sigma$, we can just take the zero sequence \[(m_0, \underbrace{m_1,\dots,m_1}_{n-\text{times}},\underbrace{m_2,\dots,m_2}_{n^2-\text{times}})\] 
which maps via the inverse map of $1-\sigma$ to $\sum_{i=0}^\infty m_i n^i$ in the zero-th entry. To do that on a technical level, one needs to pay attention to the fact, that for every entry, one needs a different zero sequence, which looks similar. E.g. for the next entry one needs the sequence \[(m_0, m_1, \underbrace{m_2,\dots,m_2}_{n-\text{times}},\underbrace{m_3,\dots,m_3}_{n^2-\text{times}})\] and so on. This way one can construct an inverse to $1-n\sigma$. \\For negative $n$ one can do just the same, alternating signs of the $m_i$ accordingly.
\end{remark**}
\begin{proposition}
    Under the maps of Theorem \ref{8.19}:
\[
    \begin{tikzcd}
        {\bigl(A^\rhd, A^+\bigr)} \arrow[r, maps to] & {A=\bigl(A^\rhd, A^+\bigr)_\square} \arrow[ld, maps to, bend left] \\
        {A^+=\left(A^\rhd, A^+\right)^+_\square}     &                                                                   
        \end{tikzcd}
\]
Equivalently, we get a fully faithful functor
\begin{align*}
    \left\{\text{Huber pairs}\right\}&\longhookrightarrow\left\{\text{(solid) analytic rings}\right\}\\
    \left(A^\rhd, A^+\right)&\longmapsto\left(A^\rhd, A^+\right)_\square
\end{align*}
\end{proposition}
\begin{proof*}\footnote{Thanks to Ferdinand's help!}
    The argument for that can be looked up in Proposition 13.16 of \cite{analytic}. Here is a rough sketch: 
    \begin{description}
        \item[faithful faithful:]
        Consider two Huber pairs $(A,A^+)$ and $(B,B^+)$. By definition maps of analytic rings $(A,A^+)_\square\rightarrow (B,B^+)_\square$ embed into maps $\underline{A}\rightarrow \underline{B}$. As $A$ and $B$ are metrizable these correspond to maps $A\rightarrow B$.
\\
        We need to show that a map $A\rightarrow B$ sends $A^+\rightarrow B^+$ iff it induces a map of analytic rings $f: (A,A^+)_\square\rightarrow (B,B^+)_\square$. Forward direction is clear. For the backward direction we want to show, that for any element $g=f(a)$ for $a\in A^+$ lies in $B^+$. From composing with the map $(\Z[T],\Z[T])\rightarrow (A,A^+)$, we can assume wlog. $(A,A^+)=(\Z[T],\Z[T])$. From lecture 7 now follows:
        \begin{align*}
            \Z((T^{-1}))&\otimes_{(\Z[T],\Z)_{\square}}(B,B^+)_{\square}\\
            &=\bigl(\underbrace{\Z((T^{-1}))\otimes_{(\Z[T],\Z)_\square}(\Z[T],\Z[T])_\square}_{=0}\bigr)\otimes_{(\Z[T],\Z[T])_\square}(B,B^+)_\square = 0
        \end{align*}
        As \[
            \colim_{\substack{\mathrm{filtered}\\\mathrm{fin. gen.}\\ R\subseteq B^+}}\;\bigl(\,\Z((T^{-1}))\otimes_{(\Z[T],\Z)_\square}(B,R)_\square\,\bigr) = \Z((T^{-1}))\otimes_{(\Z[T],\Z)_{\square}}(B,B^+)_{\square}
        \] and asking for an algebra to be $0$ is the same as asking $1=0$, there is a finitely generated $R\subseteq B^+$, s.t.\[
            \Z((T^{-1}))\otimes_{(\Z[T],\Z)_\square}(B,R)_\square = 0 
        \] 
        Using the resolution \[
            0\rightarrow\prod_\N \Z\otimes_\Z \Z[T]\xrightarrow{1-\mathrm{shift}\otimes T} \prod_\N \Z\otimes_\Z \Z[T]\rightarrow \Z((T^{-1})) \rightarrow 0
        \] which is the same as \[
            0\rightarrow P^\square\otimes_\Z \Z[T]\xrightarrow{1-\mathrm{shift}\otimes T} P^\square \otimes_\Z \Z[T]\rightarrow \Z((T^{-^1})) \rightarrow 0
        \]
        As $P$ is flat, by applying base change twice (first with $R[g]$ and then with $\underline{B}$) this induces the isomorphism
        \[
        (P\otimes_\Z R)^{R_\square}\otimes_{R_\square}\underline{B}\xrightarrow{1-\mathrm{shift}\otimes g}(P\otimes_\Z R)^{R_\square}\otimes_{R_\square}\underline{B}
        \]
    which is the same as 
    \begin{equation}\label{iso}
    \prod_\N \Z\otimes_{R_\square} \underline{B}\xrightarrow{1-\mathrm{shift}\otimes g} \prod_\N \Z\otimes_{R_\square} \underline{B}.
    \end{equation}
Now choose a ring of definition $B_0\subseteq B$ with ideal of definition $I\subseteq B_0$. We want to show, that any $g$, such that (\ref{iso}) is an iso, is integral over $R+I$. As $R+I$ is a subset of $B^+$ by definition, and $B^+$ is integrally closed, it follows that $g\in B^+$ and we are done. Now consider\footnote{Note that the colimit works so well, as $B/I$ is discrete.}
\begin{align*}
    \prod_\N R\otimes_{R_\square} B \longtwoheadrightarrow \prod_\N R\otimes_{R_\square} B/I &= \colim_{\substack{\mathrm{fin. gen.}\\ M\subseteq B/I}} \prod_\N R \otimes_{R_\square} M\\
    &= \colim \prod_\N M \longhookrightarrow \prod_\N B/I
\end{align*}
Now we have an injective map $\phi: \prod_\N R\otimes_{R_\square} B/I \longhookrightarrow \prod_\N B/I$ whose image consists of elements $(b_0, b_1, \dots)$, s.t. the submodule of $B/I$ generated by $\{b_0,\dots\}$ is finitely generated. Now we can see, that $(1, 0 , 0, \dots)$ is in the image, as it is already in $\prod_\N R$. Taking the preimage of this element under \[
    \prod_\N \Z\otimes_{R_\square} B/I \xrightarrow{1-\mathrm{shift}\otimes g} \prod_\N \Z\otimes_{R_\square} B/I\rightarrow \varprojlim_{n} \prod_\N B
 \] and then mapping it through $\phi$, we see that the submodule of $B/I$ generated by $(g^0, g^1, g^2,\dots)$ is finitely generated. Hence we get a relation \[g^i+\sum_{n=0}^{i-1}b_ig^i = 0 \mod B/I\]
 which shows as desired, that $g$ is integral over $R+I$.
\end{description}
\end{proof*}
