% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 2 (Scholze, 20.10.2023) -- {\it Light condensed sets + groups}]{Lecture 2 (2/24)}
\textbf{Want:} "global"/Spec($\Z$) perfectoid space that is modelled by rings with non-trivial topology. This should be "analytic" and include the archimedean and non-archimedean case.
\paragraph{Vague Idea} (possibly misguided):
\[\C_p\langle T^{\frac{\pm 1}{p^\infty}}\rangle=\C_p\langle T^{\Z[\frac{1}{p}]}\rangle\]

\paragraph{Globally?} $k\langle T^\Q\rangle\subset k\langle T^\R\rangle$ we first try to understand what the latter object should be. We want to remember the topology of $\R$ but must then "mix" this with the p-adic topology in $k$. If $k\langle T^\R\rangle$ is the algebra what is the geometric space? Such things will be allowed in the new proposed foundation. 

\paragraph{Since 2018:} Topological sets/groups/rings/... have been replaced by condensed sets/ groups/ rings/ ... . In this setting $k\langle T^\R\rangle$ does make sense as a condensed ring. 

\noindent\textbf{Attitude:} Develop most natural foundations for analytic geometry based on condensed rings and try it out for known formalisms.

\subsection{Light Condensed Sets}
\textbf{Starting point:} Category $\mathrm{Pro(Fin)}$ of profinite sets.
\begin{proposition}[Stone Duality] The following categories are equivalent:
\begin{enumerate}
	\item $\mathrm{Pro(Fin)}$\footnote{We will mostly use this category.}:
		\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
			\item Objects: ${\varprojlim_{i\in I}}\, S_i$ for $S_i$ finite, $I$ cofiltered poset ($I\neq\emptyset, \forall i,j\in I, $\indent$\exists k\leq i,j$)
			\item Morphisms: $\hom(\underset{i\in I}{\varprojlim}\, {S_i},\underset{j\in J}{\varprojlim}\, T_j)=\underset{i\in I}\varprojlim\,\underset{j\in J}\colim\,\hom(S_i,T_j)$
		\end{enumerate}
	\item Totally disconnected compact Hausdorff spaces $\subset$ Top
	\item $\text{(Boolean Algebras)}^\mathrm{op}$\footnote{Commutative rings R s.t. $\forall x\in R:x^2=x$.}
\end{enumerate}
\end{proposition}
\noindent The functors are: 
\[
\begin{tikzcd}[row sep=0em]
1) \arrow[r]                                          & 2) \arrow[r]                                            & 3) \\
\underset{i\in I}{\varprojlim}\, S_i \arrow[r, maps to] & S=\underset{i\in I}{\varprojlim}\, S_i \arrow[r, maps to] & \mathrm{Cont}(S,\mathbb{F}_2)=\underset{i}\colim\,\mathbb{F}_2^{S_i}\\
\underset{i}\varprojlim\,\hom(A_i,\mathbb{F}_2) & {\hom(A,\mathbb{F}_2)=\mathrm{Spec}(A)} \arrow[l, "="' ,maps to] & A \arrow[l, maps to]
\end{tikzcd}\]

\noindent Two measures of how "big" $S$ is:
\begin{definition}
Let $S=\underset{i\in I}{\varprojlim}\, S_i$ be a profinite set:
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
\item The \textbf{size} of $S$ is $\chi :=   |S|$.
\item The \textbf{weight} of $S$ is $\lambda := |\mathrm{Cont}(S,\mathbb{F}_2)|$.
\item $S$ is \textbf{light} if $\lambda\leq\omega:= |\N|$, i.e. countable limit of finite sets.
\end{enumerate}
\end{definition}
\begin{remark}
If $\lambda$ infinite, it is also equal to $|I|$ for the smallest possible $I$.
\end{remark}
\begin{example} \leavevmode
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item finite sets
	\item $\N\cup\{\infty\} = \underset{n}{\varprojlim}\,\{1,2,3,...,n,\infty\}, \chi=\omega, \lambda = \omega$
	\item Cantor Set $=\{0,1\}^{\N}=\underset{n}{\varprojlim}\,\{0,1\}^n, \chi=2^\omega, \lambda =\omega$
	\item Non-example: $S=\beta\N\;\footnote{Stone-Čech compactification}\leadsto\{\text{subsets of }\N\}=\mathrm{Cont}(S,\mathbb{F}_2)=\mathrm{Cont}(\beta\N,\mathbb{F}_2)=\mathrm{Cont}(\N,\mathbb{F}_2), \chi=2^{2^{\omega}}, \lambda = 2^\omega$
\end{enumerate}
\end{example}
\begin{proposition}
$\lambda\leq 2^\chi$ and $\chi\leq 2^\lambda.$ Also, if $\chi=\omega \Rightarrow \lambda=\omega$.
\end{proposition}
\begin{proof}
$\lambda=|\mathrm{Cont}(S,\mathbb{F}_2)|\leq |\mathrm{Map}(S,\mathbb{F}_2)|=2^\chi.$ Denote by $A$ the representation as a boolean Algebra: $\chi=|\hom(A,\mathbb{F}_2)|\leq|\mathrm{Map}(A,\mathbb{F}_2)|=2^\lambda$. Write $S=\{S_0,S_1,...\}.$ For each $n$ inductively choose a quotient $S\twoheadrightarrow S_n(\twoheadrightarrow S_{n-1})$ such that $\{S_0,...,S_n\}\hookrightarrow S_n$.
\end{proof}
\begin{proposition}
The following categories are equivalent to light profinite sets.
\begin{enumerate}
	\item $\mathrm{Pro}_\N$(Fin):
		\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
			\item Objects: $\underset{n\in \N}{\varprojlim}\, S_n$
			\item Morphisms: 
			\begin{align*}\hom(\varprojlim_{n\in \N}\, S_n,\underset{m\in \N}{\varprojlim}\, T_m)&=\underset{n\in \N}\varprojlim\,\underset{m\in \N}\colim\,\hom(S_n,T_m)\\ &={\colim_{\substack{f:\N\rightarrow\N \\\text{strictly increasing}}}}\varprojlim_{n\in \N}\,\hom(S_n,T_m)\end{align*}
		\end{enumerate} 
	\item Metrizable totally disconnected compact Hausdorff spaces $\subset$ Top
	\item $\text{(Countable Boolean Algebras)}^{\mathrm{op}}$
\end{enumerate}
\end{proposition}
\begin{proposition}
The Category of light profinite sets has all countable limits. Sequential limits of surjections are surjective. 
\end{proposition}
\noindent This is proved in Proposition $\ref{2.7}$.
\begin{proposition}
Let $S$ be a light profinite set then $\exists$ a surjection $\{0,1\}^\N\twoheadrightarrow S$.
\end{proposition}
\begin{proof}
Take this as a simple exercise: Use the universal property of the limit and find for each $S_n$ a sufficiently large $k$ such that $\{0,1\}^k$ surjects onto $S_n$.
\end{proof}
\noindent Now two properties that are special to \textit{light} profinite sets:
\begin{proposition}
Let $S$ be a light profinite set, $U\subseteq S$ open. Then $U$ is a countable disjoint union of light profinite sets\footnote{We already have control over the open sets just via light profinite sets again. This gives us nice properties for when we define our Grothendieck topology.}.
\end{proposition}
\begin{warning} In general $\exists\, U\subset S$ open profinite with $\mathrm{H}^i(U,\Z)\neq 0$ for $i>0$. That would be a problem, as disjoint union takes Cohomology to products and on profinite sets these are totally split so one has no non-trivial coverings, hence cohomology vanishes.
\end{warning}
\begin{proof}
Let $S=\underset{n}\varprojlim\, S_n$ with $Z=S\setminus U = \underset{n}\varprojlim\, Z_n \subset S\,$\footnote{One way to think about open subsets purely in the language of the pro-category profinite sets, is to think about the closed things in Sets. So closed subsets should themselves be profinite sets and the closed subsets are precisely the injective maps of profinite sets. So here the closed complement $Z\subset X$ should be a profinite set as well.}, where $Z_n= \mathrm{im}(Z\rightarrow S_n)\subset S_n$. Let $\pi_n$ be the maps $S\rightarrow S_n$. Then $U = \bigcup \pi^{-1}_n(S_n\setminus Z_n)$. Each $\pi^{-1}_n(S_n\setminus Z_n) \underset{\mathrm{clopen}}\subset S.$\\
\centerline{$\Rightarrow U = \underset{n}\bigsqcup(\pi^{-1}_{n+1}(S_{n+1}\setminus Z_{n+1})\setminus\pi^{-1}_n(S_n\setminus Z_n))$}
\end{proof}
\begin{proposition}
Let $S$ be a light profinite set. Then $S$ is an injective object in $\mathrm{Pro(Fin)}$, i.e. $\forall \, Z\subset X:$
\begin{tikzcd}[row sep=1.5em]
Z \arrow[d, hook] \arrow[r]     & S \\
X \arrow[ru, "\exists", dotted] &  
\end{tikzcd}.
\end{proposition}
\begin{proof}
First of all we need to note, that an element in Cont($X,\mathbb{F}_2$) precisely is a choice of a clopen subset in $X$.
\\Case $S = \{0,1\}$: Here, Cont($X,\mathbb{F}_2)\twoheadrightarrow$ Cont($Z,\mathbb{F}_2$) (Or any clopen subset of $Z$ extends to a clopen subset of $X$). In general, write $S=\underset{n}\varprojlim\, S_n$, all $S_{n+1}\twoheadrightarrow S_n$. Induct on $n$, and extend to $S_n$: If one already extended the map to $S_n$ then extending further to $S_{n+1}$ the whole situation decomposes into disjoint unions over all the fibers over $S_n$. Hence w.l.o.g. $S_n=\ast$ and so $S_{n+1}$ is finite. Every finite set reduces to the first case we looked at. 
\end{proof}
\begin{definition}
 A \textbf{light condensed set} is a sheaf of sets on the category of light profinite sets for the Grothendieck topology generated by:
 \begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
 	\item Finite disjoint unions
 	\item \underline{All} surjective maps
\end{enumerate}
\end{definition}
\noindent\textbf{Equivalently:} A functor 
\begin{align*}X: \mathrm{Pro}_\N(\mathrm{Fin})^{\mathrm{op}}&\longrightarrow \mathrm{Sets}\\
S&\longmapsto X(S)\end{align*}
with the following properties:
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
	\item $X(\emptyset)=\ast$
	\item$X(S_1\sqcup S_2){\xrightarrow{\sim}}X(S_1)\times X(S_2)$
	\item$\forall\,f: T\twoheadrightarrow S$ it holds that $f^*:X(S){\xrightarrow{\sim}}\mathrm{eq}\bigl(X(T)\stackrel{p^*_1}{\underset{p^*_2}\rightrightarrows} X(T\times_S T)\bigr)$
\end{enumerate}
\paragraph{Key example:} Let $A$ be a topological space. We can produce a light condensed set $\underline{A}: S\mapsto \mathrm{Cont}(S,A)$. In particular $\underline{A}(*)=A$ as a set. So for any light condensed set $X$ we think of $X(*)$ as the "underlying set".
\begin{align*}
&\underline{A}(\N\cup\infty)= \text{convergent sequences in A (with choice of limit point).}\\
&\underline{A}(\textrm{Cantor Set})\lcirclearrowright\textrm{End(Cantor Set)}
\end{align*}
\begin{remark}
A light condensed set $X$ is completely determined by $X(\textrm{Cantor Set})$ and its action on End(Cantor Set). Thus a light condensed set is the data of an abstract set and an abstract monoid.
\end{remark}
\begin{proposition}
The functor 
\begin{align*}
\mathrm{Top}&\longrightarrow \mathrm{CondSet}^{\mathrm{light}}\\
A&\longmapsto\underline{A}
\end{align*} 
has a left adjoint
\begin{align*}
\mathrm{CondSet}^{\mathrm{light}}\longrightarrow& \mathrm{Top}\\ 
X\longmapsto& X(*)_{\mathrm{top}}.
\end{align*} 
Here $X(\ast)_{\mathrm{top}}=X(\ast)$ with the quotient topology\footnote{Notice how we don't need to take all profinite sets anymore in comparison to general condensed sets.} from\footnote{For the isomorphism: By Yoneda embedding, $\hom(\underline{S},X)=X(S)$. We use that here, as we take the disjoint union of maps of the elements in $X(S)$ applied to $\ast$.} \[\underset{S,\alpha\in X(S)}\bigsqcup S\longrightarrow X(\ast)\,\cong \underset{\alpha\in X(\text{Cantor Set})}\bigsqcup\text{Cantor Set}\longrightarrow X(\ast).\] It holds that $X(\ast)_\mathrm{top}$ is a "metrizable compactly generated" topological space. If $A$ is any metrizable compactly generated topological space then $A{\xleftarrow{\sim}}\underline{A}(\ast)_\mathrm{top}.$ This is shown in \ref{metriz}.
\end{proposition}
\begin{corollary}
	\[\{\textrm{metrizable compactly generated topological spaces}\}\longhookrightarrow\mathrm{CondSet}^{\mathrm{light}}.\]
\end{corollary}
\begin{remark}
	Most spaces arising in nature such as Fréchet spaces or Banach spaces are metrizable compactly generated topological spaces.
\end{remark}
\begin{remark}
\begin{enumerate}
	\item Johnstone's "Topological Topos" has a similar idea he should rather find topos which is somehow very closely related to topological spaces, based on just $\N\cup\{\infty\}$ and uses the canonical topology (which is not finitary, which leads to bad algebraic properties of topos).
	\item Escardó-Xu also have an approach which is closely related to ours here. They take light profinite sets but only take finite disjoint unions as covers. (They use the "set and monoid" description which is reasonable in the finite disjoint union context.)
\end{enumerate}
\end{remark}
\subsection{Light Condensed Abelian Groups}
\textbf{Recall:} For sheaves on any site, sheaves of abelian groups always form a Grothendieck abelian category (filtered colimits are exact + all limits and colimits exist). Hence $\mathrm{CondAb}^{\mathrm{light}}$ is a Grothendieck abelian category.
\begin{example}
Consider $\Q\longhookrightarrow\R$, or $\R_{\mathrm{discrete}}\longrightarrow\R$. What is the cokernel here?\footnote{Cont$(S,\Q)$ are locally constant maps.}
\begin{align*}
&(\underline{\R}/\underline{\Q})(\ast)= \R/\Q\\
&(\underline{\R}/\underline{\Q})(S)= \mathrm{Cont}(S,\R)/\mathrm{Cont}(S,\Q)
\end{align*}
More drastic:
\begin{align*}
	&(\underline{\R}/\underline{\R}_{\mathrm{discrete}})(\ast)=\R/\R=0\\
	&(\underline{\R}/\underline{\R}_{\mathrm{\mathrm{discrete}}})(S)= \mathrm{Cont}(S,\R)/\mathrm{Cont}(S,\R_{\mathrm{discrete}})\neq 0,
\end{align*}
	 since $\mathrm{Cont}(S,\R_{\mathrm{discrete}})$ are again locally constant maps.
\end{example}
\begin{theorem}
$\mathrm{CondAb}^{\mathrm{light}}$ is a Grothendieck Abelian category
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
\item Countable products are exact (and satisfy (AB6))
\item $\Z[\N\cup\{\infty\}]$ is internally projective\footnote{This is a property that is extremely specific for light condensed abelian groups. In Condensed abelian groups there are many projective objects, but there are no internally projective objects except trivial cases. $\Z[\N\cup\{\infty\}]$ wouldn't even be projective in Condensed Abelian groups.}
\end{enumerate}
\end{theorem}
\begin{remark}
The forgetful functor $\mathrm{CondAb}^{\mathrm{light}}\rightarrow\mathrm{CondSet}^{\mathrm{light}}$ has a left adjoint \begin{align*}\mathrm{CondSet}^{\mathrm{light}}&\longrightarrow\mathrm{CondAb}^{\mathrm{light}}\\
X&\longmapsto\Z[X]\end{align*}
\end{remark}
