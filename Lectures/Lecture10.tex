% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 10 (Scholze, 24.11.2023) -- {\it Tate \texorpdfstring{\cancel{Analytic}}{(Analytic)} Adic Spaces}]{Lecture 10 (10/24)}
\begin{warning}[Conflict of Terminology] All adic spaces give rise to analytic spaces in our sense. 
But within the theory of adic spaces, there is a subclass of "analytic" ones.
We will call them "Tate" in this lecture.
\end{warning}
\begin{definition}
    A Huber ring $A$ is called \textbf{Tate}, if it has a topological nilpotent unit (called "pseudo-uniformizer").
\end{definition}
\begin{example}
    $\Q_p \ni p$ or any non-archimedean local field or any Huber ring over a local field.
\end{example}
\begin{definition}
    An adic space $X$ is \textbf{Tate} (usually analytic) if it is covered by $\spa(A,A^+)$ with $A$ Tate ($\iff$ the (completed) residue fields at all points of $X$ are not discrete (i.e. are non-archimedean fields))
\end{definition}

\noindent \textbf{Intuition:} \[
\begin{tikzcd}
    \text{Schemes} \arrow[r, "\subseteq", phantom] & \text{Formal Schemes} \arrow[r, "\subseteq", phantom] & \text{Adic Spaces} \arrow[r, "\supseteq", phantom] & \text{Tate Adic Spaces} \\
    \spec(\mathbb{F}_p)                            & \mathrm{Spf}(\Z_p)                                    & {\spa(\Z_p,\Z_p)}                                  & {\spa(\Q_p,\Z_p)}      
    \end{tikzcd}
    \]
\subsection{Structure of Tate Huber rings \texorpdfstring{$A$}{A}}
Consider a Huber ring $A$ with $\pi \in A$ topologically nilpotent unit and $A_0\subseteq A$ a ring of definition, hence w.l.o.g.  $\pi\in A_0$ as this is true for a sufficiently high power. Then $A_0$ carries the $\pi-$adic topology.
Then one can turn $A$ into a Banach algebra with unit ball $A_0$ via \begin{align*}
    &|f|= 2^{-n}, \textrm{ where } n = \mathrm{sup}\{n\mid \pi^{-n} f \in A_0\}  \\
    & |\pi| = \frac{1}{2}
\end{align*}
Hence we get an equivalence (without the $0$ ring on both sides)
\begin{alignat*}{2}
    \left\{\begin{array}{l}
    \text{complete Tate}\\
    \text{Huber rings}
     \end{array}\right\}
     \xrightarrow{\sim} \left\{\begin{array}{l}
        \text{complete ultra-metric Banach rings } $B$\\
        \text{admitting } \pi\in B \text{ with } 0<|\pi| <1\\
        \text{and continuous maps } |\pi| |\pi^{-1}| = 1
         \end{array}\right\}\end{alignat*}

\begin{definition}
    $(A,A^+)$ is \textbf{sheafy} if $\mathcal{O}^\text{Huber}_{\spa(A,A^+)}$ is a sheaf.
\end{definition}
\begin{theorem}[Kedlaya-Liu]\label{kedlayaliu}
    Assume $A$ Tate and $(A,A^+)$ sheafy. Then 
    \begin{align*}
      \mathrm{FinProj}  &\xrightarrow{\sim} \mathrm{VB}\bigl(\spa(A,A^+), \mathcal{O}^\mathrm{Huber}_{\spa(A,A^+)}\bigr)\\
      M &\mapsto  M\otimes_{A} \mathcal{O}^{\mathrm{Huber}}_{\spa(A,A^+)}
    \end{align*}
    is an equivalence, i.e. one can glue finite projective modules. Here by $\mathrm{VB}$ we denote finite locally free sheaves of modules.
\end{theorem}
\noindent Of this theorem, there is also a version for (pseudo-)coherent\footnote{We will define pseudo-coherence in \ref{pse-coh} again. } modules.

\begin{definition**}
    For Tate Huber pairs $R$ we now define the structure sheaf analogous to Definition \ref{structuresheaf}: \begin{align*}
        \mathcal{O}\left(U\left(\frac{f_1,\dots, f_n}{g}\right)\right) = R\Bigl[\frac{1}{g}\Bigr]^{L\frac{f_1}{g},\dots,\frac{f_n}{g}\square}
     \end{align*} If this lives in degree $0$, one can also define a sheaf of integral elements which is the same as the one in Definition \ref{structuresheaf}:\[
        \mathcal{O}^+\left(U\left(\frac{f_1,\dots, f_n}{g}\right)\right)=\overline{R^+\Bigr[\frac{f_1}{g},\dots, \frac{f_n}{g}\Bigl]}
     \]
\end{definition**}
\begin{remark**}
    Note, that this agrees with Definition \ref{structuresheaf}, as solidifying in a discrete ring doesn't change anything (elements in $\homline(P,R)$ are sequences which are constant at some point). This definition is also parallel to Huber's theory, as solidifying can be thought of a completion in the classical case, which is what we are going to defend in the following section. 
\end{remark**}
\noindent\textbf{Goal:} Give a proof of Theorem~\ref{kedlayaliu} using $D(-)^\square$.\\
\noindent\textbf{Outline:}\begin{enumerate}
    \item Relation between $\mathcal{O}^\mathrm{Huber}$ and $\mathcal{O}$.
    \item In general $U= U\left(\frac{f_1,\dots,f_n}{g}\right)\mapsto D^{\mathrm{pcoh}}_{\geq 0}\bigl(\mathcal{O}(U)\bigr)\subseteq D\left(\left(\mathcal{O}(U),\mathcal{O}^+(U)\right)_\square\right)$ is a sheaf of $\infty-$categories
\end{enumerate}
\noindent \textbf{Reference:} Andreychev.
\begin{enumerate}
    \item Relation between $\mathcal{O}^\mathrm{Huber}$ \& $\mathcal{O}$
\end{enumerate}

\begin{proposition}
    Let $U=U\bigl(\frac{f_1,\dots,f_n}{g}\bigr)\subseteq \spa(A,A^+)$ be a rational subset \big(so $(f_1,\dots, f_n)\subseteq A$ is open, but once one has a pseudo-uniformizer it follows $(f_1,\dots,f_n)=A$\big). Then \begin{align*}
\mathcal{O}^\mathrm{Huber}(U)&=A\langle T_1,\dots, T_n\rangle\big/\overline{(gT_1-f_1,\dots,gT_n-f_n)}\\
\mathcal{O}(U)&=A\langle T_1,\dots, T_n \rangle\derquo (gT_1-f_1,\dots,gT_n-f_n)
\end{align*}
where for a (condensed) ring $R$ and $x_1,\dots,x_n\in R$,\begin{align*}
    &R\derquo (x_1,\dots,x_n) = R\otimes^L_{\Z[x_1,\dots,x_n]} \Z  \\
    = \:& \left(R\rightarrow \bigoplus_{i=1}^n R\rightarrow \dots\rightarrow \bigwedge^i \Bigl(\bigoplus^n_{l=1}R\Bigr)\rightarrow\dots\rightarrow \bigoplus^n_{l=1}R\xrightarrow{(x_i)_i}R\right) \textrm{Koszul complex}
\end{align*}
\end{proposition}
\begin{proof}[Sketch of Proof.]
    Start with $A\bigl[\frac{1}{g}\bigr]= A[T_1,\dots,T_n]\derquo (gT_1-f_1,\dots,gT_n-f_n)$ and $T_1,\dots,T_n$-solidify. This is an exact operation on derived categories. As $A\bigl[\frac{f_1,\dots, f_n}{g}\bigr]$ is calculated by the Koszul complex, which is a finite resolution of finite direct sums of $A$, one just has to understand what happens when solidifying $A$. As we can write $A=\varprojlim A/(\pi^n)$ and adjoining plus solidifying $T_i$ commutes with limits, we get  \[A[T_1,\dots,T_n]^{LT_1,\dots, T_n\square}= A\langle T_1,\dots, T_n\rangle\]
\end{proof}
\begin{definition}
    In a Topos $T$ a map $f:X\rightarrow Y$ is quasicompact, if for all quasicompact objects $Z\in T$ the fibre product $Y\times_{X}Z$ is quasicompact.
\end{definition}
\begin{remark}The inclusion $\mathrm{CondSet}^{\mathrm{qs}}\subseteq \mathrm{CondSet}$ has a left adjoint \begin{align*}
    \mathrm{CondSet}&\rightarrow \mathrm{CondSet}^{\mathrm{qs}}\\
    X    &\mapsto X^{\mathrm{qs}}
\end{align*} which can be imagined like "universal Hausdorff quotient". This commutes with finite products and hence preserves algebraic structures (like being a group, ring,\dots). Concretely if $X= \tilde{X}/R$ with $\tilde{X}$ quasi-separated \& $R\subseteq\tilde{X}\times\tilde{X}$ equivalence relation. Then $\exists$ minimal quasi-compact injection $\overline{R}\subseteq \tilde{X}\times \tilde{X}$ that is also an equivalence relation with $R\subseteq \overline{R}$, then $X^{\mathrm{qs}}= \tilde{X}/\overline{R}$.
\end{remark}
\begin{corollary}
    $\mathcal{O}^\mathrm{Huber}(U)= \left(\pi_0\mathcal{O}(U)\right)^\mathrm{qs}$
\end{corollary}
\begin{lemma}\label{lem:seq}
    Let $X$ be any sequential topological space. Then \begin{align*}
    \left\{\text{closed subsets of } X\right\}&\xrightarrow{\sim} \left\{\text{quasi-compact injection into} \underline{X}\right\}    \\
          Z  &\mapsto \underline{Z}\subseteq \underline{X}
    \end{align*}
\end{lemma}
\begin{proof}[Proof of Corollary]
\begin{align*}
    \pi_0\mathcal{O}(U) &= A\langle T_1,\dots,T_n\rangle/(gT_1-f_1,\dots, gT_n-f_n)\\
\left(\pi_0\mathcal{O}(U)\right)^\mathrm{qs}&= A\langle T_1,\dots,T_n\rangle\big/\overline{(gT_1-f_1,\dots, gT_n-f_n)}\\
&=\mathcal{O}^\mathrm{Huber}(U)
\end{align*}
\end{proof}
\begin{lemma}\label{keylem2}
    Let $A$ be a Tate Huber ring, $f\in A$. Assume \begin{equation}\label{exseq}
        0\longrightarrow A\longrightarrow A\Bigl\langle\frac{f}{1}\Bigr\rangle\oplus A\Bigl\langle\frac{1}{f}\Bigr\rangle\longrightarrow A\Bigl\langle\frac{f}{1},\frac{1}{f}\Bigr\rangle \longrightarrow 0
    \end{equation} is exact\footnote{E.g. $0\rightarrow \mathcal{O}^\mathrm{Huber}(X)\rightarrow \mathcal{O}^\mathrm{Huber}(\{|f|\leq 1\})\oplus \mathcal{O}^\mathrm{Huber}(\{|f|\geq 1\})\rightarrow\mathcal{O}^{\mathrm{Huber}}(\{|f|=1\})\rightarrow 0$} (e.g. $A$ is sheafy). Then \begin{align*}
A\Bigl\langle\frac{f}{1}\Bigr\rangle&=A\langle T\rangle\derquo (T-f)\\
A\Bigl\langle\frac{1}{f}\Bigr\rangle & = A\langle T\rangle\derquo(1-Tf)
\end{align*}
\end{lemma}
\begin{proof}
    To see: \[
       A\langle T\rangle \xrightarrow[(\text{or } 1-Tf)]{T-f} A\langle T\rangle
    \] is injective with closed image. Using (\ref{exseq}) this question can be analyzed over $A\langle\frac{f}{1}\rangle$ and $A\langle\frac{1}{f}\rangle$. In other words, assume $|f|\leq 1$ or $|f|\geq 1$. But this can be checked by hand.

\end{proof}
\begin{theorem}[essentially Kedlaya]
    \begin{align*}(A,A^+) \text{ sheafy } &\,\iff \mathcal{O}^\mathrm{Huber}=\mathcal{O}\\
        & \bigl(\iff \forall U \text{ rational, } \mathcal{O}(U) \text{ sits in degree } 0 \text{ and is quasi-separated}\bigr)
    \end{align*}
\end{theorem}
\begin{proof}\leavevmode
    \begin{description}[labelindent=1cm]
        \item["$\Leftarrow$"]  is clear.
        \item["$\Rightarrow$"] suffices to check good properties of $\mathcal{O}(U)$ after possibly further refinement. Also one can always refine by "simple Laurent covers"\[
           \left\{|f|\leq 1\right\}\cup \left\{|f|\geq 1\right\} 
        \] Now this reduces to Lemma \ref{keylem2}.
    \qedhere\end{description}\end{proof}
\begin{enumerate}
    \setcounter{enumi}{1}
    \item Gluing of pseudocoherent complexes.
\end{enumerate}
\begin{definition}[\cite{SGA6}]\label{pse-coh}
    Let $R$ be a ring. Then $C\in D(R)$ is \textbf{pseudocoherent} if it can be represented by a complex\[
    \cdots\rightarrow C_n\rightarrow \cdots\rightarrow C_1\rightarrow C_0 \rightarrow \cdots \rightarrow C_{-n}\rightarrow \cdots \rightarrow0\rightarrow 0\rightarrow \cdots  
    \]
    where each $C_i$ is a finite projective $R-$module\footnote{This roughly says that each homology is coherent.}. \\
    $C$ is \textbf{perfect} if there is such a complex that is bounded. 
\end{definition}
\begin{theorem}
   Let $(A,A^+)$ be any Huber pair with $A$ Tate. Then the assignments for $V\subseteq \spa(A,A^+)$ rational 
   \[
        V\longmapsto \begin{cases}
            D^{\mathrm{pcoh}}(\mathcal{O}(V)(*))\\
            D^{\mathrm{pcoh}}_{\geq n} (\mathcal{O}(V)(*))\\
            \mathrm{Perf}(\mathcal{O}(V)(*))\\
            \mathrm{Perf}^{[a,b]}(\mathcal{O}(V)(*))
        \end{cases}
   \] are sheaves of $\infty-$categories. Here $\mathrm{Perf}^{[a,b]}(\mathcal{O}(V)(*))$ denotes non-zero entries in between degrees $a$ and $b$.
\end{theorem}
\begin{remark}
The case $\mathrm{Perf}^{[0,0]}= \mathrm{VB}$ recovers the theorem of Kedlaya-Liu.
\end{remark}

\noindent \textbf{Outline of Argument:} We know\footnote{The first inclusion is by the embedding, which firstly chooses free resolutions as representatives in $D(A(*))$ and then maps $\bigoplus_I A(*)\mapsto \bigoplus_I A$. Elements in the image of that functor are called discrete.} \[
        V\mapsto D\left((\mathcal{O}(V),\mathcal{O}^+(V))_\square\right)  \supseteq D(\mathcal{O}(V)(*))\supseteq\text{all categories above}
\] is a sheaf of $\infty-$categories.

This already reduces us to:\\
\indent If $M\in D\left( (A,A^+)_\square\right)$ s.t. over a cover, $M\otimes^L_{(A,A^+)_\square}\left(\mathcal{O}(U),\mathcal{O}^+(U)\right)_\square$ lies in one of these \indent subcategories, then so does $M$.

\begin{warning} The condition $M\in D(A(*))\subseteq D\left((A,A^+)_\square\right)$ does not globalize.
\end{warning}

\begin{example}
        Consider the Tate-elliptic curve 
        \[\mathbb{G}_{m,\Q_p}\xrightarrow{a}\mathbb{G}_{m,\Q_p}^\mathrm{ad}/p^\Z \]
        Assume $\spa(A,A^+)$ is a large open subset of $\mathbb{G}_{m,\Q_p}^\mathrm{ad}/p^\Z$. Then \[\restr{a_!\mathcal{O}_{\mathbb{G}_{m,\Q_p}^\mathrm{ad}}}{\spa(A,A^+)} \in D(A,A^+)_\square\] and is locally discrete but globally not discrete in $D\left(A(*)\right)$.
\end{example}

\subsection{Pseudocoherent and Nuclear modules}

Instead we will do the following to replace the old definition:\begin{enumerate}
    \item Define $D^{\mathrm{pcoh}}\left((A,A^+)_\square\right)\subseteq D\left((A,A^+)_\square\right)$ complexes represented by\[
        \cdots\rightarrow C_n\rightarrow C_{n-1}\rightarrow\cdots\rightarrow C_1\rightarrow C_0\rightarrow\cdots\rightarrow C_{-n}\rightarrow 0\rightarrow 0 \rightarrow\cdots
    \] with all $C_i=P^\square$
     \begin{align*}
    \iff \text{ bounded to the right and } \forall i\in\Z, &\ext^i(C,-):\module(\ab)\rightarrow \ab\\ &\text{ commutes with filtered colimits}    
    \end{align*}
and show that the property of lying in there globalizes. This is formal using that localizations have finite Tor-dimension.
\item Define nuclear modules $D^{\mathrm{nuc}} ((A,A^+)_\square)\subseteq D((A,A^+)_\square)$ and show that this property globalizes.
\item Show that $\dpcoh(A(*))\subseteq \dpcoh((A,A^+)_\square)\cap\dnuc((A,A^+)_\square)$ is an equality.
\end{enumerate}
As 1. follows from formal properties, we will now do 2.
\begin{enumerate}
    \setcounter{enumi}{1}
    \item Define nuclear modules and show that it globalizes:
\end{enumerate}
\begin{definition}
    $C\in D((A,A^+)_\square)$ is \textbf{nuclear}, if \[
        \homline(P,C) \xleftarrow{\sim} C\otimes_{(A,A^+)_\square}^L\homline(P,A)
    \] is an isomorphism. ("All maps from $P$ are trace-class")
\end{definition}
\begin{remark}\leavevmode\begin{enumerate}
\item Nuclear modules are generated under colimits and shift by $C_0(\N,A)\,(=A\langle T\rangle)$.
\item In general\footnote{I.e. for more general (non-Tate) analytic rings.} one can generate them by ${\colim(Q_0\rightarrow Q_1\rightarrow\dots)}$ where the sequence consists of trace-class morphisms, meaning they come from $Q_i^\vee\otimes Q_{i+1}$. 
\end{enumerate}
\end{remark}
So over $\Q_p$ nuclear modules are generated by $\Q_p-$Banach-spaces.\\
Now we would need to show that this property globalizes: 

\paragraph{Key:} $\homline(P,-)$ commutes with localizations. But this is okay, as localizations are also given by some $\homline(-,-)$.

\begin{enumerate}
    \setcounter{enumi}{2}
    \item Show that $\dpcoh(A(*))\subseteq \dpcoh((A,A^+)_\square)\cap\dnuc((A,A^+)_\square)$ is an equality.
\end{enumerate}

\begin{proof}[Sketch of Proof.]  (The very formal proof of this assertion can be found in \cite{complex} and uses some homotopy theory.)\\
    Assume $C\in\dpcoh((A,A^+)_\square)\cap\dnuc((A,A^+)_\square)$ and consider the map $\alpha: P\rightarrow C$ of the following form:
    \[
    \begin{tikzcd}
        C: & \dots \arrow[r] & P \arrow[r] & P \arrow[r]                 & 0 \arrow[r] & 0 \arrow[r] & \dots \\
           & \dots \arrow[r] & 0 \arrow[r] & P \arrow[r] \arrow[u, "id"] & 0 \arrow[r] & 0 \arrow[r] & \dots
        \end{tikzcd}\]
As this is surjective on $H_0$ and $C$ is nuclear, we get the following commuting diagram: 
\[       \begin{tikzcd}
            {H_0(\rhomlin(P,P))(*)} \arrow[d, "\alpha"'] & {H_0(P\otimes^L_{(A,A_+)_\square}\rhomlin(P,A))(*)=:M} \arrow[d, "\alpha", two heads] \arrow[l] \\
            {H_0(\rhomlin(P,C))(*)}                      & {H_0(C\otimes^L_{(A,A_+)_\square}\rhomlin(P,A))(*)=:N} \arrow[l, "\simeq"]                      
            \end{tikzcd}
            \]
So by taking $\alpha \in {H_0(\rhomlin(P,C))(*)}$, identifying it in $N$ and finding a preimage in $M$, we get a trace class $g\in H_0(\rhomlin(P,P))(*) $, with the following commuting diagram:
\[
    \begin{tikzcd}
        P \arrow[rd, "\alpha"] \arrow[d, "g"', dotted] &   \\
        P \arrow[r, "\alpha"]                          & C
        \end{tikzcd}    
\]
Now this factors uniquely through 
\[
    \begin{tikzcd}
        P \arrow[rd, "\alpha"] \arrow[d, "g"']                          &   \\
        P \arrow[r, "\alpha"] \arrow[d]                                 & C \\
        \mathrm{Cone}(P\xrightarrow{1-g}P) \arrow[ru, dotted] &  
        \end{tikzcd}   
\]
Then by inducting on the degree, it is enough to show, that $\cone(P\xrightarrow{1-g}P)$ lies in $\perf(A(*))$, but this is classical trace norm theory \footnote{"This is a step where you actually feel like you're doing somewhat a little bit of Analysis."}.
\end{proof}







% \[
%     \begin{tikzcd}
%     C: & \dots \arrow[r] & P \arrow[r]                       & P \arrow[r]                                                   & 0 \arrow[r] & 0 \arrow[r] & \dots \\
%        &                 & P \arrow[ru, "\exists f", dotted] &                                                               &             &             &       \\
%        & \dots \arrow[r] & 0 \arrow[r]                       & P \arrow[r] \arrow[uu, "id"'] \arrow[lu, "\exists g", dotted] & 0 \arrow[r] & 0 \arrow[r]       &  \dots    
%     \end{tikzcd}
%     \]