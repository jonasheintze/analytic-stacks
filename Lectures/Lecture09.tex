% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 9 (Clausen, 22.11.2023) -- {\it Localization in Solid Analytic Rings}]{Lecture 9 (9/24)}
\subsection*{Recall:}
Suppose $R^\rhd\in\alg(\solid_\Z)$ and let $R^\circ\subseteq R^\rhd(*)$ be the set of power bounded elements. For any $R^+\subseteq R^\circ$, one gets an analytic ring structure on $R^\rhd$, denoted by $(R^\rhd, R^+)_\square$ with \[\module_{(R^\rhd,R^+)_\square}=\left\{M\in\module_{R^\rhd}\;\big|\; \subalign{&\forall f\in R^+\\&\homline(P,M)\xrightarrow[\sim]{1-\sigma f}\homline(P,M)} \right\}\]

\begin{remark}\leavevmode
    \begin{description}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
        \item[-] $1\in R^+ \Rightarrow M\in \module_R$ is in $\solid_\Z$. 
    \item[-] $R^+\subseteq R^\circ\Rightarrow R^\rhd\in \module_R$.
    \item[-] No loss in assuming $R^+\subseteq R(*)$ is an integrally closed subring with $R^{\circ\circ}\subseteq R^+$.
    
\end{description}
\end{remark}
\begin{example}
    If $R^\rhd$ condensed Huber ring we get that integrally closed subrings $R^+$ of the form $R^{\circ\circ}\subseteq R^+\subseteq R^\circ$ are the same thing as open integrally closed subrings $R^+\subseteq R^\circ$ which are the same as the $R^+$ in Huber's theory. This notation will from now on be used interchangeably.
\end{example}
\subsection{Localization}
\begin{remark}
    Let $R$ be a solid ring and consider the pair $(R, R^+)$. Then, for the pair $(\underline{R(*)}, R^+)$ the following equation holds:
    \[
        \module_{(R,R^+)_\square} = \module_R\bigl(\module_{(\underline{R(*)},R^+)_\square}\bigr)
    \] This holds, as being a solid module is being calculated in the discrete underlying ring. Hence it suffices to build our theory over discrete cases as we only need to put $R$-module structure over the discrete case for arbitrary $R$.
\end{remark}

\noindent\textbf{Analogy:} Let $R$ be a commutative ring, and the usual derived category of $R$-modules $D(R)$ localizes over $\spec(R)$. \\
$\spec(R)$ is the set of prime ideals and has the following properties:
\begin{enumerate}
    \item has a basis of qc. opens closed under finite intersections, which are the so-called "distinguished opens", and which are of the form $U(f)=\{\mathfrak{p}: f\notin \mathfrak{p}\}$.
    \item has a structure sheaf $\mathcal{O}\bigl(U(f)\bigr) = R\bigl[\frac{1}{f}\bigr]$.
    \item $U(f)\cong \spec\bigl(R[\frac{1}{f}]\bigr)$ matches up distinguished opens.
    \item for $U$ distinguished open $\leadsto D\bigl(\mathcal{O}(U)\bigr)$\\
    for $U\supseteq V$ $\leadsto$ base change $D\bigl(\mathcal{O}(U)\bigr)\rightarrow D\bigl(\mathcal{O}(V)\bigr)$
\end{enumerate}
\begin{theorem}[Lurie]
    This presheaf is a sheaf of $\infty$-categories.
\end{theorem}
\begin{warning} In the setting that is going to be discussed next, we will only be able to get a sheaf on derived level and not on the level of abelian categories, as localization maps will not be flat in general.
\end{warning}
\begin{notation}
    As known, we can assign to a Huber pair $(A,A^+)$ the analytic ring $(A,A^+)_\square$. Then the solid derived modules of this analytic ring will be denoted as $D(A,A^+)_\square$.
\end{notation}
\noindent \textbf{Properties for spv:} Let $R$ be a discrete Huber pair (i.e. $R$ commutative ring and, $R^+\subseteq R$ int. closed subring). Then the claim is, that $D(R,R^+)_\square$ localizes on $\spv(R,R^+)$, where
 \[
    \spv(R,R^+)=\left\{v: R\rightarrow\Gamma\cup\{0\}\quad\big|\quad \subalign{&v(gf)=v(g)v(f)\\
    &v(f+g)\leq\mathrm{ max}\{v(f),v(g)\}\\
    &v(0)=0, v(1)=1\\
    &v(R^+)\leq 1}\right\}\big/\sim
 \]
 By $\Gamma$ we denote an ordered abelian group written multiplicatively and the equivalence relation $\sim$ is given by the usual equivalence of valuations\footnote{Note that the difference to $\mathrm{Spa}$ is, that we don't require the valuations to be continuous. One could describe $\spv$ as $\mathrm{Spa}$ where we take the discrete topology on $R$.}. But this is also just the same thing as\[
        \left\{ (A,\mathfrak{p}) \mid A\subseteq k(\mathfrak{p}) \text{ valuation domain}, \im(R^+)\subseteq A \right\}
 \] 
 \begin{definition}\label{structuresheaf}
We now define for a discrete Huber pair $R$:
 
 \begin{enumerate}
    \item \textit{Basis of qc opens:} "rational opens", closed under finite intersections, for $g,f_i\in R$: \[
     U\left(\frac{f_1,\dots, f_n}{g}\right):= \left\{ v \mid v(g)\neq 0, v(f_i)\leq v(g)\quad \forall i\right\}   
 \]
 \item \textit{Structure sheaves:}\begin{align*}
    &\mathcal{O}\left(U\left(\frac{f_1,\dots, f_n}{g}\right)\right) = R\Bigl[\frac{1}{g}\Bigr]\\
    &\mathcal{O}^+\left(U\left(\frac{f_1,\dots, f_n}{g}\right)\right) = \overline{R^+\Bigl[\frac{f_1}{g},\dots, \frac{f_n}{g}\Bigr]}
 \end{align*}
 where $\overline{(-)}$ denotes the integral closure.
\item\textit{Matching rational opens:}\[
        U\left(\frac{f_1,\dots, f_n}{g}\right) \cong \spv\left(\mathcal{O}(U), \mathcal{O}^+(U)\right)
\]
\item $U$ rational open $\leadsto D\left(\mathcal{O}(U),\mathcal{O}^+(U)\right)_\square$\\
$U\supseteq V \leadsto$ pullback $D\left(\mathcal{O}(U),\mathcal{O}^+(U)\right)_\square\rightarrow D\left(\mathcal{O}(V),\mathcal{O}^+(V)\right)_\square\quad$\footnote{This comes from a map of analytic rings $\left(\mathcal{O}(U),\mathcal{O}^+(U)\right)_\square\rightarrow\left(\mathcal{O}(V),\mathcal{O}^+(V)\right)_\square$.}
\end{enumerate}
\end{definition}
\begin{theorem}
    This presheaf is a sheaf of $\infty$-categories. 
\end{theorem}
As mentioned earlier, this is not true on abelian level in contrast to the classical case, as pullback is not (but almost) $t$-exact.

\subsection{Sketch of proof of Lurie's theorem} 
From here on we are going to give a proof of the classical theorem (i.e. we are proving Zariski descent for $D(R)$). Here we are working with the site of distinguished opens in $\spec(R)$ with the open cover topology.
\begin{lemma}
    To show that a presheaf has sheaf conditions on $\spec(R)$, it suffices to check sheaf conditions for covers of the form: for $f\in \mathcal{O}(U)$ 
    \[
        \begin{tikzcd}
            U(f) \arrow[rd] &   & U(1-f) \arrow[ld] \\
                            & U &                  
            \end{tikzcd}
    \] 
\end{lemma}
\begin{proof}
    Consider $f_1,\dots, f_n \in \mathcal{O}(U)$ s.t. $\exists x_1,\dots, x_n \in \mathcal{O}(U)$, $x_1f_1+,\dots, x_nf_n = 1$, which form a general cover $\{U(f_i)\}_{i\in I}$ covering $U$. But this cover is refined by a cover $\{U(f_ix_i)\}_{i\in I}$ hence we can assume w.l.o.g. $f_1+\dots+f_n=1$. Now we induct on $n$, i.e. we cover $U$ by $U(f_1+\dots+f_{n-1})$ and $U(f_n)$. Now to cover $U(f_1+f_{n-1})$ we do the same step. \\
The induction idea is the following: let $f_1+f_2+f_3 = 1$; then consider, that $U\bigl(f_1+f_2\bigr)\cup U\bigl(f_3\bigr)$ satisfies sheaf conditions. We want to show from here, that the cover $U\bigl(f_1\bigr)\cup U\bigl(f_2\bigr)\cup U\bigl(f_3\bigr)$ satisfies sheaf conditions. As it is a split cover on $U(f_3)$, we are done on that side. Now we need to show that sheaf conditions on $U\bigl(f_1\bigr)\cup U\bigl(f_2\bigr)$. We can do that algebraically as 

\begin{align*}
U\bigl(f_1+f_2\bigr) &= \spec\Bigl(R\Bigl[\frac{1}{f_1+f_2}\Bigr]\Bigr) \\&
= \spec\Bigl(R\Bigl[\frac{1}{f_1+f_2}\Bigr]\Bigl[\frac{1}{f_1/(f_1+f_2)}\Bigr]\Bigr)\cup \spec\Bigl(R\Bigl[\frac{1}{f_1+f_2}\Bigr]\Bigl[\frac{1}{f_2/(f_1+f_2)}\Bigr]\Bigr)\\
& = \spec\Bigl(R\Bigl[\frac{1}{f_1+f_2}\Bigr]\Bigl[\frac{1}{f_1}\Bigr]\Bigr)\cup \spec\Bigl(R\Bigl[\frac{1}{f_1+f_2}\Bigr]\Bigl[\frac{1}{f_2}\Bigr]\Bigr) \\
&= \Bigl(U\bigl(f_1+f_2\bigr)\cap U(f_1)\Bigr) \cup \Bigl(U\bigl(f_1+f_2\bigr)\cap U(f_2) \Bigr) 
\end{align*}
\end{proof}
\noindent Now we are done if we check that the sheaf condition holds for 
\[
    \begin{tikzcd}
        U(f) \arrow[rd] &   & U(1-f) \arrow[ld] \\
                        & U &                  
        \end{tikzcd}
\] Hence we need to check whether \[
    \begin{tikzcd}
        & {D\bigl(R\bigl[\frac{1}{f}\bigr]\bigr)} \arrow[rd]   &                          \\
D(R) \arrow[rd] \arrow[ru] &                                  & {D\bigl(R\bigl[\frac{1}{f(1-f)}\bigr]\bigr)} \\
        & {D\bigl(R\bigl[\frac{1}{1-f}\bigr]\bigr)} \arrow[ru] &                         
\end{tikzcd}
\] is a pullback of $\infty-$categories. This is the same as claiming that \[
    D(R)\xrightarrow{\sim}D\Bigl(R\Bigl[\frac{1}{f}\Bigr]\Bigr)\times_{D\bigl(R\bigl[\frac{1}{f(1-f)}\bigr]\bigr)}D\Bigl(R\Bigl[\frac{1}{1-f}\Bigr]\Bigr)
\]
\begin{proof}
    Note: each base change functor has a right adjoint\\
    $\Rightarrow D(R)\rightarrow D\bigl(R\bigl[\frac{1}{f}\bigr]\bigr)\times_{D\bigl(R\bigl[\frac{1}{f(1-f)}\bigr]\bigr)}D\bigl(R\bigl[\frac{1}{1-f}\bigr]\bigr)$ does too:

    \begin{align*}
    D(R) &\leftarrow D\Bigl(R\Bigl[\frac{1}{f}\Bigr]\Bigr)\times_{D\bigl(R\bigl[\frac{1}{f(1-f)}\bigr]\bigr)}D\Bigl(R\Bigl[\frac{1}{1-f}\Bigr]\Bigr) \\
    M\times_{M\bigl[\frac{1}{f}\bigr]}N & \,\reflectbox{\rotatebox[origin=c]{0}{$\mapsto$}} \bigl(M,N,\alpha\bigr)
    \end{align*} with $\alpha: M\bigl[\frac{1}{1-f}\bigr]\cong N\bigl[\frac{1}{f}\bigr]$. We need to check, that the unit and counit are isomorphisms.\\
Exemplary For $M\in D(R)$ we need to show for the unit:
\[
    \begin{tikzcd}
        M \arrow[r] \arrow[d]                  & {M\bigl[\frac{1}{f}\bigr]} \arrow[d] \\
        {M\bigl[\frac{1}{1-f}\bigr]} \arrow[r] & {M\bigl[\frac{1}{f(1-f)}\bigr]}     
        \end{tikzcd}    
\] is a pullback. This follows from: $N\in D(R)$, s.t. $N\bigl[\frac{1}{f}\bigr] = N\bigl[\frac{1}{1-f}\bigr]=0 \Rightarrow N=0$\\
Analog for counit.
\end{proof}
\begin{remark}\label{formals}
    Formally, what is used is the following:
    \begin{enumerate}
        \item each base change is a localization (right adjoint fully faithful)
        \item Those localizations $M\mapsto M\bigl[\frac{1}{f}\bigr], M\bigl[\frac{1}{1-f}\bigr]$
        \item if $M\mapsto0$ on each element of the cover, then $M=0$.
    \end{enumerate}
\end{remark}
\subsection{Proof of solid analog}
\noindent Now we want to proof the solid analog:\\
We consider the site of rational opens $U\subseteq \spv(R,R^+)$ with the Grothendieck topology of open covers. 
\begin{lemma}
To show the sheaf condition on this site, we only need to show it for the empty cover and for covers of the following form: for rational open: $U\subseteq\spv(R,R^+)$ and all $f\in\mathcal{O}(R)$
\[
\begin{tikzcd}
        U\bigl(\frac{f}{1}\bigr) \arrow[rd] &   & U\bigl(\frac{1}{f}\bigr) \arrow[ld] & \& & U\bigl(\frac{1}{f}\bigr) \arrow[rd] &   & U\bigl(\frac{1}{1-f}\bigr) \arrow[ld] \\
                                            & U &                                     &   &                                     & U &                                      
\end{tikzcd}
\]
\end{lemma}
\begin{proof}
    Huber showed, that  every cover can be refined by a cover of the form 
     \[
        \Bigl\{U\bigl(\frac{f_1,\dots, \hat{f}_i, \dots, f_n}{f_i} \bigr)\Bigr\}_{i\in I}
        \] where there is a collection $f_1,\dots,f_n$ generating the unit ideal. From here we use the same argument as in the classical proof.
\end{proof}

\noindent Now the theorem reduces to $(R,R^+)$ discrete Huber pair, $f\in R$, then we need that 
\[
    D\bigl(R,R^+\bigr)_{\square}\xrightarrow{\sim} D\Bigl(R\Bigl[\frac{1}{f}\Bigr], \widetilde{R^+\Bigl[\frac{1}{f}\Bigr]}\Bigr)_{\square} 
    \times_{D\bigl(R\bigl[f, \frac{1}{f}\bigr], \widetilde{R^+\bigl[f,\frac{1}{f}\bigr]}\bigr)_{\square}}
    D\bigl(R\bigl[f\bigr], \widetilde{R^+\bigl[f\bigr]}\bigr)_\square
\]
and 
\[
    D\bigl(R,R^+\bigr)_{\square}\xrightarrow{\sim} D\Bigl(R\Bigl[\frac{1}{f}\Bigr], \widetilde{R^+\Bigl[\frac{1}{f}\Bigr]}\Bigr)_{\square} 
    \times_{D\bigl(R\bigl[\frac{1}{(1-f)f}\bigr], \widetilde{R^+\bigl[f,\frac{1}{(1-f)f}\bigr]}\bigr)_{\square}}
    D\Bigl(R\Bigl[\frac{1}{1-f}\Bigr], \widetilde{R^+\Bigl[\frac{1}{1-f}\Bigr]}\Bigr)_\square 
\]

\noindent For $f\in R:$ 
\begin{enumerate}
    \item For $U\bigl(\frac{f}{1}\bigr)$\[
    D\bigl(R,R^+\bigr)\rightarrow D\bigl(R,\widetilde{R^+[f]}\bigr)
    \] 
    is just $T-$solidification for $\Z[T]\xrightarrow{T\mapsto f}R$, hence it is $\rhomlin_{\Z[T]}\bigl(\Z((T^{-1}))/Z[T][-1],-\bigr)$ 
    \item For $U\bigl(\frac{1}{f}\bigr)$
    \[D\bigl(R,R^+\bigr)\rightarrow D\Bigl(R\Bigl[\frac{1}{f}\Bigr],\widetilde{R^+\Bigl[\frac{1}{f}\Bigr]}\Bigr)\]
    is the functor, that first inverts $f$, then $T-$solidifies for $\Z[T]\xrightarrow{T\mapsto \frac{1}{f}}R\bigl[\frac{1}{f}\bigr]$
\end{enumerate}
Base change is given by 
\[
    \rhomlin_{\Z[T]}\bigl(\Z\llbracket T\rrbracket/\Z[T][-1],-\bigr)
\] where $\Z[T]\xrightarrow{T\mapsto f}R$. By lecture 7, this the localization killing the idempotent object $\Z\llbracket T\rrbracket\in D(\Z[T],\Z)_\square= D(\module_{\Z[T]}(\solid_\Z))$. (Automatically inverts $f$ because anything $T-$torsion is a $\Z[T]-$module).

\noindent Now in the solid setting, the conditions of \ref{formals} are satisfied: 
\begin{enumerate}
    \setcounter{enumi}{1}
    \item The localizations commute, as they are both given by $\rhomlin$ out of objects and these commute from tensor-hom adjunction.
    \item translates for $\bigl(U=U\bigl(\frac{1}{f}\bigr)\cup U(\frac{f}{1})\bigr)$ to $\Z\llbracket T\rrbracket\otimes^\square_{\Z[T]}\Z((T^{-1}))=\Z\llbracket T,U\rrbracket/(1-UT)=0$.  For the last equation one can use the geometric series.\\
    Similary we need that $\Z\llbracket T\rrbracket\otimes_{\Z[T]}\Z\llbracket 1-T\rrbracket=\Z\llbracket T,U\rrbracket/(1-(U+T))=0$
\end{enumerate}

\noindent The interpretation of a covering of the form $U= U=U\bigl(\frac{1}{f}\bigr)\cup U(\frac{f}{1})$ is the following: The left side is localizing away from open unit disk and the right side is localizing to the closed unit disk.
\begin{corollary}
    $R$ solid ring, $R^+\subseteq R^\circ\subseteq R(*)$
    $\Rightarrow D(R,R^+)$ localizes on $\spv(R(*),R^+)$ via \[U \mapsto \module_{R}\left(D(\mathcal{O}(U),\mathcal{O}^+(U))\right)\] for $U$ rational open 
\end{corollary}
\begin{remark}
    In fact, $D(R,R^+)$ actually lies on the closed subset $\spv(R(*),R^+, R^{\circ\circ})$.\footnote{Here with this information we add the condition , that for $f\in R^{\circ\circ}\Rightarrow v(f)<1$} On the other hand Huber studies\begin{align*}
    \textrm{Spa}(R,R^+) &= \{\textrm{continuous valuations on }R\textrm{, which are } \leq 1 \textrm{ on } R^+ \}  \\
    &= \textrm{subset of } \spv(R(*),R^+,R^{\circ\circ}) \textrm{ s.t. } f\in R^{\circ\circ}\\
    & \Rightarrow \forall \gamma\in \Gamma, \exists N\in \N \textrm{ s.t. } v(f^N)<\gamma
    \end{align*}
$D(R,R^+)$ does live over $\textrm{Spa}(R,R^+)\subseteq \spv(R,R^+)$ but $\exists$ retraction \[
        \spv(R,R^+)  \xrightarrow{r} \text{Spa}(R,R^+)
\] realizing $\mathrm{Spa}(R,R^+)$ as a quotient
$\Rightarrow$ sheaf of categories on $\mathrm{Spa}(R,R^+)$.
\end{remark}
\noindent In general one gets more flexibility for localization using this machinery than in Huber's machinery as the kind of extra things one can get are $\Z_p\llbracket x\rrbracket\bigl[\frac{1}{p}\bigr]$ which arises from the structure sheaf of the general setting but not in the structure sheaf of Huber's theory.