% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 7 (Clausen, 15.11.2023) -- {\it The Solid Affine Line}]{Lecture 7 (7/24)}
\subsection*{Recall:}
\[\begin{tikzcd}
    \solid_{\Z} \arrow[r, "\subseteq", phantom] & \cond_\Z^{\mathrm{light}} \arrow[l, "(-)^\square"', dotted, bend right=49]
    \end{tikzcd}\]
where the left side is to be understood as a complete non-archimedean topological groups and the inclusion is as an abelian subcategory and is closed under (co)lim, extensions, \dots. The dotted arrow above is the so-called solidification and is adjoint to the inclusion.\\
Recap, that $P$ is also a ring and $\exists$ a ring map\[
    \Z[T]\longrightarrow P,\]
where multiplication by $T$ is a shift in $P$. Solidifying this map we get 
\[\Z[T]\longrightarrow \Z\llbracket T \rrbracket\]
\begin{lemma}
    \[\Z\llbracket T \rrbracket\otimes_{\Z[T]}^{L\square}\Z\llbracket T \rrbracket\cong\Z\llbracket T \rrbracket\]
\end{lemma}
\begin{proof}
    \[\Z\llbracket T_1\rrbracket\otimes_\Z^\square\Z\llbracket T_2\rrbracket= \Z\llbracket T_1,T_2\rrbracket
        \]
        Now to get the desired result, we just mod out the ideal $(T_1-T_2)$.
\end{proof}
\subsection{Intuition of the Open Unit Disk}
%Whenever we are talking about $\Z[T]$ modules, we denote by $P=\Z[T][\ninfty]$. \\
\noindent The interpretation of $\Z[T]$ now is something like $\mathbb{A}^1$ and the interpretation of $\Z\llbracket T \rrbracket$ is something like some subspace of $\mathbb{A}^1$. The naive interpretation of that subspace would be an open neighbourhood of the origin in $\mathbb{A}^1$. But this intuition is getting problematic as we look at base change: \\
E.g. 
\[\Q_p\otimes^\square_\Z\Z\llbracket T \rrbracket= (\Z_p\otimes\Z\llbracket T \rrbracket)\Bigl[\frac{1}{p}\Bigr]=\Z_p\llbracket T \rrbracket\Bigl[\frac{1}{p}\Bigr]\] 
The last equation holds, as $\Z_p=\Z\llbracket U \rrbracket/(U-p)$. But $\Z_p\llbracket T \rrbracket\bigl[\frac{1}{p}\bigr]\subsetneq \Q_p\llbracket T \rrbracket$, where the inclusion only has coefficients with bounded $p$-adic norm.\\
Hence we may interpret $\Z_p\llbracket T \rrbracket\bigl[\frac{1}{p}\bigr]$ as the ring of bounded functions on the open unit disc in $\mathbb{A}^1_{\Q_p}$.\\
As an interpretation that means, if we take an element of $\Q_p$, it converges on all $f\in \Z_p\llbracket T \rrbracket\bigl[\frac{1}{p}\bigr]$, plugged in as $T$, iff its absolute value is smaller than $1$.\\
This suggests the interpretation that "$\Z\llbracket T \rrbracket\longhookrightarrow\Z[T]$" corresponds to the open unit disk. \\
But what about the closed unit disk? 
\\From complex geometry we know, that taking the complement of the closed unit disk in $\mathbb{P}^1_\C$ can be portrayed as an open  simply connected properly contained set in $\C=D(z)$ (we can take this chart as we excluded the origin). Now by Riemann mapping theorem this is isomorphic to the open unit disk at $\infty$. Denote this disk as $U_\infty$. So the intuition is that we want to construct the open unit disk at $\infty$ as the complement. But that is just the same as taking $\C\setminus (U_\infty\setminus\{\infty\})$.
\\Hence it can be thought of as 
\begin{equation}\label{moteq}
    \Z((T^{-1}))=\Z\llbracket T^{-1}\rrbracket\otimes_{\Z[T^{-1}]} \Z[T,T^{-1}]\longleftarrow \Z[T]
\end{equation}
To get the closed unit disk, "kill" $\Z((T^{-1}))$.

\paragraph{Note:}\[
    \Z\llbracket U \rrbracket[T]\xrightarrow{UT-1}\Z\llbracket U \rrbracket[T]\longrightarrow\Z((T^{-1}))\] 
$2$-term resolution by compact projective generator of $\module_{\Z[T]}(\solid_\Z)$.

\noindent So killing $\Z((T^{-1}))$ is the same as requesting the $UT-1$ map to become an isomorphism. This suggests:
\begin{definition}
    $M\in\module_{\Z[T]}(\solid_\Z)$ is \textbf{$\Z[T]$-solid} iff
    \begin{align*}&\underline{\mathrm{Hom}}_\Z(P,M)\xrightarrow{\sigma T-1}\underline{\mathrm{Hom}}_\Z(P,M)\\ \iff &
\underline{\mathrm{Hom}}_{\Z[T]}(P\otimes_\Z\Z[T],M)\xrightarrow{\sigma T-1}\underline{\mathrm{Hom}}_{\Z[T]}(P\otimes_\Z\Z[T],M)\end{align*} is an isomorphism, where $\sigma$ is the shift.
\end{definition}
\noindent The \textbf{Note} from above is suggesting, that this is equivalent to $\underline{R\mathrm{Hom}}(\Z((T^{-1})),M)=0$.
\begin{lemma**}\label{technical1}\footnote{Thanks to Ferdinand to explaining this Lemma with proof to us! We will need this as a technical statement for a proof in the next Theorem!}
    For $D(\Z[T])$-modules $M$ and $N$ where $T_M$ denotes the $T$-action on $M$ and $T_N$ analogous on $N$, we have the equality \[
        \underline{R\mathrm{Hom}}_{\Z[T]}(M,N)\simeq \fib\left(\underline{R\mathrm{Hom}}_\Z(M,N)\xrightarrow{T_N-T_M}\underline{R\mathrm{Hom}}_\Z(M,N)\right)  
    \]
\end{lemma**}

\begin{proof*}
    \begin{align*}
        &M \simeq M\otimes^L_{\Z[T]}\Z[T]\\
        &\simeq M\otimes^L_{\Z[T_M]}\cofib\left(\Z[T_M,T_N]\xrightarrow{(T_N-T_M)}\Z[T_M,T_N]\right)\\
        &\simeq \cofib\left(M\otimes^L_{\Z[T]}\bigl(\Z[T_M]\otimes_\Z^L\Z[T_N]\bigr)\xrightarrow{(T_N-T_M)}M\otimes^L_{\Z[T]}\bigl(\Z[T_M]\otimes_\Z^L\Z[T_N]\bigr)\right)\\
        &\simeq\cofib\left(M\otimes^L_\Z \Z[T_N]\xrightarrow{T_M-T_N} M\otimes^L_\Z\Z[T_N]\right)
    \end{align*}
    Now 
    \begin{align*}
        &\underline{R\mathrm{Hom}}_{\Z[T]}(M,N)\\
        &\simeq\rhomlin_{\Z[T]}\left(\cofib\left(M\otimes^L_\Z \Z[T_N]\xrightarrow{T_M-T_N} M\otimes^L_\Z\Z[T_N]\right),N\right)\\
        &\simeq\fib\left(\rhomlin_{\Z[T_N]}\bigl(M\otimes_\Z^L\Z[T_N],N\bigr)\xrightarrow{T_M-T_N}\rhomlin_{\Z[T_N]}\bigl(M\otimes_\Z^L\Z[T_N],N\bigr)\right)\\
        & \simeq\fib\left(\rhomlin_\Z(M,N)\xrightarrow{T_M-T_N}\rhomlin_\Z(M,N)\right)
    \end{align*}
\end{proof*}

\begin{lemma**}\label{techincal2}\footnote{Also here thanks to Ferdinand for this lemma and proof!}
    Let $X$ be a chain complex in $D(\module(\Z[T]))$ and $f:X\rightarrow X$, then \[
            X\simeq\cofib\left(X[T]\xrightarrow{T-f}X[T]\right)  
    \] 
\end{lemma**}
\begin{proof*}
    \begin{align*}
        &\cofib\left(X[T]\xrightarrow{T-f}X[T]\right)\\
        &\simeq \cofib\left(X\otimes_{\Z[f]}^L \Z[f,T]\xrightarrow{T-f} X\otimes_{\Z[f]}^L \Z[f,T]\right)
    \end{align*}
    \noindent From here everything analogous to last lemma. 
\end{proof*}
\begin{theorem}
    \[\solid_{\Z[T]}\subset\module_{\Z[T]}(\solid_\Z)\subset \module_{\Z[T]}(\lcondab)\] 
    The following holds:
    \begin{description}[itemsep = -1mm]
        \item[-] $\solid_{\Z[T]}$ is closed under (co)lim, extensions
        \item[-]for any $M\in\cond_{\Z[T]}^\mathrm{light}\Rightarrow\underline{\ext}^i(M,N)\in\solid_{\Z[T]}$ for $N\in\solid_{\Z[T]}$
        \item[-] there is a left adjoint $(-)^{T\square}$ to the first inclusion
        \item[-] there is a symmetric monoidal structure on $(-)^{T\square}$
        \item[-] the derived analog holds as well
        \item[-] $\left(\left(\prod_\N\Z\right)[T]\right)^{T\square}=\prod_\N\Z[T]$
    \end{description}
\end{theorem}
\begin{remark}
    As noted before, $\solid_\Z$ is analogous to complete abelian non-archimedean topological groups, where we have a basis of neighbourhoods of the origin consisting of abelian subgroups. The interpretation of $\solid_{\Z[T]}$ should also be complete non-archimedean, i.e. there is a basis of neighbourhood consisting of $\Z[T]$ submodules. A non-archimedean $\Z[T]$-module which is an element of $\solid_\Z$ but not in $\solid_{\Z[T]}$ would be $\Z((T^{-1}))$. Hence what the theorem tells us, is that "killing" such objects explains the difference of those notions.
\end{remark}
\begin{proof}[Proof of Theorem.]
    The proof will consist of a few claims which are going to be sketched step by step.
    All except last property are exactly the same as for $\solid_\Z$.\\
    \underline{Claim}: For $M\in \module_{\Z[T]}(\solid_\Z)$: \[
            M^{LT\square}=\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M\right)
    \]
    Now this gives us what we want, as we get a map 
    \[
        M\simeq\underline{R\mathrm{Hom}}_{\Z[T]}(\Z[T],M)\longrightarrow\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M\right)
    \]which gives us the derived $T$-solidification.
    \begin{adjustwidth}{1cm}{}
        \textit{Proof of Claim$^*$}.\footnote{Thanks to Ferdinand for explaining this to us!}\indent We first show that $\Z[T]$-modules of that form are solid. For this we need to show, that \[\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1})), \underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M\right)\right)=0\]
        To show this, we will use that $\Z((T^{-1}))\otimes^{L\square}_{\Z[T]}\Z((T^{-1}))=\Z((T^{-1}))$ which follows from (\ref{moteq}). We can write the above expression as 
        \begin{align*}
            &\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))\otimes_{\Z[T]}\Z((T^{-1}))/\Z[T][-1], M\right)\\
            &\simeq\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))\otimes_{\Z[T]}^{L\square}\Z((T^{-1}))/\Z[T][-1], M\right)
        \end{align*}
        To show that this equality holds, we show that for all $K$:\[\mathrm{Hom}_\Z\left(K,\underline{R\mathrm{Hom}}_\Z(N^{L\square},M)\right)\xrightarrow[\sim?]{\yo}\mathrm{Hom}_\Z\left(K,\underline{R\mathrm{Hom}}_\Z(N,M)\right)\;\footnote{This suffices as the adjunction on $\Z$ level induces an adjunction between $\module_{\Z[T]}\left(D(\lcondab)\right)$ and $\module_{\Z[T]}\left(D(\solid_\Z)\right)$.}\]
        By left adjointness of solidification, $\mathrm{Hom}_\Z(N,M)=\mathrm{Hom}_\Z(N^{L\square}, M)$  holds for solid $M$ modules. Using that, the above map is an isomorphism by Tensor-Hom-adjunction, as  \begin{align*}
            &\mathrm{Hom}_\Z\left(K,\underline{R\mathrm{Hom}}_{\Z}(N^{L\square},M)\right)\simeq \mathrm{Hom}_\Z\left(N^{L\square}, \underline{R\mathrm{Hom}}_\Z(K,M)\right)\\
            &\simeq\mathrm{Hom}_\Z\left(N,\underline{R\mathrm{Hom}}_\Z(K,M)\right) \simeq \mathrm{Hom}_\Z\left(K,\underline{R\mathrm{Hom}}_\Z(N,M)\right)
        \end{align*}

        \noindent Now it's left to show, that $\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))\otimes_{\Z[T]}^{L\square}\Z((T^{-1}))/\Z[T][-1], M\right)\simeq0$
        which follows from 
        \begin{align*}
            &\Z((T^{-1}))\otimes_{\Z[T]}^{L\square}\Z((T^{-1}))/\Z[T][-1]\\
            &\simeq \cofib\left(\Z((T^{-1}))\otimes^{L\square}_{\Z[T]}\Z[T]\rightarrow \Z((T^{-1}))\otimes^{L\square}_{\Z[T]}\Z((T^{-1}))\right)\\
            &\simeq \cofib\left(\Z((T^{-1}))\rightarrow\Z((T^{-1}))\right)\simeq 0    
        \end{align*}
        So now any module of the form of our functor is solid. Note that objects will still be mapped to the same thing if you apply the functor twice:
        \begin{align*}
            &\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M\right)\right)\\
            &\simeq\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1]\otimes_{\Z[T]}\Z((T^{-1}))/\Z[T][-1],M\right)\\
            &\simeq\underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M\right)
        \end{align*}
        \noindent From here there a still a few things left to show which we will not cover. 
        \end{adjustwidth}
    \underline{Next Claim}:
        \begin{align*}
            D(\solid_\Z)&\longrightarrow D(\solid_{\Z[T]})\\
            M&\longmapsto (M\otimes_\Z \Z[T])^{LT\square}
        \end{align*} 
        is $t$-exact, preserves (co)limits and sends $\Z\mapsto\Z[T]$.\\
        Note that if this functor commutes with products, this claim already proves the last point of the Theorem, as we would only need to understand what happens on $\Z$.  
        \begin{adjustwidth}{1cm}{}
            \textit{Proof of Claim.}
            \begin{align*}
                &\left(M\otimes\Z[T]\right)^{LT\square}\simeq \underline{R\mathrm{Hom}}_{\Z[T]}\left(\Z((T^{-1}))/\Z[T][-1],M[T]\right)=\\
                &\fib\left(\underline{R\mathrm{Hom}}_{\Z}\left(U\Z\llbracket U \rrbracket[-1], M[T]\right)\xrightarrow{T-\sigma}\underline{R\mathrm{Hom}}_{\Z}\left(U\Z\llbracket U \rrbracket[-1], M[T]\right)\right)
            \end{align*}
where $\sigma$ denotes the shift operator induced by $\cdot T$ in $\Z((T^{-1}))/\Z[T][-1]$. The last equation holds because of Lemma \ref{technical1}. Now as $U\Z[U][-1]$ is compact projective over $\Z$, we get \[
                \fib\left(\underline{R\mathrm{Hom}}_{\Z}\left(U\Z[U][-1], M\right)[T]\xrightarrow{T-\sigma}\underline{R\mathrm{Hom}}_{\Z}\left(U\Z\llbracket U \rrbracket[-1], M\right)[T]\right), 
            \] since $\underline{R\mathrm{Hom}}$ with a compact internally projective domain commutes with colimits. By Lemma \ref{techincal2} we get \[\underline{R\mathrm{Hom}}_{\Z}(U\Z\llbracket U \rrbracket[-1],M).\]
            On the way we "lost" the $\Z[T]-$module structure, which can be recovered, by denoting the multiplication with $T$ on the first component, that sends 
            \begin{align*}
                U&\mapsto 0\\
                U^2&\mapsto U\\
                U^3&\mapsto U^2
            \end{align*}
            Now as $U\Z\llbracket U \rrbracket[-1]$ is internally projective, this functor is $t$-exact and preserves (co)limits in $\solid_\Z$. But (co)limits in $\solid_{\Z[T]}$ are calculated on the underlying level, as it is part of a module category. \\
            For $\Z\mapsto\Z[T]$ we plug in $\Z$ into $\underline{R\mathrm{Hom}}_{\Z}(U\Z\llbracket U \rrbracket,-)$. Observe that taking $R$Hom from this product of copies of $\Z$ to $\Z$ turns into a direct product of copies of $\Z$. One can check that this is just $\Z[T]$ with its $\Z[T]$-module structure.\qedhere
        \end{adjustwidth}
\end{proof}
\begin{warning} For the second claim of the proof, it is important to note, that $T$-solidification itself is \underline{not} $t$-exact, only the composition!
\end{warning}
\begin{example}
    \begin{align*}
        (\Q_p[T])^{T\square}=(\Z_p[T]^{T\square})\Bigl[\frac{1}{p}\Bigr]= \left(\varprojlim\left((\Z/p^n\Z)\,[T]\right)^{T\square}\right)\Bigl[\frac{1}{p}\Bigr]&=\left(\varprojlim(\Z/p^n\Z)\;[T]\right)\Bigl[\frac{1}{p}\Bigr]\\
        &=\Z[T]_{\hat{p}}\Bigl[\frac{1}{p}\Bigr]
    \end{align*} 
    where the last object can be interpreted as the functions on the closed unit disk, as it is the subset of $\Q_p\llbracket T \rrbracket$ where the coefficients converge to $0$ $p$-adically.
\end{example}
\noindent\textbf{Some philosophical thoughts:} What should be considered as "closed", what should be considered as "open"?
\[
    \begin{tikzcd}
        D\bigl(\module_{\Z((T^{-1}))}(\solid_\Z)\bigr) \arrow[r, "i_*"'] & {D\bigl(\module_{\Z[T]}(\solid_{\Z})\bigr)} \arrow[r, "(-)^{LT\square}","=: j^*"'] \arrow[l, "{-\otimes_{\Z[T]}\Z((T^{-1}))}=:i^*"', bend right=49] \arrow[l, "i^!", bend left=49] & {D\bigl(\solid_{\Z[T]}\bigr)} \arrow[l, "\supseteq=: j_*", bend left=49] \arrow[l, "{M\mapsto[M\rightarrow M\otimes_{\Z[T]}\Z((T^{-1}))]=:j_!}"', bend right=49]
        \end{tikzcd}    
\]
where the upper arrows are the left adjoints and the lower arrows the right adjoints to the straight arrows. With our interpretation of these objects from the beginning of the lecture, this behaves exactly like:\\
$X$ top. space, $Z\subset X$ closed and $U=X\setminus Z$ open

\[
    \begin{tikzcd}
        {D(Sh(Z;\Z))} \arrow[r, "i_*"'] & D(Sh(X;\Z)) \arrow[r, "j^*"'] \arrow[l, "i^*"', bend right=49] \arrow[l, "i^!", bend left=49] & D(Sh(U;\Z)) \arrow[l, "j_!"', bend right=49] \arrow[l, "j_*", bend left=49]
        \end{tikzcd}
    \]
Now it's going to get confusing: As these formalisms work exactly analogous, we see, that the closed unit disk should be thought of as something open and the open unit disk as being something closed.

\subsection{More general setting for "Geometry"}
\textbf{Vista:} Look at solid rings, $R\in \mathrm{Alg}(\solid_\Z,\otimes)$. The main idea is to take the things we know about $\Z[T]$-modules and base change them along any map from $\Z[T]$ into $R$. These maps into $R$ are something like all possible maps in $R$. What we will end up with, is that 
$D(\module_R(\solid_\Z))$ localizes along $\spv(R(*))$\footnote{This is called the "valuative spectrum".}. Small reminder on $\spv$: The basic opens of that topology are \textbf{rational opens}: $X\left(\frac{f_1,...,f_n}{g}\right)$ for $g\neq 0$ and which consist of valuations $\abs{f_i}\leq \abs{g}$. \\
We are going to attach to \[X\left(\frac{f_1,...,f_n}{g}\right)\mapsto \bigl\{M\in D(\module_R(\solid_\Z))\bigr\}\footnote{This is not a set, but a category.}\]
% \footnotetext{This is not a set, but a category.}
s.t. 
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
    \item $\cdot g: M\xrightarrow{\sim}M$
    \item $\underline{\mathrm{Hom}}(P,M)\xrightarrow[\simeq]{\frac{f_i}{g}\sigma-1}\underline{\mathrm{Hom}}(P,M)\; \forall i$
\end{enumerate}
We want to interpret that as 
\[
    \begin{tikzcd}
        g:\spec(R) \arrow[rd] \arrow[r] & \mathbb{A}^1                           &  & \frac{f_i}{g}:\spec(R) \arrow[rd] \arrow[r] & \mathbb{A}^1               \\
                                              & \mathbb{A}^1\setminus0 \arrow[u, hook] &  &                                                   & \mathbb{D} \arrow[u, hook]
        \end{tikzcd}
\]
\noindent In particular, one gets a structure sheaf on $\spv(R(*))$.

\subsection*{Goal for the rest of the lecture:}
\begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
    \item Make $\O\left(X\left(\frac{f_1,...,f_n}{g}\right)\right)$ explicit
    \item Compare it to Huber's theory. 
\end{enumerate} 
\begin{definition}
    Let $R\in\alg(\solid_\Z,\otimes), f\in R(*)$. This gives us a map $\Z[T]\xrightarrow{T\mapsto f} R$. Now $f$ is called \textbf{topologically nilpotent} if the above map factors through $\Z\llbracket T \rrbracket = P^\square$.\footnote{Note that this is the same as the classical notion of being topological nilpotent: the map sends $T^n\mapsto f^n$. If this factors through $P^\square$ this means that the sequence $1, f^1, f^2,...$ extends to a nullsequence.} $f$ is \textbf{power-bounded} if $R\in \solid_{\Z[T]}$ (this is equivalent to $\underline{\mathrm{Hom}}(P,R)\xrightarrow[\simeq]{f\sigma-1}\underline{\mathrm{Hom}}(P,R)$).
\end{definition}
We define $R^\circ\subseteq R(*)$ as the set of power-bounded elements and $R^{\circ\circ}\subseteq R(*)$ as the set of topological nilpotent elements. 
\begin{lemma}\leavevmode
    \begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
        \item $R^\circ\subseteq R(*)$ is an integrally closed subring.
        \item $R^{\circ\circ}\subseteq R^\circ$ is a radical ideal.
    \end{enumerate} 
\end{lemma}
\begin{proof}
    \leavevmode
    \begin{enumerate}
    \item \underline{$R^\circ$ subring:}
    \begin{enumerate}[topsep=0pt,itemsep=-1ex,partopsep=1ex,parsep=1ex]
    \item $1\in R^\circ$: This follows from the definition of being solid, plug in $1$ as $f$.
    \item Closed under addition and multiplication: $f,g\in R^\circ$ then we want to show for any $F\in\Z[X,Y]$, then $F(f,g)\in R^\circ$\[
            \Z[X,Y]\xrightarrow[Y\mapsto g]{X\mapsto f} R
        \] As $f$ and $g$ are already powerbounded, we have that $R$ is a solid $\Z[X]$ and a solid $\Z[Y]$ module. What we need to show, is that for any map 
        \[
        \Z[T]\xrightarrow{F} \Z[X,Y]
\] we get an induced solid $\Z[T]$-structure on $R$.\\
Resolve $R$ by $\bigoplus\prod_\N\Z[X,Y]$'s.
\begin{description}
    \item[] Solidfy $X^\square:\;\bigoplus\left(\prod\Z[X]\right)[Y]$
    \item[] Solidfy $Y^\square:\;\bigoplus\left(\prod\Z[X,Y]\right)$
\end{description}  
After the last step, we see, that we can resolve $R$ by $\bigoplus\left(\prod\Z[X,Y]\right)$. Each of these are solid modules over $\Z[T]$, as it is a colimit of limits of something that is solid\footnote{$\Z[X,Y]$ is solid over $\Z[T]$ as it is discrete.}.
\end{enumerate}
\underline{$R^\circ$ integrally closed:}\\
 Suppose we have an equation $f^n+ c_{n-1}f^{n-1}+\dots + c_0=0$ with $c_i\in R^\circ$.\\
Now consider \[\Z[x_0,\dots, x_{n-1}]\longrightarrow\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)\]
By hypothesis, if we have a map $\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)\rightarrow R$ the composition gives $R$ a solid module structure over each variable of $\Z[x_0,\dots, x_{n-1}]$. \\
We want to show, that if we have a map \[\Z[T]\longrightarrow \Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0),\] that $R$ is a solid $\Z[T]$-module. Now we do the same trick as before: resolve $R$ as a $\Z[x_0,\dots, x_{n-1}]$-module, solidify at each variable respectively, tensor it up to be a $\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)$-module, by computations\footnote{Here it comes into play, that $\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)$ is finite free over $\Z[x_0,\dots, x_{n-1}]$, such that we can bring in the tensor into the product.} one gets, that we have sums of products of $\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)$ and as $\Z[x_0,\dots,x_{n-1},T]/(T^n+x_{n-1}T^{n-1}+\dots+ x_0)$ is individually solid, and as solid is closed under limits and colimits, one deduces, that it is solid over $\Z[T]$ as well. 
\item $R^{\circ\circ}\subseteq R^\circ$:\\
If we consider the map \[
    \Z\llbracket T \rrbracket\xrightarrow{T\mapsto f} R
\] we get a $\Z\llbracket T \rrbracket$ module structure on $R$. Now if we resolve $R$ as before and then tensor it with $\Z\llbracket T \rrbracket$ we get 
\[ \left(\bigoplus\prod_\N\Z\otimes\Z\llbracket T \rrbracket\right)= \left(\bigoplus\prod_\N\Z\llbracket T \rrbracket\right)
    \] But this is already solid over $\Z[T]$. The rest is left as a fun exercise. 
\end{enumerate}
\end{proof}
\begin{proposition} Let $R\in \alg(\solid_\Z)$, $g,f_1,\dots, f_n\in R(*)$. There is a initial solid ring \[R\longrightarrow R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle ^{\solid}\] s.t. 
\begin{enumerate}
    \item $g$ is invertible in $R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle ^{\solid}$
    \item $\frac{f_i}{g}$ is power-bounded in $R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle ^{\solid}$ for all $i$
\end{enumerate}
\end{proposition}
\noindent The candidate is 
    \[R[x_1,\dots, x_n]^{x_1\square,\dots, x_n\square}/(gx_1-f_1, gx_2-f_2,\dots, gx_n-f_n)\Bigl[\frac{1}{g}\Bigr]\] 
    The reasoning why it satisfies the proposed properties can be easily calculated and to see, that it is a solid object, one sees that all the operations that happen after solidification are just colimits. 
\begin{warning} This is \underline{not} exactly the value of the structure sheaf on $X\left(\frac{f_1,\dots,f_n}{g}\right)$. It is indeed $\pi_0(\text{value of structure sheaf})$. In almost all practical cases, all $\pi_i=0$ for $i>0$.
\end{warning}

\begin{warning} Even if $R$ is nice (complete Huber ring), this $R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle^\solid$ may not be quasi separated. But again in almost all practical cases, it is.
\end{warning}

\noindent\textbf{Relation to Huber's theory:}\\
Let $R$ be a complete Huber ring and $(f_1,\dots,f_n)\subseteq R$ open. Here Huber defines $R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle\huber$.\\
This is the "quasi-separafication" of $\pi_0\left(R\left\langle\frac{f_1,\dots,f_n}{g}\right\rangle^{\solid}\right)$. 
