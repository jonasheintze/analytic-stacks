% TeX root = ../Main.tex

% First argument to \section is the title that will go in the table of contents. Second argument is the title that will be printed on the page.
\section[Lecture 4 (Scholze, 03.11.2023) -- {\it Cohomology in Light Condensed Abelian Groups}]{Lecture 4 (4/24)}
This and the next lecture follow the first course on condensed mathematics that was given in the summer term 2019 to some extent. For further reference see \cite{condensed}.
\subsection{Ext Computations in Light Condensed Abelian Groups}
\begin{theorem}\label{extcomputat}
Let $X$ be a CW-complex, $M$ an abelian group, then \[
\ext^i_\lcondab(\Z[\underline{X}],M)\cong H^i_\mathrm{sing}(X,M)
\]
\end{theorem}

\begin{proof}[Proof sketch]
$X=\bigcup_i X_i$ with each $X_i$ compact metrizable Hausdorff. As both sides are filtered colimits to derived limits, we can now reduce to the $X$ compact Hausdorff case.
\end{proof}

\noindent More general statement: 

\begin{theorem}
Let $X$ metrizable compact Hausdorff, $M$ abelian group. Then \[
\ext^i_\lcondab(\Z[\underline{X}],M)\cong H^i_{\mathrm{sheaf}}(X,M).\]
Here, we denote by $H^i_\mathrm{sheaf}(X,M)$ sheaf cohomology.
\end{theorem}
The right side of that equation means the following: Consider a topological space $X$, then $Sh(X,\mathrm{Ab})$ is the category of sheaves on $\mathrm{Op}(X)$ with values in abelian groups.
Then the functor\[
\Gamma(X,-): Sh(X,\mathrm{Ab})\rightarrow \mathrm{Ab}.
\]
has a right derived functor \[H^i_\mathrm{sheaf}(X,-):Sh(X,\mathrm{Ab})\rightarrow \mathrm{Ab}.\]
In the theorem, we apply this functor to the constant sheaf $M$.
\paragraph{Known:} If $X$ is a CW complex, \[
H^i_\mathrm{sheaf}(X,M)\cong H^i_\mathrm{sing}(X,M).
\]
But not in general: If $X$ is a totally disconnected compact Hausdorff, then $\Gamma(X,-)$ is exact so 
\[
H^i_\mathrm{sheaf}(X,M)=\begin{cases} \text{locally constant maps } X\rightarrow M \: & i=0\\
0 & i>0
\end{cases}
\]
However \[ H^i_\mathrm{sing}(X,M)=\begin{cases}\text{\underline{all} maps } X\rightarrow M &i=0\\
0 & i>0
\end{cases} 
\]
\begin{remark}
We are \textbf{not} considering spaces up to homotopy equivalence.
\paragraph{Further Upgrade:} Fix a topological space $X$. We have two sites:\[\lcondset/\underline{X}=(\mathrm{Pro}_\N(\mathrm{Fin})/\underline{X})^\sim\overset{\lambda}\longrightarrow Op(X)^\sim=Sh(X)\] where $-^\sim$ is the functor that passes to the topos of sheaves. For $U\subset X$\[
\lambda^*U=\underline{U}\;(\text{together with morphism }\rightarrow\underline{X}).\] Correspondingly we also have a pullback functor of derived categories
\[\lambda^*: D\left(Sh(X, \mathrm{Ab})\right)\rightarrow D\left(\mathrm{Ab}(\lcondset/\underline{X})\right)\] with Ab($\lcondset/\underline{X}$) abelian sheaves.
\end{remark}
\begin{theorem}\label{cohomology}
    Assume that $X$ is a metrizable compact Hausdorff space.
    On $D^+$, $\lambda^*$ is fully faithful. In particular $\forall \, F\in Sh(X,\mathrm{Ab})$\[
    H^i(\lcondset/\underline{X},\lambda^* F)\cong H^i_\mathrm{sheaf}(X,F).   
    \]
    If we apply this to $F=\text{constant sheaf on }M$, we get previous statement.
\end{theorem}
\begin{proof}[Proof sketch]
    We need: on $A\in D^+\left(Sh(X, \mathrm{Ab})\right)$:\[
        A\rightarrow R\lambda_*\lambda^* A\] is an isomorphism. This can be checked on stalks of points $x\in X$ \[
            A\xrightarrow{\sim?} R\lambda_*\lambda^* A\]
\noindent\textbf{Key Point:} Base Change Property: Taking stalks at $x$ commutes\footnote{For this fact, see second part of the proof of Theorem 3.2 in \cite{condensed}. Here also the property that $X$ is a metrizable compact Hausdorff space plays a role.} with $R\lambda_*$.\end{proof}

\paragraph{Here:} Use general "cohomology commutes with filtered colimits" results for general "coherent topoi". "Coherent topoi" here means qcqs. 

\noindent\textbf{Key "Geometric" Input:} \underline{X} is qcqs.

\begin{example}
Here we are trying to calculate more explicitly what happens in \ref{cohomology}:
For a metrizable compact Hausdorff space $X$ we want to calculate $\ext^i(\Z[\underline{X}],\Z)$. Therefore we try to find a projective, or at least acyclic resolution.\\
\textbf{Step 1:} Show that if $X=S\in\mathrm{Pro}_\N(\mathrm{Fin})$ is totally disconnected.
\[
    \ext^i(\Z[S],\Z)=\begin{cases}
        \cont(S,\Z)&i=0\\
        0 & i>0
    \end{cases}\]
This comes down to the following: $\forall$ hypercovers
\[S_\bullet\quad\cdots \;S_2\substack{\rightarrow\\[-0.2em]\leftarrow\\[-0.2em]\rightarrow\\[-0.2em]\leftarrow\\[-0.2em]\rightarrow}S_1\substack{\rightarrow\\[-0.8em]\leftarrow\\[-0.8em]\rightarrow}S_0\rightarrow S\]
where $S_\bullet$ is a simplicial light profinite set. Concretely that means that we get covers:
\[
\begin{tikzcd}
    & S_0 \arrow[r, two heads] & S \\[-22pt]
S_1 \arrow[r, two heads] & S_0\times_S S_0          &   \\[-22pt]
...                      &                          &  
\end{tikzcd}\]
As the hypercover is a simplical profinite set, one can termwise take the free light condensed abelian groups to obtain a simplicial object in condensed abelian groups. By the usual construction of the Moore complex we obtain a resolution\footnote{This is a highly nontrivial result! We will not go further into detail but one might as well dive deeper into it via \cite{dargus}.} 
\[
\begin{tikzcd}
    ... \arrow[r] & \Z(S_2) \arrow[r] & \Z(S_1) \arrow[r] & \Z(S_0) \arrow[r] &\Z[S] \arrow[r] & 0
    \end{tikzcd}
\] which is always exact. Now its just left to show that the dual of this resolution, i.e.
\[
\begin{tikzcd}
    0 \arrow[r] & \cont(S,\Z) \arrow[r] & \cont(S_0,\Z) \arrow[r] & \cont(S_1,\Z) \arrow[r]& ...
\end{tikzcd}\]
is exact.  We can either treat the object as sheaves on $S$ and reduce it to stalks, or we can  write the hypercover as cofiltered limit of hypercovers of finite sets by finite sets, to reduce to case of finite sets, where this is clear. \\
\textbf{Step 2:} Treat general metrizable compact Hausdorff spaces X. We want to resolve $\Z[\underline{X}]$ by $\Z[S]$'s, $S\in\mathrm{Pro}_\N(\mathrm{Fin})$.
\[S_\bullet\text{ ... }\underbrace{S_0\times_{\underline{X}}S_0\times_{\underline{X}}S_0}_{S_2}\substack{\rightarrow\\[-0.2em]\leftarrow\\[-0.2em]\rightarrow\\[-0.2em]\leftarrow\\[-0.2em]\rightarrow}\underbrace{S_0\times_{\underline{X}}S_0}_{S_1}\substack{\rightarrow\\[-0.8em]\leftarrow\\[-0.8em]\rightarrow}S_0\twoheadrightarrow \underline{X}\]
The hypercover $S_\bullet\rightarrow X$ gives us the complex\[
\begin{tikzcd}
    ... \arrow[r] & \Z(S_2) \arrow[r] & \Z(S_1) \arrow[r] & \Z(S_0) \arrow[r] &\Z[\underline{X}] \arrow[r] & 0
    \end{tikzcd}
\]
which is a resolution by $\ext^i(-,\Z)$-acyclics. 
\\
Now $\ext^i(\Z[\underline{X},\Z])$ is computed by 
\[
    \begin{tikzcd}
        0 \arrow[r] & \cont(S_0,\Z) \arrow[r] & \cont(S_1,\Z) \arrow[r] & \cont(S_2,\Z) \arrow[r]& ...
    \end{tikzcd}
    \]
The argument for that unravels to treating all terms as global sections of sheaves on $X$ and checking that it resolves the constant sheaf $\Z$ on $X$.
\end{example}
\subsection{Locally Compact Abelian Groups}
Let $\mathrm{LCA}_\mathrm{m}$ be the category of metrizable locally compact abelian groups. For example it contains discrete abelian groups, $\R$, $\C/\Z$, $\Z_p$, $\mathbb{A} = \hat{\Z}\otimes\Q\times\R$. (Small Remark: For $\mathbb{A}$, $\Q$ being discrete, the sequence $0\rightarrow\Q\rightarrow\mathbb{A}\rightarrow\mathbb{A}/\Q\rightarrow 0$ is exact.)\\
Each object in $\mathrm{LCA}_\mathrm{m}$ admits a filtration with $3$ pieces: discrete, finite dimensional $\R$ vector space, and compact metrizable abelian group. Computing $\text{Yoneda-Ext}^i$'s, $\ext^i=0$ for $i\geq 2$.
\begin{theorem}
    For $A,B\in \mathrm{LCA}_\mathrm{m}$ the following holds:
    \[
        \ext^i_\lcondab(\underline{A},\underline{B})=\begin{cases}\mathrm{Hom}_{\mathrm{LCA}_\mathrm{m}}(A,B) & i = 0\\
            \ext^i_{\mathrm{LCA}_\mathrm{m}}(A,B) & i=1\\
            0 & i\geq 0       
        \end{cases}
    \]
\end{theorem}

\begin{example}
\[ \ext^i(\underline{A},\underline{\R/\Z})=\begin{cases}
    A^\vee & i = 0\\
    0 & i>0
    \end{cases}
    \]
\end{example}
\begin{example}
\[
    \underline{\ext}^i(\underline{\R},\underline{\Z}) = 0
    \]
\end{example}
How does one actually compute these things? We need to find something close to a projective resolution of $\underline{A}$.\\
\noindent\textbf{Key:} Breen Deligne resolution\footnote{This is proven in an unpublished letter from Deligne to Breen.}:
\begin{theorem}[Breen-Deligne]
    There is a resolution of the form:
    \[
        \begin{tikzcd}
            &&   & {[(a,b)]} \arrow[r, maps to] & {[a]+[b]-[a+b]}          &             &   \\[-20pt]
 ... \arrow[r] &  \Z[M^{n_i}] \arrow[r] &... \arrow[r] & {\Z[M^2]} \arrow[r]          & {\Z[M]} \arrow[r]        & M \arrow[r] & 0 \\[-20pt]
      &   &      &                              & {[m]} \arrow[r, maps to] & m           &  
 \end{tikzcd}
\]
This is functorial in abelian groups $M$.
\end{theorem}
\begin{warning} No explicit construction of the higher differentials is known. Thus this resolution is not explicit. But all the differentials are given by universal  formulas. 
\end{warning}
By functoriality, this also works for abelian sheaves on any site:
\[
    \begin{tikzcd}
    ... \arrow[r] &{\Z[\underline{A}^{n_i}]} \arrow[r]& ... \arrow[r] & {\Z[\underline{A}^2]} \arrow[r] & {\Z[\underline{A}]} \arrow[r] & \underline{A} \arrow[r] & 0 
    \end{tikzcd}
\] which reduces computation of $\ext_\lcondab^i(\underline{A},-)$ to $\ext_\lcondab^i(\Z[\underline{A}^{n_i}],-)$ which is what we already did in \ref{extcomputat}. \\
\noindent \textbf{Also need:} For all $X$ compact Hausdorff $\ext^i_\lcondab(\Z[\underline{X}],\R)=\begin{cases}
    \cont(X,\R) & i=0\\
    0 & i>0
\end{cases}
    $ This also works with $\R$ replaced by any Banach space $\underline{V}$. But it requires local convexity of $V$ (This uses partitions of unity arguments).
\begin{example}
For $\ext^i(\underline{\R},\underline{\Z})$, we get: 
\[
\begin{tikzcd}
    ... \arrow[r] & {\Z[\underline{\R}^{n_i}]} \arrow[r]& ... \arrow[r] & {\Z[\underline{\R}^2]} \arrow[r] & {\Z[\underline{\R}]} \arrow[r] & \underline{\R} \arrow[r] & 0
    \end{tikzcd}
    \]
We know $\ext^i(\Z[\underline{R}^{n_i}],\Z)=H^i_{\mathrm{sing}}(\R^{n_i},\Z)= \begin{cases}
    \Z & i=0\\
    0 & i>0
\end{cases}$
\end{example} 
\noindent\textbf{Variant} (Maclane's $Q$-construction, Commelin) is an explicit complex \[
    \begin{array}{l}
    Q(M):\\
    \begin{tikzcd}
        ... \arrow[r] & [-15pt] {\Z[M^8]} \arrow[r] &[-15pt] {\Z[M^4]} \arrow[r]            & [-15pt] {\Z[M^2]} \arrow[r]                                       & [-15pt] {\Z[M]} \arrow[r] & [-10pt]0 \\[-20pt]
                             &                     &                                & {[a,b]} \arrow[r, maps to]                                & {[a]+[b]-[a+b]}   &   \\[-20pt]
                             &                     & {[a,b,c,d]} \arrow[r, maps to] & {\begin{array}{l}[(a,b)]+[(c,d)]\\-[(a+c),(b+d)]\\-[a,c]-[b,d]\\+[(a+b),(c+d)]\end{array}} &                   &  
        \end{tikzcd}
    \end{array}\]
\begin{theorem}
    \[Q(M)\simeq Q(\Z)\otimes^L_\Z M \Rightarrow (\ext^i(\underline{A},\underline{B})=0\,\forall i\geq 0\iff \ext^i(Q(A),B)=0\,\forall i\geq 0).\]
\end{theorem}
\noindent Using some stable homotopy theory: $Q(\Z)\simeq \bigoplus_{i\geq 0}\left(\Z\otimes_\$\Z\right)^{\oplus 2i}[i]$.
\begin{corollary}
    For all discrete abelian groups $M$, the following hold: \[
        \ext^i_\lcondab\left(\prod_\N\underline{\Z}, M\right)=\begin{cases}
            \bigoplus_\N M & i= 0\\
            0 & i> 0
        \end{cases}
    \]
The object $\prod_\N\underline{\Z}$ will be a compact pojective generator of $\solid\subset \lcondab$ where $\solid$ embeds as a full subcategory. 
\end{corollary}
\begin{warning} $\prod_\N\Z=\bigcup_{f:\N\rightarrow\N}\prod_{n\in\N}[-f(n),f(n)]$ which becomes a huge limit that is hard to control.
\end{warning}

\begin{proof}
    \[ \begin{tikzcd}
        0 \arrow[r] & \prod_\N\underline{\Z} \arrow[r] & \prod_\N \underline{\R} \arrow[r] & \prod_\N \underline{\R/\Z} \arrow[r]& 0 
    \end{tikzcd}\]
    So \[\ext^i\left(\prod_\N\underline{\R/\Z}, M\right)= \begin{cases}
        0 & i\neq1\\
        \bigoplus_\N M & i=1
    \end{cases}\]
    We need 
    \[\ext^i\left(\prod_\N\underline{\R}, M\right)=0\:\forall\:i\geq 0.\] 
    It holds that 
    \[R\mathrm{Hom}_{\underline{\R}}\left(\prod_\N\underline{\R}, \underline{R\mathrm{Hom}}(\underline{\R},M)\right)= 0
    \]
\end{proof}
The following theorem is of pure set theoretic nature:
\begin{theorem}
    For all sequential limits $...\,\twoheadrightarrow M_1\twoheadrightarrow M_0$ of countable discrete abelian groups, and all discrete abelian groups $N$, it is equivalent that 
    \begin{equation}\label{settheo}
        \ext^i(\varprojlim_{n}M_n, N)=\underset{n}\colim\, \ext^i(M_n, N)\:\:( = 0 \text{ for } i \geq 2)
    \end{equation}
    \[ \iff \ext^i\left(\prod_\N\bigoplus_\N \Z, \bigoplus_I \Z\right)=0 \text{ for } i>0.
   \]
\end{theorem}

\noindent It is easy to see that under the continuum hypothesis (\ref{settheo}) fails as $\ext^1\neq 0$.
\begin{remark}
\noindent It holds, that $\ext^i\left(\prod_\N\bigoplus_\N\Z, \bigoplus_I\Z\right)= \underset{f:\N\rightarrow\N}\colim\,\prod_{n\in\N}\prod_{m\leq f(n)}\Z.$
\end{remark}
\begin{theorem}[Bergfalk, Lambie-Hanson, Hrušák, Bannister]
    \leavevmode
    \begin{enumerate}
        \item From (\ref{settheo}) it follows that $2^{\aleph_0}>\aleph_\omega$
        \item It is consistent that (\ref{settheo}) holds and $2^{\aleph_0}=\aleph_{\omega+1}$. In fact it holds in forcing extension adjoining $J_\omega$ many Cohen reals.
    \end{enumerate}
\end{theorem}

While we will not use (\ref{settheo}) in the course, it is nice to know it can be enforced as it sometimes simplifies computations.