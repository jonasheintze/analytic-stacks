These are my personal notes on the course Analytic stacks. Use at own risk! 

Right now, they are at about half the lecture. I am planning to finish these notes to cover the whole lecture but it takes some extra time. 

Please ignore the bad LaTeX style in the .tex documents, as this is one of my first experiences with LaTeX. 

If you find typos or mistakes, it is really appreciated if you email me about them: jonas@helpsyou.de. Please also feel free to give me some feedback, especially if something is written in a confusing way. 

Lecture Notes template: https://github.com/sara-venkatraman/LaTeX-Templates/tree/master/Lecture%20Notes
